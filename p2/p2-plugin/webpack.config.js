/* eslint-env node */
const path = require('path');
const crypto = require('crypto');
const { NamedChunksPlugin, HashedModuleIdsPlugin } = require('webpack');
const merge = require('webpack-merge');
const parts = require('@atlassian/aui-webpack-config/webpack.parts');
const externals = require('@atlassian/aui-webpack-config/webpack.externals');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const I18nForP2 = require('@atlassian/aui-webpack-config/plugins/i18n-for-p2');
const ReportPlugin = require('atlassian-wrm-build-report-plugin');

const { NODE_ENV } = process.env;
const SRC_DIR = path.dirname(require.resolve('@atlassian/aui/entry/aui.core'));
const OUT_DIR = path.resolve(__dirname, 'target/classes'); // TODO: read from pom.xml
const XML_DESCRIPTORS = path.resolve(
    OUT_DIR,
    'META-INF/plugin-descriptor',
    'webpacked-modules.xml');

const delimiter = '--';

const hashed = (name) => {
    return crypto
        .createHash('md4')
        .update(name)
        .digest('hex')
        .slice(0, 10);
};

const nameSplitChunk = (module, chunks, cacheGroup) => {
    const prefix = (cacheGroup && cacheGroup !== 'default' ?
        cacheGroup + delimiter :
        '');
    const names = chunks.map(c => c.name);
    if (!names.every(Boolean)) {
        return;
    }
    names.sort();
    return 'aui.splitchunk.' + prefix + hashed(names.join(delimiter));
};

const nameChunk = `aui.chunk.[contenthash]${delimiter}[chunkhash].js`;

// Tell the log that we're about to do something
console.log('webpacking assets for P2 to...', {
    OUT_DIR,
    XML_DESCRIPTORS,
    NODE_ENV
});


const ourBackbone = path.resolve(__dirname, './entry/shim/backbone.noconflict');
const ourUnderscore = path.resolve(__dirname, './entry/shim/underscore.noconflict');

module.exports = merge([
    parts.setAuiVersion(),
    parts.extractCss(),
    parts.loadSoy(),
    parts.loadFonts(),
    parts.loadImages(),
    parts.production(),
    // p2-plugin is using wrm - at the moment wrm doesn't support source files
    // and results in console errors: https://ecosystem.atlassian.net/browse/PLUGWEB-447
    { devtool: 'hidden-source-map' },
    parts.transpileJs({
        exclude: /(node_modules|bower_components|js-vendor|compiled-soy)/,
    }),
    {
        context: SRC_DIR,
        resolve: {
            alias: {
                'actual/backbone': require.resolve('backbone'),
                'actual/underscore': require.resolve('underscore'),
                'our/backbone': ourBackbone,
                'our/underscore': ourUnderscore,
                'backbone$': ourBackbone,
                'underscore$': ourUnderscore
            }
        },
        entry: {
            // Internal things that really should not be compiled at all
            'aui.internal.deprecation': './internal/deprecation.js',
            'aui.internal.alignment': './internal/alignment.js',
            'aui.vendor.backbone': path.resolve(__dirname, './entry/deprecated/vendor/backbone.js'),
            'aui.vendor.underscore': path.resolve(__dirname, './entry/deprecated/vendor/underscore.js'),
            // jQuery UI assets (we really shouldn't be providing these; they should disappear in future AUI versions)
            'jquery.ui.data': './vendor/jquery-ui/jquery.ui.data.js',
            'jquery.ui.draggable': './vendor/jquery-ui/jquery.ui.draggable.js',
            'jquery.ui.droppable': './vendor/jquery-ui/jquery.ui.droppable.js',
            'jquery.ui.focusable': './vendor/jquery-ui/jquery.ui.focusable.js',
            'jquery.ui.keycode': './vendor/jquery-ui/jquery.ui.keycode.js',
            'jquery.ui.mouse': './vendor/jquery-ui/jquery.ui.mouse.js',
            'jquery.ui.plugin': './vendor/jquery-ui/jquery.ui.plugin.js',
            'jquery.ui.position': './vendor/jquery-ui/jquery.ui.position.js',
            'jquery.ui.resizable': './vendor/jquery-ui/jquery.ui.resizable.js',
            'jquery.ui.scroll-parent': './vendor/jquery-ui/jquery.ui.scroll-parent.js',
            'jquery.ui.selectable': './vendor/jquery-ui/jquery.ui.selectable.js',
            'jquery.ui.sortable': './vendor/jquery-ui/jquery.ui.sortable.js',
            'jquery.ui.tabbable': './vendor/jquery-ui/jquery.ui.tabbable.js',
            'jquery.ui.unique-id': './vendor/jquery-ui/jquery.ui.unique-id.js',
            'jquery.ui.widget': './vendor/jquery-ui/jquery.ui.widget.js',
            // jquery plugins that we ship for now but won't in the future
            'aui.deprecated.behaviour.jquery-form': './deprecated/aui.behaviour.jquery-form.js',
            'aui.deprecated.behaviour.jquery-spin': './deprecated/aui.behaviour.jquery-spin.js',
            'aui.deprecated.behaviour.jquery-throbber': './deprecated/aui.behaviour.jquery-throbber.js',
            // JS behaviours
            'aui.behaviour.event-bus': './behaviours/aui.behaviour.event-bus.js',
            'aui.behaviour.format': './behaviours/aui.behaviour.format.js',
            'aui.behaviour.key-code': './behaviours/aui.behaviour.key-code.js',
            'aui.behaviour.keyboard-shortcuts': './behaviours/aui.behaviour.keyboard-shortcuts.js',
            'aui.behaviour.layer-manager': './behaviours/aui.behaviour.layer-manager.js',
            'aui.behaviour.progressive-data-set': './behaviours/aui.behaviour.progressive-data-set.js',
            // Deprecated, legacy components
            'aui.deprecated.behaviour.cookie': './deprecated/aui.behaviour.cookie.js',
            'aui.deprecated.behaviour.template': path.resolve(__dirname, 'entry', './deprecated/aui.behaviour.template.js'),
            'aui.deprecated.component.dialog1': './deprecated/aui.component.dialog1.js',
            'aui.deprecated.component.dropdown1': './deprecated/aui.component.dropdown1.js',
            'aui.deprecated.component.inline-dialog1': './deprecated/aui.component.inline-dialog1.js',
            'aui.deprecated.pattern.toolbar1': './deprecated/aui.pattern.toolbar1.js',
            // HTML + JS components
            'aui.component.layer': './aui.component.layer.js', // a kind of "base" component
            'aui.component.trigger': './aui.component.trigger.js', // a kind of "base" component
            'aui.component.banner': './aui.component.banner.js',
            'aui.component.dialog2': './aui.component.dialog2.js',
            'aui.component.dropdown2': './aui.component.dropdown2.js',
            'aui.component.inline-dialog2': './aui.component.inline-dialog2.js',
            'aui.component.async-header': './aui.component.async-header.js',
            'aui.component.static-header': './aui.component.static-header.js',
            'aui.component.sidebar': './aui.component.sidebar.js',
            'aui.component.message': './aui.component.message.js',
            'aui.component.nav': './aui.component.nav.js',
            'aui.component.expander': './aui.component.expander.js',
            'aui.component.flag': './aui.component.flag.js',
            'aui.component.button': './aui.component.button.js',
            'aui.component.tabs': './aui.component.tabs.js',
            'aui.component.spinner': './aui.component.spinner.js',
            'aui.component.progressbar': './aui.component.progressbar.js',
            'aui.component.tooltip': './aui.component.tooltip.js',
            'aui.component.restful-table': './aui.component.restful-table.js',
            'aui.component.sortable-table': './aui.component.sortable-table.js',
            // Form field components
            'aui.component.form-notification': './aui.component.form-notification.js',
            'aui.component.form-validation': './aui.component.form-validation.js',
            'aui.component.form.label': './aui.component.form.label.js',
            'aui.component.form.toggle': './aui.component.form.toggle.js',
            'aui.component.form.select2': './aui.component.form.select2.js',
            'aui.component.form.single-select': './aui.component.form.single-select.js',
            'aui.component.form.file-select': './aui.component.form.file-select.js',
            'aui.component.form.date-select': './aui.component.form.date-select.js',
            'aui.component.form.checkbox-multi-select': './aui.component.form.checkbox-multi-select.js',
            // HTML + CSS patterns
            'aui.pattern.page-layout': './styles/aui.pattern.page-layout.js',
            'aui.pattern.page-header': './styles/aui.pattern.page-header.js',
            'aui.pattern.avatar': './styles/aui.pattern.avatar.js',
            'aui.pattern.badge': './styles/aui.pattern.badge.js',
            'aui.pattern.button': './styles/aui.pattern.button.js',
            'aui.pattern.help': './styles/aui.pattern.help.js',
            'aui.pattern.icon': './styles/aui.pattern.icon.js',
            'aui.pattern.group': './styles/aui.pattern.group.js',
            'aui.pattern.label': './styles/aui.pattern.label.js',
            'aui.pattern.lozenge': './styles/aui.pattern.lozenge.js',
            'aui.pattern.navigation': './styles/aui.pattern.nav.js',
            'aui.pattern.multi-step-progress': './styles/aui.pattern.multi-step-progress.js',
            'aui.pattern.forms': './styles/aui.pattern.forms.js',
            'aui.pattern.table': './styles/aui.pattern.table.js',
            'aui.pattern.toolbar2': './styles/aui.pattern.toolbar2.js',
            // Page-level CSS
            'aui.page.reset': './styles/aui.page.reset.js',
            'aui.page.typography': './styles/aui.page.typography.js',
            'aui.page.iconography': './styles/aui.page.iconography.js',
            'aui.page.links': './styles/aui.page.links.js',
            // Page-level JS
            'aui.core': './aui.core.js',
            'aui.side-effects': './aui.side-effects.js',
        },
        output: {
            path: OUT_DIR,
            chunkFilename: nameChunk,
            jsonpFunction: '__auiJsonp'
        },
        optimization: {
            runtimeChunk: 'single',
            splitChunks: {
                minSize: 0,
                chunks: 'all',
                maxAsyncRequests: Infinity,
                maxInitialRequests: Infinity,
                name: nameSplitChunk,
            }
        },
        plugins: [
            new WrmPlugin({
                pluginKey: 'com.atlassian.auiplugin', // TODO: read from pom.xml
                amdProvider: 'com.atlassian.auiplugin:null',
                xmlDescriptors: XML_DESCRIPTORS,
                providedDependencies: {
                    jquery: {
                        dependency: 'com.atlassian.plugins.jquery:jquery',
                        import: externals.jqueryExternal.jquery
                    }
                },
                resourceParamMap: {
                    'eot': [{
                        'name': 'allow-public-use',
                        'value': 'true'
                    }, {
                        'name': 'content-type',
                        'value': 'application/vnd.ms-fontobject'
                    }],
                    'otf': [{
                        'name': 'allow-public-use',
                        'value': 'true'
                    }, {
                        'name': 'content-type',
                        'value': 'application/x-font-opentype'
                    }],
                    'svg': [{
                        'name': 'allow-public-use',
                        'value': 'true'
                    }, {
                        'name': 'content-type',
                        'value': 'image/svg+xml'
                    }],
                    'svgz': [{
                        'name': 'allow-public-use',
                        'value': 'true'
                    }, {
                        'name': 'content-type',
                        'value': 'image/svg+xml'
                    }],
                    'ttf': [{
                        'name': 'allow-public-use',
                        'value': 'true'
                    }, {
                        'name': 'content-type',
                        'value': 'application/x-font-truetype'
                    }],
                    'woff': [{
                        'name': 'allow-public-use',
                        'value': 'true'
                    }, {
                        'name': 'content-type',
                        'value': 'application/font-woff'
                    }],
                    'woff2': [{
                        'name': 'allow-public-use',
                        'value': 'true'
                    }, {
                        'name': 'content-type',
                        'value': 'application/font-woff'
                    }]
                },
                webresourceKeyMap: {
                    // Internal things that really should not be compiled at all
                    'aui.internal.deprecation': 'aui-deprecation',
                    'aui.internal.alignment': 'aui-alignment',
                    'aui.vendor.backbone': 'internal-vendor-backbone',
                    'aui.vendor.underscore': 'internal-vendor-underscore',
                    // jQuery UI assets (we really shouldn't be providing these; they should disappear in future AUI versions)
                    'jquery.ui.data': 'jquery-ui-data',
                    'jquery.ui.draggable': 'jquery-ui-draggable',
                    'jquery.ui.droppable': 'jquery-ui-droppable',
                    'jquery.ui.focusable': 'jquery-ui-focusable',
                    'jquery.ui.keycode': 'jquery-ui-keycode',
                    'jquery.ui.mouse': 'jquery-ui-mouse',
                    'jquery.ui.plugin': 'jquery-ui-plugin',
                    'jquery.ui.position': 'jquery-ui-position',
                    'jquery.ui.resizable': 'jquery-ui-resizable',
                    'jquery.ui.scroll-parent': 'jquery-ui-scroll-parent',
                    'jquery.ui.selectable': 'jquery-ui-selectable',
                    'jquery.ui.sortable': 'jquery-ui-sortable',
                    'jquery.ui.tabbable': 'jquery-ui-tabbable',
                    'jquery.ui.unique-id': 'jquery-ui-unique-id',
                    'jquery.ui.widget': 'jquery-ui-widget',
                    // jQuery plugins
                    'aui.deprecated.behaviour.jquery-form': 'internal-deprecated-jquery-form',
                    'aui.deprecated.behaviour.jquery-spin': 'internal-deprecated-jquery-spin',
                    'aui.deprecated.behaviour.jquery-throbber': 'internal-deprecated-jquery-throbber',
                    // JS behaviours
                    'aui.behaviour.event-bus': 'aui-event-bus',
                    'aui.behaviour.format': 'aui-format',
                    'aui.behaviour.key-code': 'aui-key-code',
                    'aui.behaviour.keyboard-shortcuts': 'aui-keyboard-shortcuts',
                    'aui.behaviour.layer-manager': 'aui-layer-manager',
                    'aui.behaviour.progressive-data-set': 'aui-progressive-data-set',
                    // Deprecated, legacy components
                    'aui.deprecated.behaviour.cookie': 'internal-deprecated-cookie',
                    'aui.deprecated.behaviour.template': 'internal-deprecated-template',
                    'aui.deprecated.component.dialog1': 'internal-deprecated-dialog1',
                    'aui.deprecated.component.dropdown1': 'internal-deprecated-dropdown1',
                    'aui.deprecated.component.inline-dialog1': 'internal-deprecated-inline-dialog1',
                    'aui.deprecated.pattern.toolbar1': 'internal-deprecated-toolbar1',
                    // HTML + JS components
                    'aui.component.layer': 'aui-layer',
                    'aui.component.trigger': 'aui-trigger',
                    'aui.component.banner': 'aui-banner',
                    'aui.component.dialog2': 'aui-dialog2',
                    'aui.component.dropdown2': 'aui-dropdown2',
                    'aui.component.inline-dialog2': 'aui-inline-dialog2',
                    'aui.component.async-header': 'aui-header',
                    'aui.component.static-header': 'aui-header-unresponsive',
                    'aui.component.sidebar': 'aui-sidebar',
                    'aui.component.message': 'aui-message',
                    'aui.component.nav': 'aui-navigation',
                    'aui.component.expander': 'aui-expander',
                    'aui.component.flag': 'aui-flag',
                    'aui.component.button': 'aui-button',
                    'aui.component.tabs': 'aui-tabs',
                    'aui.component.spinner': 'aui-spinner',
                    'aui.component.progressbar': 'aui-progressbar',
                    'aui.component.tooltip': 'aui-tooltip',
                    'aui.component.restful-table': 'aui-restfultable',
                    'aui.component.sortable-table': 'aui-table-sortable',
                    // Form field components
                    'aui.component.form-notification': 'aui-form-notification',
                    'aui.component.form-validation': 'aui-form-validation',
                    'aui.component.form.label': 'aui-label', // todo: rename to avoid confusion with "label" pills?
                    'aui.component.form.toggle': 'aui-toggle',
                    'aui.component.form.select2': 'aui-select2',
                    'aui.component.form.date-select': 'aui-date-picker',
                    'aui.component.form.file-select': 'fancy-file-input',
                    'aui.component.form.single-select': 'aui-select', // todo: rename?
                    'aui.component.form.checkbox-multi-select': 'aui-checkbox-multiselect', // todo: rename?
                    // HTML + CSS patterns
                    'aui.pattern.page-layout': 'aui-page-layout',
                    'aui.pattern.page-header': 'aui-page-header',
                    'aui.pattern.avatar': 'aui-avatar',
                    'aui.pattern.badge': 'aui-badge',
                    'aui.pattern.help': 'aui-help',
                    'aui.pattern.icon': 'aui-icon',
                    'aui.pattern.group': 'aui-group',
                    'aui.pattern.label': 'aui-labels', // todo: rename to avoid confusion with <aui-label>
                    'aui.pattern.lozenge': 'aui-lozenge',
                    'aui.pattern.navigation': 'aui-navigation-patterns',
                    'aui.pattern.multi-step-progress': 'aui-progress-tracker',
                    'aui.pattern.forms': 'aui-forms',
                    'aui.pattern.table': 'aui-table',
                    'aui.pattern.toolbar2': 'aui-toolbar2',
                    // Page-level CSS
                    'aui.page.reset': 'aui-reset',
                    'aui.page.typography': 'aui-page-typography',
                    'aui.page.iconography': 'aui-iconography',
                    'aui.page.links': 'aui-link',
                    // Page-level JS
                    'aui.core': 'aui-core',
                    'aui.side-effects': 'ajs',
                },
            }),
            new ReportPlugin({
                filename: './report.json',
                staticReport: true,
                baseUrl: 'http://localhost:9999/ajs/'
            }),
            new NamedChunksPlugin(chunk => {
                if (chunk.name) {
                    return chunk.name;
                }

                return Array.from(chunk.modulesIterable, m =>
                    path.relative(
                        m.context,
                        m.userRequest.substring(
                            0,
                            m.userRequest.lastIndexOf('.')
                        )
                    )
                ).join(delimiter);
            }),
            new HashedModuleIdsPlugin(),
            new I18nForP2()
        ],
    },
]);
