import globalize from '@atlassian/aui/src/js/aui/internal/globalize';
import { fn, prop } from '@atlassian/aui/src/js/aui/internal/deprecation';
import template from '@atlassian/aui-template';

// Don't encourage use of this templating function.
const ajsTplProps = {
    sinceVersion: '8.0.0',
    removeInVersion: '9.0.0',
    extraInfo: 'We recommend use of other open-source templating engines, such as "lit-html" or "react".' +
        ' If you cannot do without this templating function,' +
        ' it is available as its own Node package: "@atlassian/aui-template".'
};
const ajsTemplate = fn(template, 'AJS.template', ajsTplProps);
ajsTemplate.load = template.load;
ajsTemplate.escapeHtml = template.escapeHtml;
prop(ajsTemplate, 'load', ajsTplProps);
prop(ajsTemplate, 'escapeHtml', ajsTplProps);

globalize('template', ajsTemplate);
export default ajsTemplate;
