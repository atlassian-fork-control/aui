package com.atlassian.test.servlets;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.atlassian.test.Config.PLUGIN_KEY;

@Scanned
public class SoyServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(SoyServlet.class);

    // ------------------------------------------------------------------------------------------------------- Constants
    private static final String CONTENT_TYPE = "text/html;charset=UTF-8";
    private static final String SERVER_SOY_MODULE_KEY = PLUGIN_KEY + ":soy-test-pages";
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final WebResourceManager webResourceManager;
    private final SoyTemplateRenderer soyTemplateRenderer;
    // ---------------------------------------------------------------------------------------------------- Constructors

    @Inject
    public SoyServlet(
            WebResourceManager webResourceManager,
            @ComponentImport SoyTemplateRenderer soyTemplateRenderer
    ) {
        this.webResourceManager = webResourceManager;
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // ------------------------------------------------------------------------------------------------- Helper Methods

    /**
     * Constructs a Soy namespace from the path
     *
     * @param pathInfo {@link String} representing a path, e.g. /test-pages/pages/experimental/page-layout/index.soy
     * @return {@link String} representing a camelCased Soy namespace, e.g. testPages.experimental.pageLayout.index
     */
    protected static String pathToSoyTemplate(String pathInfo) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean lastCharWasDash = false;
        final char[] chars = pathInfo.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c == '-') {
                lastCharWasDash = true;
            } else {
                if (c == '/') {
                    // we don't want the slash if its the first character in the path
                    if (i != 0) {
                        stringBuilder.append(".");
                    }
                } else if (lastCharWasDash) {
                    stringBuilder.append(Character.toUpperCase(c));
                } else {
                    stringBuilder.append(c);
                }
                lastCharWasDash = false;
            }
        }

        String newString = stringBuilder.toString();

        if (newString.endsWith(".soy")) {
            newString = newString.substring(0, newString.length() - 4);
        }
        return newString;
    }

    protected static String removePagesFromPath(String template) {
        return template.replace("testPages/pages", "testPages");
    }

    /**
     * Remove the context path from the path
     *
     * @param pathInfo {@link String} the path
     * @return {@link String} the path with the context path removed
     */
    protected static String stripContextPath(String pathInfo) {
        return pathInfo.replaceFirst("/ajstest", "");
    }
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators

    /**
     * Looks at the path info, and either serves a directory listing, or a rendered soy file
     *
     * @param req  {@link HttpServletRequest}
     * @param resp {@link HttpServletResponse}
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> context = Maps.newHashMap();
        context.put("webResourceManager", webResourceManager);
        resp.setContentType(CONTENT_TYPE);

        String pathInfo = removePagesFromPath(stripContextPath(req.getPathInfo()));

        if (pathInfo.endsWith("/")) {
            pathInfo += "index.soy";
        }

        final String templateName = pathToSoyTemplate(pathInfo);
        try {
            log.info("Rendering template '{}'", templateName);
            soyTemplateRenderer.render(resp.getWriter(), SERVER_SOY_MODULE_KEY, templateName, context);
        } catch (SoyException e) {
            log.error("Error rendering '{}': {}", templateName, e.getMessage());
            if (e.getCause() instanceof IOException) {
                throw (IOException) e.getCause();
            } else {
                throw new ServletException(e);
            }
        }

        resp.getWriter().close();
    }
}
