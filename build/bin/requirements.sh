#!/usr/bin/env bash
#set -x
#set -e

self=${0##*/}
usage=" USAGE: ${self} <dir>
  <dir> : relative path to location where the code is checked out

    Checks that we have the prerequisite commands and resources for releasing
  "

#--[ Process Arguments ]----
while [ "${1:0:1}" == "-" ]; do
  case ${1:1} in
    h|-help)
      echo "$usage"
      exit 0
    ;;
    *)    # unknown option
      echo "Unknown argument: -${1}"
      echo "$usage"
      exit 0
    ;;
  esac
  shift # past argument or value
done

if [ ${#@} -lt 1 ]; then echo "$usage"; exit 1; fi

CHECKOUT_DIR="${1}"

# Set and check the working directory
cd "$CHECKOUT_DIR"

CHECKOUT_DIR=$(pwd)
auidir="${CHECKOUT_DIR}"

# Check that we have the prerequisite commands for releasing
command -v yarn >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires yarn but it's not installed. Aborting."; exit 2; }
command -v npm >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires npm but it's not installed. Aborting."; exit 2; }
command -v git >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires git but it's not installed. Aborting."; exit 2; }
command -v mvn >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires Maven but it's not installed. Aborting."; exit 2; }

if [ ! -d "${auidir}/node_modules" ]; then
    echo >&2 "Releasing AUI requires that Node dependencies are installed. Aborting."; exit 3
fi

if [ -n "$(git status --porcelain)" ]; then
    echo >&2 "Releasing AUI requires a clean git working directory. Aborting."; exit 4
fi
