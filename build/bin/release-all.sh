#!/bin/bash
set -x
set -e

# ▄▄▄▄▄▄▄▄▄▄   ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄
#▐░░░░░░░░░░▌ ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░▌
#▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌
#▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌       ▐░▌     ▐░▌     ▐░▌          ▐░▌       ▐░▌
#▐░▌       ▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌
#▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌          ▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░▌       ▐░▌
#▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀█░█▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌     ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀ ▐░▌       ▐░▌
#▐░▌       ▐░▌▐░▌          ▐░▌          ▐░▌     ▐░▌  ▐░▌          ▐░▌          ▐░▌       ▐░▌     ▐░▌     ▐░▌          ▐░▌       ▐░▌
#▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌          ▐░▌      ▐░▌ ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌
#▐░░░░░░░░░░▌ ▐░░░░░░░░░░░▌▐░▌          ▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░▌
# ▀▀▀▀▀▀▀▀▀▀   ▀▀▀▀▀▀▀▀▀▀▀  ▀            ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀

# This script has been deprecated and is left for backwards compatibility with old build process.
# It is replaced with a set of 3 independent release scripts:
## bump-version.sh - responbsible for version bumping of all AUI components
## requirements.sh - responsible for verifying if all conditions needed for new AUI release are met
## build.sh - responsible for building and deploying new AUI version

BRANCH=$1
RELEASE_VERSION=$2
NEXT_VERSION=$3

# Check the appropriate parameters were provided to the script.
if [ -z "${BRANCH}" -o -z "${RELEASE_VERSION}" -o -z "${NEXT_VERSION}" ]; then
  echo "Usage: $(basename ${0}) [branch] [release-version] [next-version]"
  exit 1
fi

# Configure a temporary tag for NPM releases that we'll get rid of later.
TEMPTAG="do-not-use"

# Check that we have the prerequisite commands for releasing
command -v yarn >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires yarn but it's not installed. Aborting."; exit 2; }
command -v npm >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires npm but it's not installed. Aborting."; exit 2; }
command -v git >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires git but it's not installed. Aborting."; exit 2; }
command -v mvn >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires Maven but it's not installed. Aborting."; exit 2; }

# Set and check the working directory
basedir=$(pwd)
auidir=${basedir}

if [ ! -d "${auidir}/node_modules" ]; then
    echo >&2 "Releasing AUI requires that Node dependencies are installed. Aborting."; exit 3
fi

cd ${auidir}
if [ -n "$(git status --porcelain)" ]; then
    echo >&2 "Releasing AUI requires a clean git working directory. Aborting."; exit 4
fi


##
# Prep the release space
#
if [ ! -d ${auidir}/.tmp ]; then
    mkdir ${auidir}/.tmp
fi

NODE_ENV=production

##
# Bump AUI versions in preparation for building everything
#
cd ${auidir}
${auidir}/build/bin/bump-version.sh ${RELEASE_VERSION}
git commit -am "Release ${RELEASE_VERSION}."

##
# Build and deploy the Node package
# Generation of files is thanks to the prepublishOnly step in package.json
#
cd ${auidir}/packages/core
npm publish --@atlassian:registry=https://registry.npmjs.org --tag ${TEMPTAG}
npm publish --@atlassian:registry=https://packages.atlassian.com/api/npm/atlassian-npm/ --tag ${TEMPTAG}

##
# Build and deploy the P2 plugin
#
cd ${auidir}/p2
NODE_ENV=production \
mvn clean deploy --batch-mode -DskipTests

##
# Build and deploy the flat distribution
# NOTE: this also gets built as a prepublishOnly step of the core package, so it might be better to
#       lean on the side-effects of releasing to npm if we wanted to avoid building this twice.
#yarn dist --no-color --emoji 0
distzip="${auidir}/.tmp/aui-flat-pack-${RELEASE_VERSION}.zip"
cd ${auidir}/packages/core
zip -r ${distzip} ./dist
mvn deploy:deploy-file --batch-mode -DskipTests \
                       -DgroupId=com.atlassian.aui \
                       -DartifactId=aui-flat-pack \
                       -Dversion="${RELEASE_VERSION}" \
                       -Dpackaging=zip \
                       -Dfile=${distzip} \
                       -DrepositoryId=maven-atlassian-com \
                       -Durl=https://packages.atlassian.com/maven/public \


##
# If we get to here, the publish should have succeeded, so we can commit to the new version.
# Literally.
#
cd ${auidir}
${auidir}/build/bin/bump-version.sh ${NEXT_VERSION}
git tag -a ${RELEASE_VERSION} -m "${RELEASE_VERSION}"
git commit -am "Prepare for ${NEXT_VERSION}."
git push git@bitbucket.org:atlassian/aui.git ${RELEASE_VERSION}
git push git@bitbucket.org:atlassian/aui.git ${BRANCH}
