const fs = require('fs');
const path = require('path');

// compile the es6 imports
require('babel-register');

const BASEDIR = path.join(__dirname, '../../');
const SRCDIR = path.join(BASEDIR, 'packages/core/src/js/aui/internal/i18n/');
const OUTDIR = path.join(BASEDIR, '.tmp/');

function keysToProperties (keys) {
    const props = [];

    for (const key in keys) {
        if (Object.prototype.hasOwnProperty.call(keys, key)) {
            props.push(`${key} = ${keys[key]}`);
        }
    }

    return props.join('\n');
}

function convertI18n (p, cb) {
    const lang = path.basename(p, '.js').replace('aui', '');
    const filename = `i18n${lang}.properties`;
    const jsData = require(p);
    const contents = keysToProperties(jsData);

    fs.writeFile(path.join(OUTDIR, filename), contents, 'utf-8', err => {
        cb(err, {
            filename: filename,
            filepath: path.join(OUTDIR, filename)
        })
    });
}

function processI18n({ srcDir = SRCDIR, outDir = OUTDIR } = {}, cb) {
    let resolve, reject;
    const control = new Promise((good, bad) => {
        resolve = good;
        reject = bad;
    });

    // Ensure the output directory exists.
    fs.stat(outDir, function (err, stat) {
        if (!stat) {
            fs.mkdirSync(outDir);
        }

        // Read our i18n data.
        fs.readdir(srcDir, function (err, files) {
            if (err) {
                return cb(err);
            }

            let results = files
                .filter(filename => filename.indexOf('.js') > 0)
                .map(filename => {
                    const filepath = path.join(srcDir, filename);
                    return new Promise((resolve, reject) => {
                        convertI18n(filepath, (err, file) => {
                            console.log(`i18n properties for ${filename}`, err ? err : 'ok');
                            err ? reject(file.filepath, err) : resolve(file);
                        });
                    });
                });
            let final = Promise.all(results);
            final.then(cb, cb);
            final.then(resolve, reject);
        });
    });

    return control;
}

module.exports = processI18n;
