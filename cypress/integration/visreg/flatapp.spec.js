/// <reference types="Cypress" />
/* eslint-disable */

function visreg(items) {
    return items.map(function (id) {
        return { id, selector: '[data-visreg="' + id + '"]' }
    })
}

const variants = {
    'forms/default': visreg([
        'default-text-fields',
        'default-file-upload',
        'default-dropdowns-and-selects',
        'default-select-date',
        'default-textarea-legend',
        'default-checkboxes-edit',
        'default-checkboxes-legend',
        'default-one-checkbox',
        'default-legend-length-testing',
        'default-multi-column-checkboxes',
        'default-radio-buttons',
        'default-radio-matrix--deprecated',
        'default-buttons-container',
        'default-field-value-state',
        'default-checkboxes-state',
        'default-radio-buttons-state',
        'pre-text-area',
        'field-group-paragraph',
        'right-aligned-buttons--deprecated',
        'button-spacing',
        'field-icons',
        'soy-default',
        'soy-extra',
    ]),
    'forms/inlineForm': visreg([
        'inline-form',
    ]),
    'forms/topLabels': visreg([
        'top-labels-one',
        'top-label',
        'file-upload',
        'buttons-container',
        'checkboxes-edit',
        'one-checkbox',
        'legend-long-text',
        'radio-buttons'
    ]),
    'forms/inlineHelp': visreg([
        'text-entry',
        'file-upload',
        'dropdowns-and-selects',
        'date-select',
        'textarea-legend',
        'groups',
        'more-checkboxes',
        'one-checkbox',
        'multi-column-checkboxes',
        'radio-buttons',
        'radio-matrix',
        'text-entry-state',
        'checkboxes-view',
        'radio-buttons-state',
        'pre-text-area',
        'edit-state',
        'file-upload-state',
        'dropdowns-and-selects-state',
        'date-select-state',
        'textarea-legend-state',
        'checkboxes-edit-state',
        'checkboxes-legend-state',
        'one-checkbox-state',
        'checkbox-matrix-state',
        'radio-buttons-state-2',
        'radio-matrix-state-2',
        'dfv1-text',
        'dfv1-checkboxes',
        'dfv1-radio-buttons',
        'dfv1-pre-text-area',
        'dfv1-inline-form',
        'dfv1-top-label',
        'dfv1-checkboxes-legend',
        'dfv1-checkbox-legend',
        'dfv1-radio-buttons-long'
    ]),
    'forms/fieldsAndStates': visreg([
        'field-types',
        'disabled-styling',
        'field-widths-default',
        'field-widths-short',
        'field-widths-medium',
        'field-widths-long',
        'field-widths-fill-width',
    ])
};

const togglers = {
    '#switch-to-adg3': '---adg3',
    '#switch-to-legacy': '---legacy'
};
Object.entries(togglers).forEach(([query, suffix]) => {
    Object.entries(variants).forEach(([path, selectors]) => {
        context(path, () => {
            const pathPrefix = path.replace(/\//g, '_');

            beforeEach(() => {
                cy.visit(path);
                cy.get(query).click();
            });

            selectors.forEach(({ id, selector }) => {
                it(id, () => {
                    cy.get(selector).matchImageSnapshot(`${pathPrefix}_${id}${suffix}`)
                })
            });
        });
    });
});

it('experimental/select2', () => {
    const path = 'experimental/select2/';
    const selector = '.aui';
    const pathPrefix = path.replace(/\//g, '_');

    cy.visit(path);
    cy.get(selector).first().matchImageSnapshot(`${pathPrefix}_${selector}`);
});

it('experimental/select2 - multiselect', () => {
    const path = 'experimental/select2/';
    const selector = '.aui';
    const multiselectSelector = '#s2id_aui-select2-single';
    const pathPrefix = path.replace(/\//g, '_');
    cy.visit(path);

    // Multiselect closed
    {
        cy.get(multiselectSelector).first().matchImageSnapshot(`${pathPrefix}_${multiselectSelector}_closed`);
    }

    // Multiselect open
    {
        cy.get(multiselectSelector).first().click();
        cy.focused().blur();
        cy.get(selector).matchImageSnapshot(`${pathPrefix}_${multiselectSelector}_open`);
    }

    // Multiselect with choice
    {
        const selectOffscreenSelector = '.select2-results';
        cy.get(selectOffscreenSelector).get('.select2-result-selectable').first().click();
        cy.focused().blur();
        cy.get(multiselectSelector).first().matchImageSnapshot(`${pathPrefix}_${selectOffscreenSelector}`);
    }
});

it('experimental/select2 - single select', () => {
    const path = 'experimental/select2/';
    const selector = '.aui';
    const containerSelector = '#s2id_aui-select2-single2';
    const pathPrefix = path.replace(/\//g, '_');
    cy.visit(path);

    // Single select closed
    {
        cy.get(containerSelector).matchImageSnapshot(`${pathPrefix}_${containerSelector}_closed`);
    }

    // Single select open
    {
        cy.get(containerSelector).first().click();
        cy.focused().blur();
        cy.get(selector).matchImageSnapshot(`${pathPrefix}_${containerSelector}_open`);
    }
});

it('experimental/singleSelect', () => {
    const path = 'singleSelect';
    const selector = '.aui-page-panel-content';

    cy.visit(path);
    cy.get(selector).matchImageSnapshot(`${path}_${selector}`);
})
