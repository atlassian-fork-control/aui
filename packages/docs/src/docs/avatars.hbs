---
component: Avatars
analytics:
  pageCategory: component
  component: avatar
design: https://design.atlassian.com/latest/product/components/avatars/
status:
  api: general
  core: true
  wrm: com.atlassian.auiplugin:aui-avatar
  amd: false
  experimentalSince: 5.0
  generalSince: 5.1
---

<h2>Summary</h2>

<p>
    Avatars are used to add a human touch and instant clarity when understanding which user
    did what in the application. We also use avatars for projects, repositories, spaces and
    other container metaphors within Atlassian Apps.
</p>

<h2>Status</h2>
{{> status }}

<h2>Examples</h2>
<article class="aui-flatpack-example">
    <span class="aui-avatar aui-avatar-xsmall">
        <span class="aui-avatar-inner">
            <img src="images/avatar-person.svg" alt="User Avatar">
        </span>
    </span>
    <span class="aui-avatar aui-avatar-small">
        <span class="aui-avatar-inner">
            <img src="images/avatar-person.svg" alt="User Avatar">
        </span>
    </span>
    <span class="aui-avatar aui-avatar-medium">
        <span class="aui-avatar-inner">
            <img src="images/avatar-person.svg" alt="User Avatar">
        </span>
    </span>
    <span class="aui-avatar aui-avatar-large">
        <span class="aui-avatar-inner">
            <img src="images/avatar-person.svg" alt="User Avatar">
        </span>
    </span>
    <span class="aui-avatar aui-avatar-xxlarge">
        <span class="aui-avatar-inner">
            <img src="images/avatar-person.svg" alt="User Avatar">
        </span>
    </span>
    <span class="aui-avatar aui-avatar-project aui-avatar-small">
        <span class="aui-avatar-inner">
            <img src="images/avatar-project.svg" alt="Project Avatar">
        </span>
    </span>
    <span class="aui-avatar aui-avatar-project aui-avatar-medium">
        <span class="aui-avatar-inner">
            <img src="images/avatar-project.svg" alt="Project Avatar">
        </span>
    </span>
    <span class="aui-avatar aui-avatar-project aui-avatar-large">
        <span class="aui-avatar-inner">
            <img src="images/avatar-project.svg" alt="Project Avatar">
        </span>
    </span>
    <span class="aui-avatar aui-avatar-project aui-avatar-xlarge">
        <span class="aui-avatar-inner">
            <img src="images/avatar-project.svg" alt="Project Avatar">
        </span>
    </span>
    <span class="aui-avatar aui-avatar-project aui-avatar-xxxlarge">
        <span class="aui-avatar-inner">
            <img src="images/avatar-project.svg" alt="Project Avatar">
        </span>
    </span>
</article>

<h2 id="variations">Variations</h2>

<h3>User avatars</h3>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <span class="aui-avatar aui-avatar-xxlarge">
            <span class="aui-avatar-inner">
                <img src="images/avatar-person.svg" alt="Person McRealface" />
            </span>
        </span>
    </noscript>
</aui-docs-example>

<h3>Project avatars</h3>

<p>
    Avatars can be used for "container" objects &mdash; projects, spaces, repositories, etcetera &mdash;
    to give them a recognisable visual identity.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <span class="aui-avatar aui-avatar-project aui-avatar-xlarge">
            <span class="aui-avatar-inner">
                <img src="images/avatar-project.svg" alt="My super awesome project" />
            </span>
        </span>
    </noscript>
</aui-docs-example>

<h2 id="behavior">Behaviour</h2>

<h3>Textual descriptions of avatars</h3>

<p>
    An avatar must include a textual description of the concept it represents, so that its meaning can be
    conveyed and understood in non-visual contexts as well.
</p>

<ul>
    <li>
        In most cases, the avatar's <code>&lt;img&gt;</code> tag should include an <code>alt</code>
        attribute describing the image.
    </li>
    <li>
        In cases where the avatar is logically grouped with text content that describes the concept
        (for example, in a <a href="{{rootPath}}docs/page-header.html">page header</a>), the avatar is effectively decorative.
        In such cases, you should provide <strong>an empty</strong> <code>alt</code> attribute description.
    </li>
</ul>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <header class="aui-page-header">
            <div class="aui-page-header-inner">
                <div class="aui-page-header-image">
                    <span class="aui-avatar aui-avatar-project aui-avatar-xxxlarge">
                        <span class="aui-avatar-inner">
                            <img src="images/avatar-project.svg" alt="" role="presentation" />
                        </span>
                    </span>
                </div>
                <div class="aui-page-header-main">
                    <h2>My super awesome project</h2>
                    <p>
                        The avatar is related to this heading. If I were using a screen reader here,
                        it would be redundant to hear the name of the project read out twice. So,
                        we prevent the image from being described.
                    </p>
                </div>
            </div>
        </header>
    </noscript>
</aui-docs-example>

<h3>Badged content</h3>

<p>Avatars may have a single additional element overlaid in one of their four corners.</p>

<p>
    Use badged content to represent the type, permission, presence, or other contextually relevant information
    about the user or container.
</p>

<p>
    The badged content must also include a textual description of the information it represents,
    so that its meaning can be conveyed and understood in non-visual contexts as well.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <span class="aui-avatar aui-avatar-xxlarge">
            <span class="aui-avatar-inner">
                <img src="images/avatar-person.svg" alt="Person McRealface" />
            </span>
            <span class="custom-presence-indicator">
                <svg height="100%" version="1.1" viewBox="0 0 8 8" width="100%" xmlns="http://www.w3.org/2000/svg">
                    <description>Online</description>
                    <circle cx="4" cy="4" r="4"></circle>
                </svg>
            </span>
        </span>
    </noscript>
    <noscript is="aui-docs-code" type="text/css">
        .aui-avatar .custom-presence-indicator {
            /* position the indicator. AUI Avatars are a relative container. */
            position: absolute;
            bottom: 0;
            right: 0;
            /* size the indicator appropriately. content should not appear outside the indicator. */
            width: 16px;
            height: 16px;
            overflow: hidden;
            /* style the indicator. */
            color: #00875A;
            border: 3px solid #fff;
            border-radius: 100%;
            fill: #00875A;
        }
    </noscript>
</aui-docs-example>
