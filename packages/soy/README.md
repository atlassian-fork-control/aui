# AUI Soy templates

The HTML patterns for [AUI library][aui] components,
wrapped up in a [Soy][soy] (aka. [Google Closure templates][soy]) API.

[aui]: https://aui.atlassian.com/
[soy]: https://developers.google.com/closure/templates/
