import './aui.page.reset';
import './aui.page.typography';
import './aui.page.links';
import './aui.pattern.avatar';
import './aui.pattern.button';
import '@atlassian/aui/src/less/aui-page-header.less';
