/**
 * @fileOverview
 * Provides the `AJS.format` function, which powers
 * all code transformed through the WRM's jsI18n transformation.
 * @note This behaviour really should be a part of the WRM itself.
 * @see https://ecosystem.atlassian.net/browse/PLUGWEB-109
 */
export {default as format} from '@atlassian/aui/src/js/aui/format';
