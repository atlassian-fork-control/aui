/* eslint-env node */
const path = require('path');
const merge = require('webpack-merge');
const {DllReferencePlugin} = require('webpack');
const { librarySkeleton, libraryExternals } = require('@atlassian/aui-webpack-config/webpack.skeleton');
const { sourceDir, outputDir } = require('./dist.config');

const auiCssDeprecations = require.resolve('@atlassian/aui/src/js/aui-css-deprecations.js');

module.exports = merge([
    librarySkeleton,

    {
        context: sourceDir,
        entry: {
            'aui-css-deprecations': auiCssDeprecations,
            'aui-components': './aui.batch.components.js',
            'aui-datepicker': './aui.component.form.date-select.js',
            'aui-header': './aui.component.async-header.js',
            'aui-restfultable': './aui.component.restful-table.js',
            'aui-select2': './aui.component.form.select2.js',
            'aui-sidebar': './aui.component.sidebar.js',
        },

        output: {
            path: outputDir,
        },

        externals: [libraryExternals.jqueryExternal],

        optimization: {
            splitChunks: {
                chunks: 'async',
                maxAsyncRequests: Infinity,
                maxInitialRequests: Infinity,
                minSize: Infinity,
                name: true,
                cacheGroups: {
                    commons: {
                        test: /[\\/]node_modules[\\/]/,
                        priority: -10,
                        chunks: 'all',
                    },
                },
            },
        },

        plugins: [
            new DllReferencePlugin({
                context: '.',
                manifest: path.resolve(outputDir, 'aui-manifest.json')
            }),
        ]
    }
]);
