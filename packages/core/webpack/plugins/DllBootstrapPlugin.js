// This plugin modifies the generated webpack code to execute a module within the DLL bundle in addition to preserving
// the default behavior of exporting the webpack require function
class DllBootstrapPlugin {
    constructor(options) {
        this.options = options || {}
    }

    apply(compiler) {
        compiler.plugin('compilation', (compilation) => {
            compilation.mainTemplate.plugin('startup', (source, chunk) => {
                const bootstrapEntry = this.options[chunk.name];
                if (bootstrapEntry) {
                    for (const module of chunk.modulesIterable) {
                        if (module.rawRequest === bootstrapEntry) {
                            source = `
// Include bootstrap entry
__webpack_require__('${module.id}');
${source}`;
                            break;
                        }
                    }
                }

                return source;
            })
        })
    }
}

module.exports = DllBootstrapPlugin;
