/* eslint-env node */
const path = require('path');
const { BannerPlugin } = require('webpack');
const merge = require('webpack-merge');
const { librarySkeleton, libraryExternals } = require('@atlassian/aui-webpack-config/webpack.skeleton');
const { sourceDir, outputDir } = require('./dist.config');
const { name: packageName } = require('../package.json');
const  { WebpackBundleSizeLimitPlugin } = require('webpack-bundle-size-limit-plugin');

// TODO FEP-551: fix in linux / docker environment; ensure it is exercised in CI
// eslint-disable-next-line
const bundleSizePlugin = new WebpackBundleSizeLimitPlugin({
    exclude: ['.map'],
    enforceForAllBundles: true,
    config: path.join(__dirname, './bundle-sizes.js')
});

const bannerPlugin = new BannerPlugin({
    banner: require('./util/banner'),
    raw: true,
    entryOnly: true
});

const mainLibraryConfig = merge([
    librarySkeleton,

    {
        context: sourceDir,

        output: {
            libraryTarget: 'umd2',
            library: {
                root: 'AJS',
                amd: packageName,
                commonjs: packageName
            },
            umdNamedDefine: true,
            path: outputDir,
            jsonpFunction: 'auiRuntimeLoader'
        },

        resolve: {
            alias: {
                underscore$: require.resolve('underscore'),
                backbone$: require.resolve('backbone')
            }
        },

        optimization: {
            splitChunks: {
                chunks: 'initial',
                maxAsyncRequests: Infinity,
                maxInitialRequests: Infinity,
                minSize: Infinity,
                name: true,
                cacheGroups: {
                    vendors: false
                }
            }
        },

        plugins: [
            bannerPlugin,
            // TODO FEP-551: fix in linux / docker environment; ensure it is exercised in CI
            // bundleSizePlugin
        ]
    }
]);

const libraryWithInternalisedDeps = merge([
    mainLibraryConfig,
    {
        entry: {
            'aui-prototyping': './aui.batch.prototyping.js',
        },
        externals: [libraryExternals.jqueryExternal],
    }
]);

const libraryWithExternalisedDeps = merge([
    mainLibraryConfig,
    {
        entry: {
            'aui-prototyping.nodeps': './aui.batch.prototyping.js',
        },
        externals: Object.values(libraryExternals),
    }
]);

const deprecationsConfig = merge([
    librarySkeleton,
    {
        entry: {
            'aui-css-deprecations': require.resolve('@atlassian/aui/src/js/aui-css-deprecations.js'),
        },
        output: {
            libraryTarget: 'var',
            path: outputDir,
        },
        externals: [libraryExternals.jqueryExternal],
    }
]);

module.exports = [
    libraryWithInternalisedDeps,
    libraryWithExternalisedDeps,
    deprecationsConfig
];
