/* eslint-env node */
const path = require('path');
const merge = require('webpack-merge');
const { DllPlugin } = require('webpack');
const DllBootstrapPlugin = require('./plugins/DllBootstrapPlugin');
const { librarySkeleton, libraryExternals } = require('@atlassian/aui-webpack-config/webpack.skeleton');
const { sourceDir, outputDir } = require('./dist.config');

const auiLib = ['./aui.batch.patterns.js', './aui.core.js', './aui.side-effects.js'];

module.exports = merge([
    librarySkeleton,

    {
        context: sourceDir,
        entry: {
            'aui': auiLib,
        },

        externals: [libraryExternals.jqueryExternal],

        output: {
            path: outputDir,
            libraryTarget: 'var',
            library: '__AJS',
        },

        optimization: {
            splitChunks: {
                chunks: 'all',
                maxAsyncRequests: Infinity,
                maxInitialRequests: Infinity,
                minSize: Infinity,
                name: true,
            }
        },

        plugins: [
            new DllPlugin({
                path: path.resolve(outputDir, 'aui-manifest.json'),
                name: '__AJS'
            }),

            new DllBootstrapPlugin({
                'aui': auiLib
            }),
        ],
    }
]);
