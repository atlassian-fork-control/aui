const NAMESPACE = 'AJS';

export default function globalize (name, value) {
    if (typeof window[NAMESPACE] !== 'object') {
        window[NAMESPACE] = {};
    }

    return window[NAMESPACE][name] = value;
}
