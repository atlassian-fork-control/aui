import $ from './jquery';
import globalize from './internal/globalize';

function containDropdown (dropdown, containerSelector, dynamic) {
    function getDropdownOffset() {
        return dropdown.$.offset().top - $(containerSelector).offset().top;
    }

    var container;
    var ddOffset;
    var availableArea;
    var shadowOffset = 25;

    if (dropdown.$.parents(containerSelector).length !== -1) {
        container = $(containerSelector);
        ddOffset = getDropdownOffset();
        shadowOffset = 30;
        availableArea = container.outerHeight() - ddOffset - shadowOffset;

        if (availableArea <= parseInt(dropdown.$.attr('scrollHeight'), 10)) {
            containDropdown.containHeight(dropdown, availableArea);
        } else if (dynamic) {
            containDropdown.releaseContainment(dropdown);
        }
        dropdown.reset();
    }
};

containDropdown.containHeight = function (dropdown, availableArea) {
    dropdown.$.css({
        height: availableArea
    });
    if (dropdown.$.css('overflowY') !== 'scroll') {
        dropdown.$.css({
            width: 15 + dropdown.$.attr('scrollWidth'),
            overflowY: 'scroll',
            overflowX: 'hidden'
        });
    }
};

containDropdown.releaseContainment = function (dropdown) {
    dropdown.$.css({
        height: '',
        width: '',
        overflowY: '',
        overflowX: ''
    });
};

globalize('containDropdown', containDropdown);

export default containDropdown;
