const gulp = require('gulp');
const path = require('path');
const soyCli = require('../../../build/lib/soy-cli');
const soyPkg = require.resolve('@atlassian/aui-soy/package.json');

function parseOptions (opts) {
    const glob = '**.soy';
    const defaultOpts = {
        baseDir: path.resolve(__dirname, '../../test-pages/pages'),
        outDir: path.resolve(__dirname, '../dist'),
        glob,
        data: {
            language: 'en',
        },
        dependencies: [{
            baseDir: path.resolve(path.dirname(soyPkg), 'src'),
            glob,
        }, {
            baseDir: path.resolve(__dirname, '../src/soy'),
            glob,
        }, {
            baseDir: path.resolve(__dirname, '../../test-pages/common'),
            glob,
        }],
        rootNamespace: 'testPages.pages',
    };
    return Object.assign({}, defaultOpts, opts);
}

module.exports = (buildOpts) => function buildPages(done) {
    const { watch } = buildOpts;
    const soyOpts = parseOptions(buildOpts);
    const compiler = soyCli(soyOpts);

    const compileSoy = (d) => compiler.compile(soyOpts.glob).then(() => d(), d);

    if (watch) {
        compiler.compile(soyOpts.glob).then(() => {
            const allTemplates = [].concat(
                soyOpts.dependencies.map(dep => path.join(dep.baseDir, '**/*.soy')),
                path.join(soyOpts.baseDir, '**/*.soy')
            );
            gulp.watch(allTemplates, compileSoy);
            done();
        }, done);
    } else {
        compileSoy(done);
    }
};
