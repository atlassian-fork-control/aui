import $ from '@atlassian/aui/src/js/aui/jquery';
import _ from 'underscore';
import { undim } from '@atlassian/aui/src/js/aui/blanket';
import CustomEvent from '@atlassian/aui/src/js/aui/polyfills/custom-event';
import format from '@atlassian/aui/src/js/aui/format';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import layerManagerGlobal from '@atlassian/aui/src/js/aui/layer-manager-global';

/* global sinon */

function domNodeFrom (orig) {
    let target = orig;
    target = (typeof target === 'string') ? $(target) : target;
    target = (target instanceof $ || target.jquery) ? target[0] : target;
    return target;
}

function dispatch (event, orig, data) {
    const target = domNodeFrom(orig);

    if (typeof event === 'string') {
        if (event === 'click') {
            return click(target);
        }
        event = new CustomEvent(event, {
            bubbles: true,
            cancelable: true,
            detail: data
        });
    }

    if (!target || typeof target.dispatchEvent !== 'function') {
        var msg = 'The object provided to dispatch events to did not resolve to a DOM element';
        console.error(msg, {target, orig});
        var errMsg = format('{0}: was {1}', msg, String(target));
        var err = new Error(errMsg);
        err.target = target;
        throw err;
    }

    return target.dispatchEvent(event);
}

function ensureHtmlElement (el) {
    if (typeof el === 'string') {
        var div = document.createElement('div');
        div.innerHTML = el;
        return div.children.item(0);
    }

    if (el instanceof $) {
        return el.get(0);
    }

    return el;
}

function getLayers () {
    return $('.aui-layer');
}

function fixtures (fixtureItems, removeOldFixtures, fixtureElement) {
    fixtureElement = fixtureElement || document.getElementById('test-fixture');

    if (removeOldFixtures || removeOldFixtures === undefined) {
        fixtureElement.innerHTML = '';
    }

    if (fixtureItems) {
        for (var name in fixtureItems) {
            if (fixtureItems.hasOwnProperty(name)) {
                fixtureItems[name] = ensureHtmlElement(fixtureItems[name]);
                fixtureElement.appendChild(fixtureItems[name]);
            }
        }
    }

    return fixtureItems;
}

function removeLayers () {
    var $layer;

    while ($layer = layerManagerGlobal.getTopLayer()) {
        layerManagerGlobal.popUntil($layer);
        $layer.remove();
    }

    getLayers().remove();
    undim();
    // let's do some explicit cleanup... just in case ;)
    // Clean up after:
    // - blanket
    // - dialog
    // - dialog2
    // - tipsy
    $('.aui-blanket').remove();
    $('.aui-dialog, .aui-popup [data-tether-id]').remove();
    $('.aui-tooltip').remove();
    $('.tipsy').remove();
}

function click (orig) {
    const target = domNodeFrom(orig);
    if (!target || !target.nodeType) {
        return console.trace('The object provided to click on to did not resolve to a DOM element');
    }

    // sometimes people want to "click" on the document, which is nonsensical (since it isn't an element),
    // but it typically means they're trying to de-focus some element or otherwise trigger some behaviour
    // by interacting outside of a given component.
    if (target.nodeType === document.nodeType || target.nodeType === window.nodeType) {
        return document.body.click();
    }

    if (!target.click) {
        return console.trace('The object provided to click on was an odd node type', target);
    }

    // now we can click the target element itself.
    return target.click();
}

function mousedown (element) {
    dispatch('mousedown', element);
}

function hover (element) {
    ['mouseenter','mouseover','mousemove'].forEach(function (name) {
        dispatch(name, element);
    });
}

function activeElement() {
    let el;
    try {
        el = document.activeElement;
        // In IE 9+, activeElement can be null.
        // In IE 11, activeElement can be an empty object.
        if (!el || !el.nodeName) {
            el = document.body;
        }
    } catch (e) {
        console.debug('Failed to capture document.activeElement, reverting to document.body', e);
        el = document.body;
    }
    return el;
}

function pressKey (key, modifiers, onElement) {
    const target = onElement || activeElement();
    const e = new CustomEvent('keydown', {
        bubbles: true,
        cancelable: true
    });
    const keyPress = new CustomEvent('keypress', {
        bubbles: true,
        cancelable: true
    });

    modifiers = modifiers || {};

    if (typeof key === 'string') {
        let ucKey = key.toUpperCase();
        if (typeof keyCode[ucKey] === 'number') {
            key = keyCode[ucKey];
        } else {
            key = key.charCodeAt(0);
        }
    }

    [keyPress, e].forEach(event => {
        event.keyCode = key;
        event.ctrlKey = !!modifiers.control;
        event.shiftKey = !!modifiers.shift;
        event.altKey = !!modifiers.alt;
        event.metaKey = !!modifiers.meta;
        dispatch(event, target);
    });
}

function fakeTypingOut (stringInput, onElement) {
    onElement = onElement || activeElement();

    String(stringInput).split('').forEach(function(char) {
        pressKey(char, onElement);
        onElement.value += char;

        if (setTimeout.clock) {
            setTimeout.clock.tick(1);
        }
    });
}

function fakeBackspace (onElement, numTimes) {
    onElement = onElement || activeElement();
    numTimes = typeof numTimes === 'number' ? numTimes : 1;

    _(numTimes).times(function () {
        pressKey(keyCode.BACKSPACE, null, onElement);
        onElement.value = onElement.value.substring(0, onElement.value.length - 1);

        if (setTimeout.clock) {
            setTimeout.clock.tick(1);
        }
    });
}

function fakeClear (withKey, onElement) {
    onElement = onElement || activeElement();
    fakeBackspace(onElement, onElement.value.length);
    pressKey(withKey || 'escape');
}

var realTimeout = window.setTimeout;
function afterMutations (callback, delay) {
    realTimeout(callback, typeof delay === 'number' ? delay : 1);
}

var realRAF = window.requestAnimationFrame;
function afterRender (callback) {
    afterMutations(function() {
        realRAF(function() {
            callback();
        });
    })
}

function respondWithJson (server, pattern, json) {
    if (arguments.length === 2) {
        json = pattern;
        pattern = /.*/;
    }

    server.respondWith(pattern, [200, 'application/json', JSON.stringify(json)]);
}

function focus($element){
    var element = $($element)[0];
    element.focus();
    element.dispatchEvent(new CustomEvent('focus'));
}

function blur($element) {
    var element = $($element)[0];
    element.blur();
    element.dispatchEvent(new CustomEvent('blur'));
}

function mockPopper() {
    const clock = sinon.useFakeTimers();
    const onNextTick = fn => setTimeout(fn, 0);
    const raf = sinon.stub(window, 'requestAnimationFrame', onNextTick);
    const caf = sinon.stub(window, 'cancelAnimationFrame', clearTimeout);
    let restorePromise;

    if ('Promise' in window) {
        const org = window.Promise;
        window.Promise = { resolve: () => ({ then: onNextTick }) };
        restorePromise = () => window.Promise = org;
    } else {
        restorePromise = () => false;
    }

    return {
        tick() {
            clock.tick(100);
        },
        restore() {
            clock.restore();
            restorePromise();
            raf.restore();
            caf.restore();
        }
    }
}

function withKarmaIframeTopOffset(value) {
    if (window.parent === window) {
        return value
    }
    const frame = window.parent.document.querySelector('iframe')
    const rect = frame.getBoundingClientRect()

    return Math.ceil(value + rect.top);
}

function accountForKarmaIframe(position) {
    if (window.parent === window) {
        // the code is not run from iframe
        return position;
    }
    const { top } = window.parent.document.querySelector('iframe').getBoundingClientRect();
    return { x: position.x, y: Math.ceil(position.y + top) }
}

export {
    afterMutations,
    afterRender,
    click,
    dispatch,
    ensureHtmlElement,
    mousedown,
    fakeBackspace,
    fakeClear,
    fakeTypingOut,
    fixtures,
    hover,
    pressKey,
    removeLayers,
    respondWithJson,
    focus,
    blur,
    mockPopper,
    accountForKarmaIframe,
    withKarmaIframeTopOffset
};
