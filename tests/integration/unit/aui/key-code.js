import keyCode from '@atlassian/aui/src/js/aui/key-code';

describe('aui/key-code', function () {
    it('globals', function () {
        expect(AJS.keyCode.toString()).to.equal(keyCode.toString());
    });
});
