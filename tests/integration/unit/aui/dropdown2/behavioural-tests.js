import '@atlassian/aui/src/js/aui/dropdown2';
import $ from '@atlassian/aui/src/js/aui/jquery';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import {expectEvent} from '../../../helpers/assertions';
import {
    afterMutations,
    afterRender,
    click,
    focus,
    hover,
    mockPopper,
    pressKey,
} from '../../../helpers/all';

const nativeFocusAndClick = function (element) {
    focus(element);
    click(element);
};

function isVisible (element) {
    var $element = $(element);
    return $element.is(':visible');
}

function isFocus ($element) {
    return document.activeElement === $element[0] || $element.is(':focus');
}

function hasActiveClasses (element) {
    var $anchor = $(element).find('a').addBack();
    return $anchor.is('.active.aui-dropdown2-active');
}

function runDropdown2BehaviouralTests(Dropdown) {

    let clock;
    let $hideout;
    let $trigger;
    let singleDropdown;

    function singleDropdownTests() {

        function invokeTrigger($el) {
            $el.trigger('aui-button-invoke');
            clock.tick(100);
        }

        describe('with a link section,', function () {
            beforeEach(function () {
                singleDropdown.addPlainSection();
                singleDropdown.initialise();
            });

            // Dropdown2 - Trigger
            // -------------------
            //
            // Test opening and closing a dropdown via its trigger,
            // and the expected state of both the trigger and dropdown.
            //
            describe('on the trigger element', function () {
                beforeEach(function () {
                    expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                    expect(hasActiveClasses($trigger)).to.be.false;
                    expect($trigger.attr('aria-expanded')).to.equal('false');
                });

                it('firing aui-button-invoke event opens dropdown', function () {
                    invokeTrigger($trigger);

                    expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                    expect(hasActiveClasses($trigger)).to.be.true;
                    expect($trigger.attr('aria-expanded')).to.equal('true');
                });

                it('firing aui-button-invoke hides dropdown if dropdown was open', function () {
                    invokeTrigger($trigger);
                    invokeTrigger($trigger);

                    expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                    expect(hasActiveClasses($trigger)).to.be.false;
                    expect($trigger.attr('aria-expanded')).to.equal('false');
                });

                it('pressing Spacebar opens the dropdown', function () {
                    $trigger.focus();
                    pressKey(keyCode.SPACE);

                    expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                    expect(hasActiveClasses($trigger)).to.be.true;
                    expect($trigger.attr('aria-expanded')).to.equal('true');
                });

                it('pressing Enter opens the dropdown', function () {
                    $trigger.focus();
                    pressKey(keyCode.ENTER);

                    expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                    expect(hasActiveClasses($trigger)).to.be.true;
                    expect($trigger.attr('aria-expanded')).to.equal('true');
                });

                it('pressing Up Arrow key opens the dropdown', function () {
                    $trigger.focus();
                    pressKey(keyCode.UP);

                    expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                    expect(hasActiveClasses($trigger)).to.be.true;
                    expect($trigger.attr('aria-expanded')).to.equal('true');
                });

                it('pressing Down Arrow key opens the dropdown', function () {
                    $trigger.focus();
                    pressKey(keyCode.DOWN);

                    expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                    expect(hasActiveClasses($trigger)).to.be.true;
                    expect($trigger.attr('aria-expanded')).to.equal('true');
                });

                it('clicking opens the dropdown', function () {
                    click($trigger);

                    expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                    expect(hasActiveClasses($trigger)).to.be.true;
                    expect($trigger.attr('aria-expanded')).to.equal('true');
                });

                it('cannot open dropdown when disabled', function () {
                    $trigger.attr('aria-disabled', 'true');

                    click($trigger);

                    expect(hasActiveClasses($trigger)).to.be.false;
                    expect($trigger.attr('aria-expanded')).to.equal('false');
                    expect(document.location.hash).to.not.equal($trigger.attr('href'));
                });

                it('preserves existing data-aui-alignment attributes with static alignment', function () {
                    var dropdownEl = singleDropdown.$dropdown[0];
                    dropdownEl.setAttribute('data-aui-alignment', 'something useless');
                    dropdownEl.setAttribute('data-aui-alignment-static', true);

                    click($trigger);

                    expect(dropdownEl.getAttribute('data-aui-alignment')).to.equal('something useless');
                    expect(dropdownEl.hasAttribute('data-aui-alignment-static')).to.equal(true);
                });

                it('preserves existing data-aui-alignment attributes without static alignment', function () {
                    var dropdownEl = singleDropdown.$dropdown[0];
                    dropdownEl.setAttribute('data-aui-alignment', 'something useless');

                    click($trigger);

                    expect(dropdownEl.getAttribute('data-aui-alignment')).to.equal('something useless');
                    expect(dropdownEl.hasAttribute('data-aui-alignment-static')).to.equal(false);
                });

                it('clicking adds aui-dropdown2-active and active classes to trigger', function () {
                    click($trigger);

                    expect(hasActiveClasses($trigger)).to.be.true;
                });

                it('aui-button-invoke adds aui-dropdown2-active and active classes to trigger', function () {
                    invokeTrigger($trigger);

                    expect(hasActiveClasses($trigger)).to.be.true;
                });
            });

            describe('on the dropdown element', function () {
                var prevent = function (e) {
                    e.preventDefault();
                };

                afterEach(function () {
                    singleDropdown.$dropdown.off('aui-dropdown2-show');
                    singleDropdown.$dropdown.off('aui-dropdown2-hide');
                });

                it('aui-dropdown2-show event is fired when it is shown by click', function () {
                    var spy = sinon.spy();
                    singleDropdown.$dropdown.on('aui-dropdown2-show', spy);

                    click($trigger);

                    spy.should.have.been.calledOnce;
                });

                it('aui-dropdown2-show event is fired when it is shown by invoke', function () {
                    var spy = sinon.spy();
                    singleDropdown.$dropdown.on('aui-dropdown2-show', spy);

                    invokeTrigger($trigger);

                    spy.should.have.been.calledOnce;
                });

                it('aui-dropdown2-show event cannot be prevented', function () {
                    var preventSpy = sinon.spy(prevent);
                    singleDropdown.$dropdown.on('aui-dropdown2-show', preventSpy);

                    click($trigger);
                    expect(preventSpy.callCount).to.equal(1);
                    expect(singleDropdown.$dropdown[0].isVisible()).to.equal(true);
                });

                it('aui-dropdown2-hide is fired when it is hidden by click', function () {
                    var spy = sinon.spy();
                    singleDropdown.$dropdown.on('aui-dropdown2-hide', spy);

                    clock.tick(1000);
                    click($trigger);
                    clock.tick(1000);
                    click($trigger);
                    clock.tick(1000);

                    spy.should.have.been.calledOnce;
                });

                it('aui-dropdown2-hide is fired when it is closed by invoke', function () {
                    var spy = sinon.spy();
                    singleDropdown.$dropdown.on('aui-dropdown2-hide', function () {
                        spy();
                    });

                    click($trigger);
                    invokeTrigger($trigger);

                    spy.should.have.been.calledOnce;
                });

                it('aui-dropdown2-hide event cannot be prevented', function () {
                    var preventSpy = sinon.spy(prevent);
                    singleDropdown.$dropdown.on('aui-dropdown2-hide', preventSpy);

                    click($trigger);
                    click($trigger);

                    expect(preventSpy.callCount).to.equal(1);
                    expect(singleDropdown.$dropdown[0].isVisible()).to.equal(false);
                });

                it('aria-hidden set to false when opened', function () {
                    click($trigger);
                    expect(singleDropdown.$dropdown.attr('aria-hidden')).to.equal('false');
                });

                it('aria-hidden set to true when hidden', function () {
                    click($trigger);
                    click($trigger);
                    expect(singleDropdown.$dropdown.attr('aria-hidden')).to.equal('true');
                });

                it('closes when Escape is pressed', function (done) {
                    click($trigger);
                    afterMutations(function() {
                        pressKey(keyCode.ESCAPE, null, document.body);
                        afterMutations(function() {
                            expect(singleDropdown.$dropdown.attr('aria-hidden')).to.equal('true');
                            done();
                        }, 50);
                    }, 50);
                });
            });

            // Dropdown2 - API
            // ---------------
            //
            // Test the functions present on a dropdown2 component element
            //
            describe('Dropdown Element API -', function () {
                it('show() will open dropdown', function () {
                    singleDropdown.$dropdown[0].show();
                    expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                });

                it('hide() will close dropdown', function () {
                    singleDropdown.$dropdown[0].show();
                    singleDropdown.$dropdown[0].hide();
                    expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                });

                it('isVisible() returns false when hidden on load', function () {
                    expect(singleDropdown.$dropdown[0].isVisible()).to.be.false;
                });

                it('isVisible() returns true when visible', function () {
                    singleDropdown.$dropdown[0].show();
                    expect(singleDropdown.$dropdown[0].isVisible()).to.be.true;
                });

                it('isVisible() returns false when hidden after hide', function () {
                    singleDropdown.$dropdown[0].hide();
                    expect(singleDropdown.$dropdown[0].isVisible()).to.be.false;
                });
            });

            // Dropdown 2 - DOM
            // ----------------
            //
            // Test configuring a dropdown element's resting place, etc.
            //
            describe('DOM -', function () {
                it('returned to original location if data-dropdown2-hide-location is not specified', function () {
                    var originalParent = singleDropdown.$dropdown.parent()[0];

                    click($trigger);
                    clock.tick(100);
                    click($trigger);
                    clock.tick(100);

                    expect(singleDropdown.$dropdown.parent()[0]).to.equal(originalParent);
                    expect($hideout.find(singleDropdown.$dropdown).length).to.equal(0);
                });

                it('specifying the data-dropdown2-hide-location works properly when dropdown is hidden', function () {
                    $trigger.attr('data-dropdown2-hide-location', 'hideout');
                    var originalParent = singleDropdown.$dropdown.parent()[0];

                    click($trigger);
                    clock.tick(100);
                    click($trigger);
                    clock.tick(100);

                    expect(singleDropdown.$dropdown.parent()[0]).to.not.equal(originalParent);
                    expect($hideout.find(singleDropdown.$dropdown).length).to.equal(1);
                });
            });

            // Dropdown2 - Items
            // -----------------
            //
            // Test navigating and interacting with items in the dropdown
            //
            describe('Items -', function () {
                beforeEach(function (done) {
                    singleDropdown.addPlainSection();
                    singleDropdown.addHiddenSection();
                    singleDropdown.addInteractiveSection();
                    singleDropdown.initialise();
                    window.location.hash = '';
                    afterMutations(done); // otherwise, firefox will trigger hashchange
                });

                it('first item NOT active when dropdown opened by mouse', function () {
                    var $i1 = singleDropdown.getItem(1);

                    click($trigger);

                    expect(hasActiveClasses($trigger)).to.be.true;
                    expect(hasActiveClasses($i1)).to.be.false;
                });

                it('first item is focused when dropdown opened by keyboard (a11y)', function () {
                    var $i1 = singleDropdown.getItem(1);

                    $trigger.focus();
                    expect(document.activeElement).to.equal($trigger[0]);

                    pressKey(keyCode.SPACE);
                    expect(hasActiveClasses($i1)).to.be.true;
                });

                it('can navigate items using down arrow key', function () {
                    var $i1 = singleDropdown.getItem(1);
                    var $i2 = singleDropdown.getItem(2);

                    $trigger.focus();
                    pressKey(keyCode.ENTER);
                    pressKey(keyCode.DOWN);

                    expect(hasActiveClasses($i1)).to.be.false;
                    expect(hasActiveClasses($i2)).to.be.true;
                });

                it('navigated correctly using keys with hidden items', function () {
                    let $i1 = singleDropdown.getItem(1);
                    let $i2 = singleDropdown.getItem(2);
                    let $i3 = singleDropdown.getItem(3);
                    let $i4 = singleDropdown.getItem(4);

                    $i1.parent().addClass('hidden');
                    $i3.parent().addClass('hidden');

                    $trigger.focus();
                    pressKey(keyCode.ENTER);
                    expect(hasActiveClasses($i1)).to.be.false;
                    expect(hasActiveClasses($i2)).to.be.true;

                    pressKey(keyCode.DOWN);

                    expect(hasActiveClasses($i2)).to.be.false;
                    expect(hasActiveClasses($i3)).to.be.false;
                    expect(hasActiveClasses($i4)).to.be.true;
                });

                it('navigated correctly using keys with hidden items that were added after opened', function () {
                    let $i1 = singleDropdown.getItem(1);
                    let $i2 = singleDropdown.getItem(2);
                    let $i3 = singleDropdown.getItem(3);
                    let $i4 = singleDropdown.getItem(4);

                    $i1.parent().addClass('hidden');

                    $trigger.focus();
                    pressKey(keyCode.ENTER);

                    $i3.parent().addClass('hidden');
                    pressKey(keyCode.DOWN);

                    expect(hasActiveClasses($i2)).to.be.false;
                    expect(hasActiveClasses($i3)).to.be.false;
                    expect(hasActiveClasses($i4)).to.be.true;
                });

                it('can be focused with the keyboard even when disabled (a11y)', function () {
                    var $i2 = singleDropdown.getItem(2);
                    var $i3 = singleDropdown.getItem(3);
                    var $i4 = singleDropdown.getItem(4);

                    $i2.attr('aria-disabled', true);
                    $i3.attr('aria-disabled', true);

                    $trigger.focus();
                    pressKey(keyCode.ENTER);
                    pressKey(keyCode.DOWN);

                    expect(hasActiveClasses($i4)).to.be.false;
                    expect(hasActiveClasses($i2)).to.be.true;
                });

                it('can be activated by clicking', function () {
                    var spy = sinon.spy();
                    var $i1 = singleDropdown.getItem(1).attr('href','#actionable');

                    $i1.on('click', spy);
                    click($trigger);
                    click($i1);

                    spy.should.have.been.calledOnce;
                    expectEvent.isNotPrevented(spy.getCall(0).args[0]);
                });

                it('clicking on links works', function (done) {
                    $(window).one('hashchange.aui-test-suite', function () {
                        expect(window.location.hash).to.equal('#link');
                        done();
                    });
                    click($trigger);
                    click(singleDropdown.getItem(1));
                });

                it('cannot activate disabled items', function () {
                    var spy1 = sinon.spy();
                    var spy2 = sinon.spy();
                    var $i1 = singleDropdown.getItem(1);
                    $i1.attr('href', '#shouldnt-be-actionable');
                    $i1.attr('aria-disabled', true);

                    $trigger.click();
                    singleDropdown.$dropdown.on('click', spy2);
                    $i1.on('click', spy1);
                    // using jQuery clicks instead of native, because IE 11 is fucked
                    // and, for some reason, silently rewrites the original event's defaultPrevented
                    // property from true to false somewhere along the event chain before
                    // returning control to this test.
                    $i1.click();

                    expect(spy1.callCount).to.equal(1, 'on-element handlers should fire');
                    expect(spy2.callCount).to.equal(1, 'delegated handlers should still fire');
                    expectEvent.isPrevented(spy1.args[0][0]);
                    expectEvent.isPrevented(spy2.args[0][0]);
                });

                it('clicking normal dropdown items should close dropdown', function () {
                    var $ir3 = singleDropdown.getItem(3, 3);

                    click($trigger);

                    click($ir3);
                    clock.tick(100);

                    expect(hasActiveClasses($trigger)).to.be.false;
                    expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                });

                it('pressing enter or space on non-interactive radio or checkbox items should close dropdown', function () {
                    var $ir3 = singleDropdown.getItem(3, 3);

                    click($trigger);

                    pressKey(keyCode.ENTER, null, $ir3);
                    clock.tick(100);

                    expect(hasActiveClasses($trigger)).to.be.false;
                    expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                });

                it('clicking a dropdown item with the \'aui-dropdown2-interactive\' class doesn\'t hide the dropdown', function () {
                    var $ir1 = singleDropdown.getItem(1, 3);

                    click($trigger);

                    click($ir1);
                    clock.tick(100);

                    expect(hasActiveClasses($trigger)).to.be.true;
                    expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                });

                it('clicking a child of a dropdown item with the \'aui-dropdown2-interactive\' class doesn\'t hide the dropdown', function () {
                    var $ir1 = singleDropdown.getItem(1, 3);
                    var $within = $('<span>within item 1</span>');
                    $ir1.append($within)

                    click($trigger);

                    click($within);
                    clock.tick(100);

                    expect(hasActiveClasses($trigger)).to.be.true;
                    expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                });

                it('clicking a dropdown item with the legacy \'interactive\' class doesn\'t hide the dropdown', function () {
                    var $ir2 = singleDropdown.getItem(2, 3);

                    click($trigger);

                    click($ir2);
                    clock.tick(100);

                    expect(hasActiveClasses($trigger)).to.be.true;
                    expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                });

                it('clicking a dropdown item and calling preventDefault on the event should still close the dropdown', function () {
                    const prevent = function (e) {
                        e.preventDefault();
                    };

                    const $i1 = singleDropdown.getItem(1);

                    $i1.on('click', prevent);
                    click($trigger);
                    click($i1);
                    clock.tick(100);

                    expect(hasActiveClasses($trigger)).to.equal(false, 'trigger should not be active');
                    expect(isVisible(singleDropdown.$dropdown)).to.equal(false, 'dropdown should not be visible')
                });

                it('are reverted to inactive when the dropdown is closed', function () {
                    var $i3 = singleDropdown.getItem(3);

                    $trigger.focus();
                    pressKey(keyCode.ENTER);
                    pressKey(keyCode.DOWN);
                    pressKey(keyCode.DOWN);

                    expect(hasActiveClasses($i3)).to.be.true;

                    pressKey(keyCode.ESCAPE);

                    expect(hasActiveClasses($i3)).to.be.false;
                });
            });
        });

        describe('with a trigger on the left side of the window that has been clicked', function () {
            beforeEach(function () {
                $trigger.css({
                    float: 'left'
                });
                singleDropdown.initialise();
                click($trigger);
            });

            afterEach(function () {
                singleDropdown.$dropdown.removeAttr('style');
            });

            it('opens a dropdown that is anchored to the left hand side of the trigger', function () {
                var dropdownLeft = (singleDropdown.$dropdown.position().left);
                var triggerLeft = ($trigger.position().left);
                expect(triggerLeft).to.be.within(dropdownLeft - 2, dropdownLeft);
            });
        });

        describe('with a trigger on the right side of the window that has been clicked', function () {
            let popper;
            beforeEach(function () {
                popper = mockPopper();
                $trigger.css({
                    float: 'right'
                });
                singleDropdown.initialise();
                click($trigger);
                popper.tick();
            });

            afterEach(function () {
                singleDropdown.$dropdown.removeAttr('style');
                popper.restore();
            });

            it('opens a dropdown that is anchored to the right hand side of the trigger', function () {

                var dropdownRight = singleDropdown.$dropdown.position().left + singleDropdown.$dropdown.outerWidth();
                var triggerRight = $trigger.position().left + $trigger.outerWidth();
                expect(triggerRight).to.be.within(dropdownRight - 1, dropdownRight + 1);
            });
        });

        describe('with an open dropdown inside scrollable container', function () {
            let popper;
            beforeEach(function () {
                popper = mockPopper();
                const markup = $(`<div id="scroll-container" style="height: 500px; width: 500px; overflow-y: scroll">
                            <div style="height: 200px">
                            </div>
                        </div>`);
                $('#test-fixture').append(markup);
                const $container = $('#scroll-container');
                singleDropdown.initialise($container);
                $container.append($('<div style="height: 500px"></div>'));
                click($trigger);
                popper.tick();
            });

            afterEach(function () {
                popper.restore();
            });

            it('dropdown is aligned correctly after scroll of the container', function (done) {
                const scrollAmount = 100;
                const scrollContainer = $('#scroll-container');
                const oldPosition = singleDropdown.$dropdown.position();
                scrollContainer[0].scrollTop = scrollAmount;
                expect(oldPosition.top).to.equal(singleDropdown.$dropdown.position().top);
                afterRender(() => {
                    popper.tick();
                    const newPosition = singleDropdown.$dropdown.position();
                    expect(oldPosition.top).to.not.equal(newPosition.top, 'y position of dropdown should change to account for container scroll');
                    done();
                })
            });
        });

        // Dropdown2 - Special Items
        // -------------------------
        //
        // Test interaction with special dropdown item types, like
        // checkboxes and radio buttons.
        //
        describe('with checkbox and hidden items; clicking the trigger', function () {
            beforeEach(function () {
                singleDropdown.addCheckboxSection();
                singleDropdown.addHiddenSection();
                singleDropdown.initialise();

                click($trigger);
            });

            it('adds checked attrs/classes on click checkboxes', function () {
                var $c1 = singleDropdown.getItem(1);
                click($c1);
                expect(singleDropdown.isChecked($c1)).to.be.true;
            });

            it('toggles checked to unchecked on checkboxes', function () {
                var $c1 = singleDropdown.getItem(1);
                var $c2 = singleDropdown.getItem(2);

                click($c1);
                click($c2);

                expect(singleDropdown.isChecked($c1)).to.be.true;
                expect(singleDropdown.isUnchecked($c2)).to.be.true;
            });

            it('can have the click event prevented', function () {
                let $c1 = singleDropdown.getItem(1);

                $c1.on('click', function (e) {
                    e.preventDefault();
                });

                click($c1);

                expect(singleDropdown.isUnchecked($c1)).to.be.true;
            });

            it('fires event for checkbox being checked', function (done) {
                var checkedCount = 0;
                singleDropdown.$dropdown.on(singleDropdown.checkEvent, function (e) {
                    if (singleDropdown.isChecked(e.target)) {
                        checkedCount += 1;
                    }
                });

                let $c1 = singleDropdown.getItem(1);
                let $c2 = singleDropdown.getItem(2);
                let $c3 = singleDropdown.getItem(3);

                click($c1);
                afterMutations(function uncheck2() {
                    click($c2);
                    afterMutations(function check3() {
                        click($c3);
                        afterMutations(() => {
                            // afterMutations since the web component triggers its
                            // check event from an attribute listener.
                            expect(checkedCount).to.equal(2);
                            done();
                        });
                    });
                });
            });

            it('fires event for checkbox being unchecked', function (done) {
                var uncheckedCount = 0;
                singleDropdown.$dropdown.on(singleDropdown.uncheckEvent, function (e) {
                    if (singleDropdown.isUnchecked(e.target)) {
                        uncheckedCount += 1;
                    }
                });

                let $c1 = singleDropdown.getItem(1);
                let $c2 = singleDropdown.getItem(2);
                let $c3 = singleDropdown.getItem(3);

                click($c1);
                afterMutations(function uncheck2() {
                    click($c2);
                    afterMutations(function check3() {
                        click($c3);
                        afterMutations(() => {
                            // afterMutations since the web component triggers its
                            // uncheck event from an attribute listener.
                            expect(uncheckedCount).to.equal(1);
                            done();
                        });
                    });
                });
            });

            it('cannot check hidden, disabled checkboxes', function () {
                var $h1 = singleDropdown.getItem(1, 2);
                var $h2 = singleDropdown.getItem(2, 2);

                click($h1);
                click($h2);

                expect(singleDropdown.isUnchecked($h1)).to.equal(true, 'h1');
                expect(singleDropdown.isChecked($h2)).to.equal(true, 'h2');
            });

            it('and pressing enter on a checkbox item toggles it', function () {
                var $c1 = singleDropdown.getItem(1);
                pressKey('enter', {}, $c1[0]);
                expect(singleDropdown.isChecked($c1)).to.be.true;
            });

            it('and pressing space on a checkbox item toggles it', function () {
                var $c1 = singleDropdown.getItem(1);
                pressKey('space', {}, $c1[0]);
                expect(singleDropdown.isChecked($c1)).to.be.true;
            });
        });

        describe('with radio buttons; clicking the trigger', function () {
            beforeEach(function () {
                singleDropdown.addRadioSection();
                singleDropdown.initialise();
                click($trigger);
            });

            it('and clicking radio buttons adds aui-dropdown2-checked', function () {
                var $r3 = singleDropdown.getItem(3);
                click($r3);
                expect(singleDropdown.isChecked($r3)).to.be.true;
            });

            it('toggles checked to unchecked on radio buttons', function () {
                var $r1 = singleDropdown.getItem(1);
                var $r2 = singleDropdown.getItem(2);

                click($r1);

                expect(singleDropdown.isChecked($r1)).to.be.true;
                expect(singleDropdown.isUnchecked($r2)).to.be.true;
            });

            it('can have the click event prevented', function () {
                let $r1 = singleDropdown.getItem(1);
                let $r2 = singleDropdown.getItem(2);

                $r1.on('click', function (e) {
                    e.preventDefault();
                });

                click($r1);

                expect(singleDropdown.isUnchecked($r1)).to.be.true;
                expect(singleDropdown.isChecked($r2)).to.be.true;
            });


            it('fires event for radio being checked', function (done) {
                var checkedCount = 0;
                singleDropdown.$dropdown.on(singleDropdown.checkEvent, function (e) {
                    if (singleDropdown.isChecked(e.target)) {
                        checkedCount += 1;
                    }
                });

                var $r1 = singleDropdown.getItem(1);
                var $r2 = singleDropdown.getItem(2);
                var $r3 = singleDropdown.getItem(3);

                click($r2);
                afterMutations(function deselect2andSelect1() {
                    click($r1);
                    afterMutations(function deselect1andSelect3() {
                        click($r3);
                        afterMutations(() => {
                            // afterMutations since the web component triggers its
                            // check event from an attribute listener.
                            expect(checkedCount).to.equal(2);
                            done();
                        });
                    });
                });
            });

            it('fires event for radio being unchecked', function (done) {
                var uncheckedCount = 0;
                singleDropdown.$dropdown.on(singleDropdown.uncheckEvent, function (e) {
                    if (singleDropdown.isUnchecked(e.target)) {
                        uncheckedCount += 1;
                    }
                });

                var $r1 = singleDropdown.getItem(1);
                var $r2 = singleDropdown.getItem(2);
                var $r3 = singleDropdown.getItem(3);

                click($r1);
                afterMutations(function deselect1andSelect2() {
                    click($r2);
                    afterMutations(function deselect2andSelect3() {
                        click($r3);
                        afterMutations(() => {
                            // afterMutations since the web component triggers its
                            // uncheck event from an attribute listener.
                            expect(uncheckedCount).to.equal(3);
                            done();
                        });
                    });
                });
            });

            it('and pressing enter on a radio item toggles it', function () {
                var $r3 = singleDropdown.getItem(3);
                pressKey('enter', {}, $r3[0]);
                expect(singleDropdown.isChecked($r3)).to.be.true;
            });

            it('and pressing space on a radio item toggles it', function () {
                var $r3 = singleDropdown.getItem(3);
                pressKey('space', {}, $r3[0]);
                expect(singleDropdown.isChecked($r3)).to.be.true;
            });
        });

        // Dropdown2 - Submenus
        // --------------------
        //
        // Test environment setup for dropdowns with submenus.
        // Test the basic interactions with dropdown submenus.
        //
        describe('which has a submenu', function () {
            var clock;
            var $firstMenuTrigger;
            var $secondMenuTrigger;
            var $thirdMenuTrigger;
            var firstSubmenu;
            var secondSubmenu;

            beforeEach(function () {
                firstSubmenu = new Dropdown();
                singleDropdown.addSubmenuSection(firstSubmenu);

                secondSubmenu = new Dropdown();
                firstSubmenu.addSubmenuSection(secondSubmenu);

                secondSubmenu.addPlainSection();

                singleDropdown.initialise();
                firstSubmenu.initialise();
                secondSubmenu.initialise();

                clock = sinon.useFakeTimers();

                $firstMenuTrigger = $trigger;
                $secondMenuTrigger = singleDropdown.getItem(2);
                $thirdMenuTrigger = firstSubmenu.getItem(2);
            });

            afterEach(function () {
                clock.restore();
            });

            function countOpenDropdowns () {
                return $('.aui-dropdown2[aria-hidden=false]').length;
            }

            it('can be opened with aui-button-invoke', function () {
                expect(countOpenDropdowns()).to.equal(0);

                invokeTrigger($firstMenuTrigger);
                invokeTrigger($secondMenuTrigger);

                expect(countOpenDropdowns()).to.equal(2);
            });

            it('will focus first item in submenu if opened via keyboard (a11y)', function () {
                $firstMenuTrigger.focus();
                clock.tick(100);
                pressKey(keyCode.SPACE);
                clock.tick(100);
                $secondMenuTrigger.focus();
                clock.tick(100);
                pressKey(keyCode.SPACE);
                clock.tick(100);

                var $firstMenuFirstItem = firstSubmenu.getItem(1);
                expect(document.activeElement).to.equal($firstMenuFirstItem[0]);
            });

            it('can be opened via the right arrow key', function () {
                $firstMenuTrigger.focus();
                pressKey(keyCode.ENTER);
                $secondMenuTrigger.focus();
                pressKey(keyCode.RIGHT);
                expect(countOpenDropdowns()).to.equal(2);
                pressKey(keyCode.DOWN);
                pressKey(keyCode.RIGHT);
                expect(countOpenDropdowns()).to.equal(3);
            });

            it('will not open when pressing right arrow on a non trigger item', function () {
                $firstMenuTrigger.focus();
                pressKey(keyCode.ENTER);
                pressKey(keyCode.RIGHT);
                expect(countOpenDropdowns()).to.equal(1);
            });

            it('will not open when pressing the up arrow on a trigger item', function () {
                $firstMenuTrigger.focus();
                pressKey(keyCode.ENTER);
                $secondMenuTrigger.focus();
                pressKey(keyCode.UP);
                expect(countOpenDropdowns()).to.equal(1);
            });

            it('will not open when pressing the down arrow on a trigger item', function () {
                $firstMenuTrigger.focus();
                pressKey(keyCode.ENTER);
                $secondMenuTrigger.focus();
                pressKey(keyCode.DOWN);
                expect(countOpenDropdowns()).to.equal(1);
            });

            it('can be opened by hovering their trigger', function () {
                var $nonTriggerInFirstMenu = singleDropdown.getItem(1);

                click($firstMenuTrigger);
                hover($secondMenuTrigger);
                clock.tick(100);
                hover($thirdMenuTrigger);
                clock.tick(100);

                expect(countOpenDropdowns()).to.equal(3);

                hover($nonTriggerInFirstMenu);
                clock.tick(100);

                expect(countOpenDropdowns()).to.equal(1);
            });

            it('cannot be opened if their trigger is aria-disabled', function () {
                $secondMenuTrigger.attr('aria-disabled', 'true');

                click($firstMenuTrigger);

                hover($secondMenuTrigger);
                expect(countOpenDropdowns()).to.equal(1);

                click($secondMenuTrigger);
                expect(countOpenDropdowns()).to.equal(1);
            });

            it('cannot be opened if their triggers have the disabled or aui-dropdown2-disabled class', function () {
                $secondMenuTrigger.addClass('disabled aui-dropdown2-disabled');

                click($firstMenuTrigger);

                hover($secondMenuTrigger);
                expect(countOpenDropdowns()).to.equal(1);

                click($secondMenuTrigger);
                expect(countOpenDropdowns()).to.equal(1);
            });

            it('will not close top-level dropdown when pressing left arrow key in a top-level menu', function () {
                $firstMenuTrigger.focus();
                pressKey(keyCode.ENTER);
                pressKey(keyCode.LEFT);

                expect(countOpenDropdowns()).to.be.at.least(1);
            });

            it('will not close the dropdown when clicking the submenu trigger item', function () {
                click($firstMenuTrigger);
                expect(countOpenDropdowns()).to.equal(1);

                click($secondMenuTrigger);
                expect(countOpenDropdowns()).to.equal(2, 'clicking submenu trigger opens the submenu without closing current dropdown');

                click($secondMenuTrigger);
                expect(countOpenDropdowns()).to.equal(1, 'clicking submenu trigger for open submenu closes submenu without closing current dropdown');
            });

            describe('and all submenus are opened with an event', function () {
                beforeEach(function () {
                    invokeTrigger($firstMenuTrigger);
                    invokeTrigger($secondMenuTrigger);
                    invokeTrigger($thirdMenuTrigger);
                    clock.tick(100);
                });

                it('leave three open dropdowns', function () {
                    expect(countOpenDropdowns()).to.equal(3);
                });

                it('will close all nested submenus when the submenu trigger is clicked', function () {
                    click($secondMenuTrigger);
                    expect(countOpenDropdowns()).to.equal(1);
                });

                it('will not place focus on dropdown trigger when clicking the document to close dropdowns', function () {
                    click(document);
                    clock.tick(100);
                    expect(document.activeElement).to.not.equal($firstMenuTrigger[0]);
                });
            });

            describe('and all submenus are opened with click', function () {
                beforeEach(function () {
                    click($firstMenuTrigger);
                    click($secondMenuTrigger);
                    click($thirdMenuTrigger);
                });

                it('leave three open dropdowns', function () {
                    expect(countOpenDropdowns()).to.equal(3);
                });

                it('will all close when the document is clicked', function () {
                    click(document);
                    clock.tick(100);
                    expect(countOpenDropdowns()).to.equal(0);
                });

                it('will all close when root dropdown trigger is clicked', function () {
                    click($firstMenuTrigger);
                    expect(countOpenDropdowns()).to.equal(0);
                });
            });

            describe('and all submenus are opened with hover', function () {
                beforeEach(function () {
                    click($firstMenuTrigger);
                    hover($secondMenuTrigger);
                    clock.tick(100);
                    hover($thirdMenuTrigger);
                    clock.tick(100);
                });

                it('leave three open dropdowns', function () {
                    expect(countOpenDropdowns()).to.equal(3);
                });

                it('will not place focus on dropdown submenu trigger when hovering a non-trigger item in that menu', function () {
                    var $nonTriggerInFirstMenu = singleDropdown.getItem(1);
                    hover($nonTriggerInFirstMenu);
                    clock.tick(100);

                    expect(countOpenDropdowns()).to.equal(1);
                    expect(document.activeElement).to.not.equal($firstMenuTrigger[0]);
                });
            });

            describe('and all submenus are opened with the keyboard', function () {
                beforeEach(function () {
                    $firstMenuTrigger.focus();
                    pressKey(keyCode.ENTER);
                    $secondMenuTrigger.focus();
                    pressKey(keyCode.ENTER);
                    $thirdMenuTrigger.focus();
                    pressKey(keyCode.ENTER);
                });

                it('leave three open dropdowns', function () {
                    expect(countOpenDropdowns()).to.equal(3);
                });

                it('then dropdowns can be closed from any position in a dropdown with the left arrow key', function () {

                    pressKey(keyCode.LEFT);

                    expect(countOpenDropdowns()).to.equal(2);

                    pressKey(keyCode.UP);
                    pressKey(keyCode.LEFT);

                    expect(countOpenDropdowns()).to.equal(1);
                });

                it('will leave their triggers active when closed by pressing Escape', function () {
                    expect(hasActiveClasses($secondMenuTrigger)).to.be.true;
                    expect(hasActiveClasses($thirdMenuTrigger)).to.be.true;

                    pressKey(keyCode.ESCAPE);

                    expect(hasActiveClasses($thirdMenuTrigger)).to.be.true;
                    expect(hasActiveClasses($secondMenuTrigger)).to.be.true;
                    expect(document.activeElement).to.equal($thirdMenuTrigger[0]);

                    pressKey(keyCode.ESCAPE);

                    expect(hasActiveClasses($thirdMenuTrigger)).to.be.false;
                    expect(hasActiveClasses($secondMenuTrigger)).to.be.true;
                    expect(document.activeElement).to.equal($secondMenuTrigger[0]);
                });

                it('will close one submenu at a time when pressing Escape', function () {
                    expect(countOpenDropdowns()).to.equal(3);
                    pressKey(keyCode.ESCAPE);
                    expect(countOpenDropdowns()).to.equal(2);
                    pressKey(keyCode.ESCAPE);
                    expect(countOpenDropdowns()).to.equal(1);
                    pressKey(keyCode.ESCAPE);
                    expect(countOpenDropdowns()).to.equal(0);
                });

                it('will place focus on dropdown trigger when closed with Escape', function () {
                    pressKey(keyCode.ESCAPE);
                    expect(document.activeElement).to.equal($thirdMenuTrigger[0]);
                    pressKey(keyCode.ESCAPE);
                    expect(document.activeElement).to.equal($secondMenuTrigger[0]);
                    pressKey(keyCode.ESCAPE);
                    expect(document.activeElement).to.equal($firstMenuTrigger[0]);
                });
            });
        });
    }

    // Dropdown2 - Single Dropdown Environment
    // ---------------------------------------
    //
    // Create the basic dropdown test environment.
    // There is only one dropdown and one trigger in this environment.
    // The assertion of how dropdowns behave with other dropdowns and layers comes later.
    //
    describe('and one dropdown+trigger', function () {

        beforeEach(function () {
            clock = sinon.useFakeTimers();
            singleDropdown = new Dropdown();
            singleDropdown.addTrigger();

            $trigger = singleDropdown.$trigger;
            $hideout = $('<div id="hideout"></div>');
            $('#test-fixture').append($hideout);
            window.location.hash = 'test';
        });

        afterEach(function () {
            clock.restore();

            $('.aui-dropdown2').remove();
            $('.aui-dropdown2-trigger').remove();
            $hideout.remove();
        });

        singleDropdownTests();
    });

    // Dropdown2 - Double-trigger Dropdown Environment
    // ---------------------------------------
    //
    // Create a slightly more complex dropdown test environment.
    // There is only one dropdown but it has two triggers in this environment.
    // The assertions here all happen on the second trigger.
    // The assertion of how dropdowns behave with other dropdowns and layers comes later.
    //
    describe('and one dropdown which has 2 triggers', function () {

        beforeEach(function () {
            clock = sinon.useFakeTimers();

            singleDropdown = new Dropdown();
            singleDropdown.addTrigger();
            singleDropdown.addSecondTrigger();

            $trigger = singleDropdown.$secondTrigger;
            $hideout = $('<div id="hideout"></div>');
            $('#test-fixture').append($hideout);
            window.location.hash = 'test';
        });

        afterEach(function () {
            clock.restore();

            $('.aui-dropdown2').remove();
            $('.aui-dropdown2-trigger').remove();
            $hideout.remove();
        });

        singleDropdownTests();
    });

    // Dropdown2 - Multiple Dropdown Environment
    // -----------------------------------------
    //
    // Create a dropdown test environment with multiple dropdowns.
    // This environment is for asserting that interaction a second dropdown+trigger will
    // put the first dropdown+trigger in an expected state.
    //
    describe('and multiple dropdowns', function () {
        var clock;
        var dropdown1;
        var dropdown2;

        beforeEach(function () {
            clock = sinon.useFakeTimers();

            dropdown1 = new Dropdown();
            dropdown1.addPlainSection();
            dropdown1.addPlainSection2();
            dropdown1.addTrigger();

            dropdown2 = new Dropdown();
            dropdown2.addPlainSection();
            dropdown2.addPlainSection2();
            dropdown2.addTrigger();

            var $triggerGroup = $('<ul class="aui-dropdown2-trigger-group"></ul>');
            $('#test-fixture').append($triggerGroup);

            dropdown1.initialise($triggerGroup);
            $triggerGroup.append('<a href="#">Not a menu trigger</a>');
            dropdown2.initialise($triggerGroup);
        });

        afterEach(function () {
            clock.restore();

            $('.aui-dropdown2').remove();
            $('.aui-dropdown2-trigger').remove();

            dropdown1.$dropdown.remove();
            dropdown1.$trigger.remove();
            dropdown2.$dropdown.remove();
            dropdown2.$trigger.remove();

            dropdown1 = null;
            dropdown2 = null;
        });

        it('open on click correctly one dropdown', function () {
            click(dropdown1.$trigger);
            clock.tick(100);

            expect(hasActiveClasses(dropdown1.$trigger)).to.be.true;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;
        });

        it('in aui-dropdown2-trigger-group open on click correctly multiple dropdowns', function () {
            click(dropdown1.$trigger);
            clock.tick(100);
            click(dropdown2.$trigger);
            clock.tick(100);

            expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.true;
        });

        it('in aui-dropdown2-trigger-group open on hover after one is clicked', function () {
            click(dropdown1.$trigger);
            clock.tick(100);
            hover(dropdown2.$trigger);
            clock.tick(0);

            expect(isFocus(dropdown1.$trigger)).to.be.false;

            expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.true;
        });

        it('in aui-dropdown2-trigger-group doesnt open on over when one not clicked', function () {
            hover(dropdown2.$trigger);
            clock.tick(0);

            expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;
        });

        it('handles focus correctly when clicking one trigger and then another', function () {
            nativeFocusAndClick(dropdown1.$trigger[0]);
            clock.tick(100);

            expect(isFocus(dropdown1.$trigger)).to.be.true;
            expect(isFocus(dropdown2.$trigger)).to.be.false;

            expect(hasActiveClasses(dropdown1.$trigger)).to.be.true;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;

            nativeFocusAndClick(dropdown2.$trigger[0]);
            clock.tick(100);

            expect(isFocus(dropdown1.$trigger)).to.be.false;
            expect(isFocus(dropdown2.$trigger)).to.be.true;

            expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.true;
        });

        it('doesn\'t leave focus on the first trigger when clicking and then hovering over another ', function () {
            nativeFocusAndClick(dropdown1.$trigger[0]);
            clock.tick(100);
            hover(dropdown2.$trigger);
            clock.tick(0);

            expect(isFocus(dropdown1.$trigger)).to.be.false;
            expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.true;
        });

        var isSafari = navigator.userAgent.indexOf('Safari') !== -1;
        // skip this test for Safari
        (isSafari ? it.skip : it)('doesn\'t leave focus on the first trigger when double clicking and then hovering over another ', function () {
            nativeFocusAndClick(dropdown1.$trigger[0]);
            clock.tick(100);
            nativeFocusAndClick(dropdown1.$trigger[0]);
            clock.tick(100);
            hover(dropdown2.$trigger);
            clock.tick(0);

            expect(isFocus(dropdown1.$trigger)).to.be.false;
            expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
        });

        it('in aui-dropdown2-trigger-group pressing tab will focus next item in group', function () {
            var keySpy = sinon.spy();

            dropdown1.$trigger.focus();
            pressKey(keyCode.ENTER);
            pressKey(keyCode.DOWN);

            expect(hasActiveClasses(dropdown1.$trigger)).to.be.true;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;

            $(document).on('keydown', keySpy);
            pressKey(keyCode.TAB);
            $(document).off('keydown', keySpy);

            expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;
            // I'd love to test document.activeElement, but I can't adequately fake the native behaviour such that the focus moves of its own accord in a test.
            keySpy.should.have.been.calledOnce;
            expectEvent.isNotPrevented(keySpy.args[0][0]);
        });

        it('in aui-dropdown2-trigger-group pressing shift-tab will focus previous item in group', function () {
            var keySpy = sinon.spy();

            dropdown2.$trigger.focus();
            pressKey(keyCode.ENTER);
            pressKey(keyCode.DOWN);

            expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.true;

            $(document).one('keydown', keySpy);
            pressKey(keyCode.TAB, {shift: true});

            expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
            expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;
            // I'd love to test document.activeElement, but I can't adequately fake the native behaviour such that the focus moves of its own accord in a test.
            keySpy.should.have.been.calledOnce;
            expectEvent.isNotPrevented(keySpy.args[0][0]);
        });

        it('only allows for one trigger to have aui-dropdown2-active and active classes', function () {
            click(dropdown1.$trigger);
            expect(hasActiveClasses(dropdown1.$trigger)).to.be.true;

            // Open the second dropdown
            click(dropdown2.$trigger);

            expect(hasActiveClasses(dropdown2.$trigger)).to.be.true;
            expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
        });
    });
}

export {
    runDropdown2BehaviouralTests,
};
