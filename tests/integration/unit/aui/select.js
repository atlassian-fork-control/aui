import $ from '@atlassian/aui/src/js/aui/jquery';
import { I18n } from '@atlassian/aui/src/js/aui/i18n';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import SingleSelect, {bindSelectMousedown} from '@atlassian/aui/src/js/aui/select';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import { INPUT_SUFFIX } from '@atlassian/aui/src/js/aui/internal/constants';
import CustomEvent from '@atlassian/aui/src/js/aui/polyfills/custom-event';
import {
    afterMutations,
    blur,
    click,
    fixtures,
    focus,
    fakeBackspace,
    fakeClear,
    fakeTypingOut,
    hover,
    mousedown,
    pressKey,
    respondWithJson,
    mockPopper
} from '../../helpers/all';
import {expectEvent} from '../../helpers/assertions';

let clock;

function createAndSkate (html, target) {
    var constructed = fixtures({
        singleSelect: html,
        styles: '<style>[aria-hidden=true] { display: none !important; } .active { background-color: yellow; } </style>'
    }, true, target);

    return skate.init(constructed.singleSelect);
}

function isInResults(results, text) {
    return results.indexOf(text) !== -1;
}

function getInputFor (singleSelect) {
    return singleSelect._input;
}

function getResultsAsArray (singleSelect) {
    var anchors = singleSelect._dropdown.querySelectorAll('li[id]');
    var options = [];

    for (var a = 0; a < anchors.length; a++) {
        options.push(anchors[a].textContent);
    }

    return options;
}

function getResultsForTyping (singleSelect, text) {
    const input = getInputFor(singleSelect);
    input.value = '';
    focus(input);
    fakeTypingOut(text, input);
    return getResultsAsArray(singleSelect);
}

function getDropdownFor (singleSelect) {
    return singleSelect._dropdown;
}

function getTriggerFor (singleSelect) {
    return singleSelect.querySelector('button');
}

function getHighlightedDropdownItem (dropdown) {
    return dropdown.querySelector('.aui-select-active');
}

function resizeWindow (height) {
    $('#test-fixture').height(height);
    $(window).trigger('resize');
    clock.tick(200);
}

function positionElement (element, top) {
    var $wrapper = $('<div></div>');
    $wrapper.css('position', 'absolute');
    $wrapper.css('top', top + 'px');
    $(element).wrap($wrapper);
}

function positionOf(el1) {
    return {
        isBelow(el2) {
            return el1.getBoundingClientRect().top > el2.getBoundingClientRect().top;
        }
    };
}

function hasNoSuggestions (singleSelect) {
    var noSuggestionsElement = getDropdownFor(singleSelect).querySelectorAll('li')[0];
    return getResultsAsArray(singleSelect).length === 0 && noSuggestionsElement.innerHTML === I18n.getText('aui.select.no.suggestions');
}

describe('aui/select', function () {
    it('global', function () {
        expect(AJS.select.toString()).to.equal(SingleSelect.toString());
    });

    it('AMD module', function (done) {
        amdRequire(['aui/select'], function (amdModule) {
            expect(amdModule).to.equal(SingleSelect);
            done();
        });
    });
});

describe('aui/select - construction and initialisation', function () {
    var singleSelect;
    var dropdown;

    beforeEach(function () {
        singleSelect = undefined;
        dropdown = undefined;
    });

    it('can be initialised', function () {
        singleSelect = new SingleSelect();
        expect(singleSelect.querySelector('select')).to.exist;
    });

    it('is constructed with an associated dropdown', function () {
        singleSelect = new SingleSelect();
        dropdown = getDropdownFor(singleSelect);
        expect(dropdown).to.be.an.instanceof(HTMLElement);
    });

    describe('should expose the value', function () {
        it('for getting', function () {
            singleSelect = new SingleSelect();
            expect(singleSelect.value).to.be.empty;
        });

        it('for setting', function () {
            singleSelect = createAndSkate('<aui-select><aui-option>Option 1</aui-option><aui-option>Option 2</aui-option><aui-option>Option 3</aui-option></aui-select>');
            singleSelect.value = 'Option 2';
            expect(singleSelect.value).to.equal('Option 2');
        });

        it('displayValue should be the text node', function () {
            singleSelect = createAndSkate('<aui-select><aui-option value="1">Option 1</aui-option><aui-option value="2">Option 2</aui-option><aui-option value="3">Option 3</aui-option></aui-select>');
            singleSelect.value = '2';
            expect(singleSelect.displayValue).to.equal('Option 2');
        });

        it('displayValue should not return the value in the option', function () {
            singleSelect = createAndSkate('<aui-select><aui-option value="1">Option 1</aui-option><aui-option value="2">Option 2</aui-option><aui-option value="3">Option 3</aui-option></aui-select>');
            singleSelect.value = '2';
            expect(singleSelect.displayValue).to.not.equal('2');
        });

        it('value should not return what is in the text node', function () {
            singleSelect = createAndSkate('<aui-select><aui-option value="1">Option 1</aui-option><aui-option value="2">Option 2</aui-option><aui-option value="3">Option 3</aui-option></aui-select>');
            singleSelect.value = '2';
            expect(singleSelect.value).to.not.equal('Option 2');
        });

        it('for setting based on actual value', function () {
            singleSelect = createAndSkate('<aui-select><aui-option value="1">Option 1</aui-option><aui-option>Option 2</aui-option><aui-option>Option 3</aui-option></aui-select>');
            singleSelect.value = '1';
            expect(singleSelect.value).to.equal('1');
        });
    });

    describe('should propagate the name attribute to the nested <select> form element', function () {
        beforeEach(function () {
            singleSelect = createAndSkate('<aui-select name="foo"></aui-select>');
        });

        it('when it is initialised', function () {
            singleSelect.querySelector('select').name.should.equal('foo');
        });

        it('when it changes', function (done) {
            singleSelect.setAttribute('name', 'bar');
            afterMutations(function () {
                singleSelect.querySelector('select').name.should.equal('bar');
                done();
            });
        });
    });

    describe('should add an id attribute to the nested <input> form element with the suffix ' + INPUT_SUFFIX, function () {
        // Because otherwise, we fail at accessibility if there is a <label> for the field.
        // Until we define an <aui-field> element, we'll have to deal with this here.

        beforeEach(function () {
            singleSelect = createAndSkate('<aui-select id="foo"></aui-select>');
        });

        it('when it is initialised', function () {
            expect(getInputFor(singleSelect).id).to.equal('foo' + INPUT_SUFFIX);
            expect(singleSelect.id).to.equal('foo');
        });

        it('when it changes (or is re-added)', function (done) {
            singleSelect.setAttribute('id', 'bar');
            afterMutations(function () {
                expect(getInputFor(singleSelect).id).to.equal('bar' + INPUT_SUFFIX);
                expect(singleSelect.id).to.equal('bar');
                done();
            });
        });
    });

    describe('when no <aui-options> are provided', function () {
        it('starts with no selected value', function () {
            singleSelect = createAndSkate('<aui-select></aui-select>');
            expect(singleSelect.value).to.be.empty;
        });
    });

    describe('when <aui-options> are provided', function () {
        it('should store user defined <aui-option>s', function () {
            singleSelect = createAndSkate('<aui-select><aui-option>One</aui-option><aui-option>Two</aui-option><aui-option>Three</aui-option></aui-select>');
            singleSelect.querySelectorAll('aui-option').length.should.equal(3);
        });

        it('starts with no selected value if none of the <aui-options> have the "selected" attribute', function () {
            singleSelect = createAndSkate('<aui-select><aui-option>One</aui-option><aui-option>Two</aui-option><aui-option>Three</aui-option></aui-select>');
            expect(singleSelect.value).to.be.empty;
        });

        it('starts with selected value of first <aui-option> with "selected" attribute', function () {
            // This is congruous with what the w3c spec says about the <select> element: http://www.w3.org/TR/html5/forms.html#dom-select-selectedindex
            singleSelect = createAndSkate('<aui-select><aui-option>One</aui-option><aui-option selected>Two</aui-option><aui-option selected>Three</aui-option></aui-select>');
            expect(singleSelect.value).to.equal('Two');
        });

        it('should preserve escaped HTML characters', function () {
            // This is congruous with what the w3c spec says about the <select> element: http://www.w3.org/TR/html5/forms.html#dom-select-selectedindex
            singleSelect = createAndSkate('<aui-select><aui-option selected>&lt;iframe src=&quot;https://www.atlassian.com&quot;&gt;&lt;/iframe&gt;</aui-option></aui-select>');
            expect(singleSelect.value).to.equal('&lt;iframe src=&quot;https://www.atlassian.com&quot;&gt;&lt;/iframe&gt;');
        });
    });

    it('when .focus()\'d, delegates focus to the input', function () {
        singleSelect = createAndSkate('<aui-select><aui-option>One</aui-option><aui-option selected>Two</aui-option><aui-option selected>Three</aui-option></aui-select>');
        focus(singleSelect);

        var input = getInputFor(singleSelect);
        expect(document.activeElement).to.equal(input);
    });
});

describe('aui/select - behaviour and interactions', function () {
    var singleSelect;
    var dropdown;
    var input;
    var trigger;

    beforeEach(function () {
        singleSelect = createAndSkate('<aui-select name="foo"><aui-option>Option 1</aui-option><aui-option>Option 2</aui-option><aui-option value="third-option">Third Option</aui-option></aui-select>');
        dropdown = getDropdownFor(singleSelect);
        input = getInputFor(singleSelect);
        trigger = getTriggerFor(singleSelect);
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    it('exposes an empty selected value', function () {
        expect(singleSelect.value).to.be.empty;
    });

    it('the dropdown should be open when the single select is focused', function () {
        focus(singleSelect);
        expect(dropdown).to.be.visible;
    });

    it('the dropdown should be closed when the single select is blurred', function () {
        focus(singleSelect);
        blur(singleSelect);
        expect(dropdown).to.not.be.visible;
    });

    it('the dropdown should be opened when the trigger is clicked', function () {
        click(trigger);
        expect(dropdown).to.be.visible;
    });

    it('the dropdown should be closed when the trigger is clicked', function () {
        trigger.click();
        blur(singleSelect);
        trigger.click();
        expect(dropdown).to.be.visible;
    });

    it('the dropdown should be closed when escape is pressed', function () {
        focus(singleSelect);
        pressKey(keyCode.ESCAPE);
        expect(dropdown).to.not.be.visible;
    });

    it('should not allow new values to be set by default', function () {
        singleSelect.value = 'New option';
        expect(singleSelect.value).to.be.empty;
    });

    describe('when the dropdown is shown via the keyboard after click focus', function () {
        beforeEach(function () {
            click(input);
            focus(input);
            pressKey(keyCode.DOWN);
            expect(dropdown).to.be.visible;
        });

        it('should close the dropdown when the value is set', function () {
            // Simulate tab: sending the TAB keypress by itself won't blur the field
            pressKey(keyCode.TAB);
            input.blur();

            expect(dropdown).to.not.be.visible;

            // Might've accidentally appeared again as a result of user input!
            clock.tick(1);
            expect(dropdown).to.not.be.visible;
        });
    });

    describe('when the dropdown is shown via the keyboard', function () {
        beforeEach(function () {
            focus(singleSelect);
        });

        it('highlights the first suggestion', function () {
            expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
        });

        it('closes the dropdown when pressing escape', function () {
            pressKey(keyCode.ESCAPE);
            expect(dropdown).to.not.be.visible;
        });

        it('retains typed value when escape is pressed', function () {
            fakeTypingOut('and more');
            pressKey(keyCode.ESCAPE);
            expect(input.value).to.equal('and more');
        });

        describe('and the middle suggestion is highlighted in the dropdown', function () {
            beforeEach(function () {
                pressKey(keyCode.DOWN);
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 2');
            });

            it('selects next item in dropdown when down arrow is pressed', function () {
                pressKey(keyCode.DOWN);
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Third Option');
            });

            it('selects previous item in dropdown when up arrow is pressed', function () {
                pressKey(keyCode.UP);
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
            });

            it('should highlight the best suggestion when autohighlighting is on', function () {
                focus(input);
                fakeTypingOut('o');
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 2');
                fakeBackspace();
                fakeTypingOut('t');
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Third Option');
                fakeBackspace();
                fakeTypingOut('o');
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
            });

            it('should retain "Option 2" when more options are made available from clearing the input', function () {
                focus(input);
                fakeBackspace();
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 2');
            });

            it('and the dropdown is closed, tabbed out of and selected again, the first suggestion is highlighted in the dropdown', function () {
                pressKey(keyCode.ESC);
                pressKey(keyCode.TAB);
                click(input);
                focus(input);
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
            });
        });

        describe('and the first suggestion is highlighted in the dropdown', function () {
            beforeEach(function () {
                singleSelect._suggestionModel.highlight(0); // Kinda cheating, kinda not.
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
            });

            it('does not loop to last suggestion when the up arrow is pressed', function () {
                pressKey(keyCode.UP);
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Option 1');
            });
        });

        describe('and the last suggestion is highlighted in the dropdown', function () {
            beforeEach(function () {
                singleSelect._suggestionModel.highlight(2); // Kinda cheating, kinda not.
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Third Option');
            });

            it('does not loop to first suggestion when the down arrow is pressed', function () {
                pressKey(keyCode.DOWN);
                expect(getHighlightedDropdownItem(dropdown).innerHTML).to.equal('Third Option');
            });
        });

        describe('and a valid suggestion is highlighted in the dropdown', function () {
            it('should set the value when the user presses enter', function () {
                pressKey(keyCode.ENTER);
                expect(singleSelect.value).to.equal('Option 1');
            });

            it('should set the value when the user presses tab', function () {
                pressKey(keyCode.TAB);
                expect(singleSelect.value).to.equal('Option 1');
            });

            it('should update the text in the <input> when the value is set', function () {
                pressKey(keyCode.ENTER);
                expect(input.value).to.equal('Option 1');
            });

            it('should close the dropdown when the value is set', function () {
                pressKey(keyCode.TAB);
                expect(dropdown).to.not.be.visible;

                // Might've accidentally appeared again as a result of user input!
                clock.tick(1);
                expect(dropdown).to.not.be.visible;
            });
        });

        describe('and no valid suggestions are highlighted', function () {
            var originalValue;
            var highlightedStub;

            beforeEach(function () {
                originalValue = singleSelect.value;
                fakeTypingOut('invalid option');
                highlightedStub = sinon.stub(singleSelect._suggestionModel, 'highlighted').returns(undefined);
            });

            afterEach(function () {
                highlightedStub.restore();
            });

            it('reverts value to original when enter is pressed', function () {
                pressKey(keyCode.ENTER);
                expect(singleSelect.value).to.equal(originalValue);
            });

            it('reverts value to original when tab is pressed', function () {
                pressKey(keyCode.TAB);
                expect(singleSelect.value).to.equal(originalValue);
            });

        });
    });

    describe('when the dropdown is shown via the mouse', function () {
        beforeEach(function () {
            click(getTriggerFor(singleSelect));
        });

        it('should highlight the first suggestion', function () {
            expect(getHighlightedDropdownItem(dropdown)).to.not.be.null;
        });

        it('does not hide the dropdown when the user clicks the <input>', function () {
            var input = getInputFor(singleSelect);

            click(input);
            expect(dropdown).to.be.visible;
        });

        it('does not hide the dropdown when the user clicks the dropdown container', function () {
            var ddContainer = getDropdownFor(singleSelect);

            click(ddContainer);
            expect(dropdown).to.be.visible;
        });

        it('hides the dropdown when the user clicks outside the <input> and dropdown container', function () {
            click(document.body);
            expect(dropdown).to.not.be.visible;
        });

        it('reverts value to original when the clicks outside the <input> and dropdown container', function () {
            singleSelect.value = 'Option 2';
            fakeTypingOut('Opt 1');
            click(document.body);
            expect(singleSelect.value).to.equal('Option 2');
        });

        it('does not highlight any suggestions when the trigger is clicked', function () {
            expect(getHighlightedDropdownItem(dropdown)).to.not.be.defined;
        });

        it('should focus the input box when the trigger is clicked', function () {
            expect(getInputFor(singleSelect)).to.be.equal(document.activeElement);
        });

        it('shows all results', function () {
            var results = getResultsAsArray(singleSelect);
            expect(results.length).to.equal(3);
        });

        describe('and the input is focused and blurred, then the trigger is clicked', function () {
            beforeEach(function () {
                focus(singleSelect);
                blur(singleSelect);
                trigger.click();
            });

            it('the dropdown should be visible', function () {
                expect(dropdown).to.be.visible;
            });

            describe('twice', function () {
                beforeEach(function () {
                    click(getTriggerFor(singleSelect));
                });

                it('the dropdown should be visible', function () {
                    expect(dropdown).to.be.visible;
                });
            });
        });

        describe('and the mouse is over a suggestion', function () {
            var currentOption;

            beforeEach(function () {
                currentOption = getDropdownFor(singleSelect).querySelector('li:nth-child(2)');
            });

            it('should highlight that suggestion', function () {
                currentOption.dispatchEvent(new CustomEvent('mouseover', {bubbles: true}));
                expect(singleSelect._suggestionModel.highlightedIndex()).to.equal(1);
            });

            it('should not highlight a suggestion if the dropdown is closed and reopened', function () {
                currentOption.dispatchEvent(new CustomEvent('mouseover', {bubbles: true}));
                expect(singleSelect._suggestionModel.highlightedIndex()).to.equal(1);
                click(document.body);
                click(getTriggerFor(singleSelect));
                expect(getHighlightedDropdownItem(dropdown)).to.not.be.defined;
            });

            describe('when the suggestion is clicked', function () {
                it('should select that suggestion', function () {
                    var newOption = getDropdownFor(singleSelect).querySelector('li:nth-child(1)');
                    mousedown(newOption);
                    expect(singleSelect.value).to.equal(newOption.textContent);
                });

                it('should close the dropdown', function () {
                    mousedown(currentOption);
                    expect(dropdown).to.not.be.visible;
                });
            });
        });
    });

    describe('with a selected value', function () {
        beforeEach(function () {
            focus(singleSelect);
            pressKey(keyCode.DOWN);
            pressKey(keyCode.ENTER);
        });

        it('should return the selected value', function () {
            expect(singleSelect.value).to.equal('Option 2');
        });

        it('is opened by typing a character it should display the selected value in the list', function () {
            var selectedValue = singleSelect.value;
            var results = getResultsForTyping(singleSelect, 'O');
            expect(isInResults(results, selectedValue)).to.equal(true);
        });

        it('is opened by the down arrow and a charcter is typed it should display the selected value in the list', function () {
            pressKey(keyCode.DOWN);
            var selectedValue = singleSelect.value;
            var results = getResultsForTyping(singleSelect, 'O');
            expect(isInResults(results, selectedValue)).to.equal(true);

        });

        it('is opened by the down arrow it should not display the selected value in the list', function () {
            var selectedValue = singleSelect.value;
            pressKey(keyCode.DOWN);
            var results = getResultsAsArray(singleSelect);
            expect(isInResults(results, selectedValue)).to.equal(false);
        });

        it('is opened by click it should not display the selected value in the list', function () {
            var selectedValue = singleSelect.value;
            click(getTriggerFor(singleSelect));
            var results = getResultsAsArray(singleSelect);
            expect(isInResults(results, selectedValue)).to.equal(false);
            expect(results.length).to.equal(2);
        });

        it('typing "abc" and blurring should leave the display value as "abc" and the actual value as ""', function () {
            getResultsForTyping(singleSelect, 'abc');
            blur(input);
            expect(singleSelect.value).to.equal('');
            expect(singleSelect.displayValue).to.equal('abc');
        });

        it('pressing escape when the dropdown is closed should keep the currently selected value', function () {
            pressKey(keyCode.ESCAPE);
            expect(input.value).to.equal('Option 2');
            expect(singleSelect.value).to.equal('Option 2');
        });

        it('types "doge" and then clicks the "no suggestions" item the value should not change', function () {
            getResultsForTyping(singleSelect, 'doge');
            var oldValue = singleSelect.value;
            mousedown(dropdown.querySelector('li'));
            expect(singleSelect.value).to.be.equal(oldValue);
        });

        it('deleting all text should not expand the dropdown', function () {
            fakeClear(keyCode.BACKSPACE);
            expect(dropdown).to.not.be.visible;
        });

        describe('in a form', function () {
            var form;

            beforeEach(function () {
                form = document.createElement('form');
                form.setAttribute('id', 'test-form');
                form.appendChild(singleSelect);
                document.body.appendChild(form);
            });

            afterEach(function () {
                $('#test-form').remove();
            });

            it('serialises the option label if no value is provided', function () {
                const data = $(form).serializeArray();
                expect(data[0]).to.have.property('name', 'foo');
                expect(data[0]).to.have.property('value', 'Option 2');
            });

            it('serialises the option value if one is provided', function () {
                singleSelect.value = 'Third Option';
                const data = $(form).serializeArray();
                expect(data[0]).to.have.property('name', 'foo');
                expect(data[0]).to.have.property('value', 'third-option');
            });
        });

        describe('and the select is opened again', function () {
            beforeEach(function () {
                focus(input);
                pressKey(keyCode.DOWN);
            });

            it('the selected value should not be shown in results', function () {
                var results = getResultsAsArray(singleSelect);
                expect(results).to.not.contain(singleSelect.value);
            });
        });

        describe('and input field is cleared', function () {
            beforeEach(function () {
                fakeClear('a', input);
                pressKey(keyCode.DOWN, input);
            });

            it('results should include the entire list including currently selected value', function () {
                var results = getResultsAsArray(singleSelect);
                expect(results.length).to.equal(3);
            });

            it('blurring should leave the value as ""', function () {
                blur(input);
                expect(singleSelect.value).to.equal('');
            });
        });
    });

    describe('no space behaviours', function () {
        let popper;
        beforeEach(function () {
            popper = mockPopper();
        });

        afterEach(() => {
            popper.restore();
        });

        it('displays the dropdown below the input when the container is large and there is space below it', function () {
            var height = $(window).height();
            resizeWindow(height);
            positionElement(singleSelect, 0);
            focus(singleSelect);
            expect(dropdown).to.be.visible;
            expect(positionOf(dropdown).isBelow(input)).to.be.true;
        });

        it('displays the dropdown below the input when the container is large but there is no space below it', function () {
            var height = $(window).height();
            resizeWindow(height);
            positionElement(singleSelect, height - 100);
            focus(singleSelect);
            expect(dropdown).to.be.visible;
            expect(positionOf(dropdown).isBelow(input)).to.be.true;
        });
    });
});

describe('aui/select that cannot be empty', function () {
    var singleSelect;
    var input;

    beforeEach(function () {
        singleSelect = createAndSkate(
            '<aui-select no-empty-values name="foo">' +
                '<aui-option>Option 1</aui-option>' +
                '<aui-option>Option 2</aui-option>' +
                '<aui-option value="third-option">Third Option</aui-option>' +
            '</aui-select>'
        );
        input = getInputFor(singleSelect);
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    it('blurring should restore the original value', function () {
        focus(singleSelect);
        pressKey(keyCode.DOWN);
        pressKey(keyCode.ENTER);

        expect(singleSelect.value).to.equal('Option 2');

        fakeClear('a', input);
        pressKey(keyCode.DOWN, input);

        blur(input);
        expect(singleSelect.value).to.equal('Option 2');
    });
});

describe('aui/select that can create new values', function () {
    var singleSelect;
    var input;

    beforeEach(function () {
        singleSelect = createAndSkate(
            '<aui-select can-create-values name="foo">' +
                '<aui-option>Option 1</aui-option>' +
                '<aui-option>Option 2</aui-option>' +
                '<aui-option value="third-option">Third Option</aui-option>' +
                '</aui-select>'
        );
        input = getInputFor(singleSelect);
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    it('allows new values to be selected', function () {
        focus(singleSelect);
        fakeTypingOut('New option');
        expect(getResultsAsArray(singleSelect).length).to.equal(1);

        pressKey(keyCode.ENTER);
        expect(singleSelect.value).to.equal('New option');
    });

    it('does not offer to create a new value if the match is exact', function () {
        focus(singleSelect);
        fakeTypingOut('Option');
        expect(getResultsAsArray(singleSelect).length).to.equal(3);

        fakeTypingOut(' 1');
        expect(getResultsAsArray(singleSelect).length).to.equal(1);
    });

    it('does not offer to create a value if the input is empty', function () {
        focus(singleSelect);
        expect(getResultsAsArray(singleSelect).length).to.equal(3);
    });

    it('allows new values to be selected programmatically', function () {
        singleSelect.value = 'New option';
        expect(singleSelect.value).to.equal('New option');
        expect(input.value).to.equal('New option');
    });

    it('does not create a new value programmatically if the input is empty', function () {
        singleSelect.value = '';
        expect(singleSelect.value).to.be.empty;

        focus(singleSelect);
        expect(getResultsAsArray(singleSelect).length).to.equal(3);
    });

    it('deselects the selection and clears the displayed value if the input is set to empty programmatically', function () {
        focus(singleSelect);
        fakeTypingOut('New option');
        expect(getResultsAsArray(singleSelect).length).to.equal(1);

        pressKey(keyCode.ENTER);
        expect(singleSelect.value).to.equal('New option');

        singleSelect.value = '';
        expect(singleSelect.value).to.be.empty;
        expect(input.value).to.be.empty;

        focus(singleSelect);
        expect(getResultsAsArray(singleSelect).length).to.equal(3);
    });
});

describe('aui/select with values specified for options, with the second option selected by default', function () {
    let singleSelect;
    let input;

    beforeEach(function () {
        const form = $('<form class="aui">');
        form.appendTo(document.getElementById('test-fixture'));
        singleSelect = createAndSkate(
            '<aui-select with-option-values name="foo">' +
            '<aui-option value="option1">Option 1</aui-option>' +
            '<aui-option selected value="option2">Option 2</aui-option>' +
            '<aui-option value="option3">Option 3</aui-option>' +
            '</aui-select>'
        );
        input = getInputFor(singleSelect);
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    it('returns the correct value and displayValue from the single select', function () {
        expect(singleSelect.value).to.equal('option2');
        expect(singleSelect.displayValue).to.equal('Option 2');
    });

    it('opening the dropdown shows a dropdown with the rest of the options', function () {
        focus(singleSelect);

        const results = getResultsAsArray(singleSelect);
        expect(isInResults(results, 'Option 1')).to.equal(true);
        expect(isInResults(results, 'Option 3')).to.equal(true);
    });

    describe('selecting the first option', function () {
        beforeEach(function (done) {
            afterMutations(function () {
                focus(singleSelect);
                afterMutations(function () {
                    pressKey(keyCode.ENTER);
                    afterMutations(function () {
                        done();
                    }, 100);
                }, 100);
            }, 100);
        });

        it('shows the label and not the value in the input box', function () {
            expect(input.value).to.equal('Option 1');
        });

        it('returns the correct value and displayValue from the single select', function (done) {
            afterMutations(function () {
                expect(singleSelect.value).to.equal('option1');
                expect(singleSelect.displayValue).to.equal('Option 1');
                done();
            }, 50);
        });

        it ('shows the correct options in the dropdown', function () {
            focus(singleSelect);

            const results = getResultsAsArray(singleSelect);
            expect(isInResults(results, 'Option 2')).to.equal(true);
            expect(isInResults(results, 'Option 3')).to.equal(true);
        });
    });


});

describe('aui/select matching', function () {
    var singleSelect;
    var input;
    var allResultsLength;

    beforeEach(function () {
        clock = sinon.useFakeTimers();
        singleSelect = createAndSkate([
            '<aui-select>',
            '<aui-option>zero</aui-option>',
            '<aui-option>one</aui-option>',
            '<aui-option>two</aui-option>',
            '<aui-option>three</aui-option>',
            '<aui-option>four</aui-option>',
            '<aui-option>five</aui-option>',
            '<aui-option>twenty</aui-option>',
            '<aui-option>twenty one</aui-option>',
            '<aui-option>one hundred</aui-option>',
            '</aui-select>'
        ].join(''));

        input = getInputFor(singleSelect);
        allResultsLength = singleSelect.querySelectorAll('aui-option').length;
    });

    afterEach(function () {
        clock.restore();
    });

    describe('with the default prefix matcher', function () {
        describe('user types "z"', function () {
            it('should see a list of one result containing the option "zero"', function () {
                var results = getResultsForTyping(singleSelect, 'z');

                expect(results.length).to.equal(1);
                expect(results).to.include.members(['zero']);
            });
        });

        describe('user types "f"', function () {
            it('should see list of two results containing "four" and "five"', function () {
                var results = getResultsForTyping(singleSelect, 'f');

                expect(results.length).to.equal(2);
                expect(results).to.include.members(['four', 'five']);
            });
        });

        describe('user types "four"', function () {
            it('should see a list of one result containing the option "four"', function () {
                var results = getResultsForTyping(singleSelect, 'four');

                expect(results.length).to.equal(1);
                expect(results).to.include.members(['four']);
            });
        });

        describe('user types "one"', function () {
            var results;

            beforeEach(function () {
                results = getResultsForTyping(singleSelect, 'one');
            });

            it('should see list of two results containing "one" and "one hundred"', function () {
                expect(results.length).to.equal(2);
                expect(results).to.include.members(['one', 'one hundred']);
            });

            it('should not see "twenty one"', function () {
                expect(results).to.not.include.members(['twenty one']);
            });
        });

        describe('user types "one hundred"', function () {
            var results;

            beforeEach(function () {
                results = getResultsForTyping(singleSelect, 'one hundred');
            });

            it('should see "one hundred" in the suggestions list', function () {
                expect(results.length).to.equal(1);
                expect(results).to.include.members(['one hundred']);
            });

            it('should not see "one"', function () {
                expect(results).to.not.include.members(['one']);
            });

            describe('then clears', function () {
                beforeEach(function () {
                    pressKey(keyCode.ENTER);
                    fakeClear(input);
                });

                it('and blurs input, should see "" and the value should be ""', function () {
                    blur(input);
                    clock.tick(500);
                    expect(singleSelect.value).to.equal('');
                    expect(singleSelect.displayValue).to.equal('');
                });

                it('and types "one", then blurs input, should see "one" in the input, but the value should be ""', function () {
                    fakeTypingOut('one');
                    blur(input);
                    expect(singleSelect.value).to.equal('');
                    expect(singleSelect.displayValue).to.equal('one');
                });

                it('and types "jibberish", then blurs input, should see "jibberish" in input, and the value should be ""', function () {
                    fakeTypingOut('jibberish');
                    blur(input);
                    expect(singleSelect.value).to.equal('');
                    expect(singleSelect.displayValue).to.equal('jibberish');
                });
            });
        });

        describe('user types "a"', function () {
            var dropdown;

            beforeEach(function () {
                getResultsForTyping(singleSelect, 'a');
                dropdown = singleSelect._dropdown;
            });

            it('should see a no suggestions message in the dropdown', function () {
                expect(hasNoSuggestions(singleSelect)).to.equal(true);
            });

            it('and then clicks the "no suggestions" item the dropdown should still be visible', function () {
                mousedown(dropdown.querySelector('li'));
                expect(dropdown).to.be.visible;
            });

            it('and then clicks the "no suggestions" item the single select value should not change', function () {
                var oldValue = singleSelect.value;
                mousedown(dropdown.querySelector('li'));
                expect(singleSelect.value).to.equal(oldValue);
            });

            it('and hovers mouse over the "no suggestions" item the item should not be highlighted', function () {
                hover(dropdown.querySelector('li'));
                expect(singleSelect._suggestionModel.highlightedIndex()).to.equal(-1);
            });

            it('and presses the down arrow the item should not be highlighted', function () {
                pressKey(keyCode.DOWN);
                expect(singleSelect._suggestionModel.highlightedIndex()).to.equal(-1);
            });

            it('and then clicks the "no suggestions" item the single select value should not change', function () {
                var oldValue = singleSelect.value;
                mousedown(dropdown.querySelector('li'));
                expect(singleSelect.value).to.equal(oldValue);
            });

            it('blurs, re-focuses the input, deletes all the content and then presses DOWN the dropdown should display all results', function () {
                blur(singleSelect._input);
                focus(singleSelect._input);
                fakeClear(singleSelect._input);
                pressKey(keyCode.DOWN);
                expect(dropdown).to.be.visible;
                expect(getResultsAsArray(singleSelect).length).to.equal(allResultsLength);
            });

        });

        // because we're not doing any word breaks... yet)!
        describe('user types "hundred"', function () {
            it('should see a no suggestions message in the dropdown???', function () {
                getResultsForTyping(singleSelect, 'hundred');
                expect(hasNoSuggestions(singleSelect)).to.equal(true);
            });
        });
    });
});

describe('aui/select async', function () {
    var select;
    var dropdown;
    var server;
    var clock;
    var input;
    var numbersResponse = [{
        label: 'One',
        value: 'one'
    }, {
        label: 'Two',
        value: 'two'
    }, {
        label: 'Three',
        value: 'three'
    }];

    function setUpSingleSelectAsync (response, optionHtml) {
        select = createAndSkate('<aui-select src="get-select-options">' + (optionHtml || '') + '</aui-select>');
        dropdown = getDropdownFor(select);
        input = getInputFor(select);
        server = sinon.fakeServer.create();
        clock = sinon.useFakeTimers();

        input.value = '';
        focus(input);
        respondWithJson(server, response);
    }

    afterEach(function () {
        server.restore();
        clock.restore();
    });

    describe('when the dropdown is opened', function () {
        beforeEach(function () {
            setUpSingleSelectAsync(numbersResponse);
        });

        describe('user types "a"', function () {
            it('should see a no suggestions message in the dropdown', function () {
                fakeTypingOut('a');
                server.respond();
                expect(hasNoSuggestions(select)).to.equal(true);
            });
        });

        describe('user types "t"', function () {
            it('should show "two" and "three"', function () {
                fakeTypingOut('t');
                server.respond();

                var results = getResultsAsArray(select);

                expect(results.length).to.equal(2);
                expect(results).to.include.members(['Two', 'Three']);
            });

            it('should set aria-busy to true until async result returns', function () {
                fakeTypingOut('t');
                expect(input.getAttribute('aria-busy')).to.equal('true');
                server.respond();
                expect(input.getAttribute('aria-busy')).to.equal('false');
            });
        });

        describe('user types "th"', function () {
            it('should show "three"', function () {
                fakeTypingOut('th');
                server.respond();

                var results = getResultsAsArray(select);

                expect(results.length).to.equal(1);
                expect(results).to.include.members(['Three']);
            });
        });

        describe('user types while a previous request is still loading', function () {
            var tServer;
            var thServer;

            beforeEach(function (done) {
                setUpSingleSelectAsync(numbersResponse, '<aui-option>One</aui-option>');

                tServer = sinon.fakeServer.create();
                thServer = sinon.fakeServer.create();

                respondWithJson(tServer, [{label: 'Two'}, {label: 'Three'}]);
                respondWithJson(thServer, [{label: 'Three'}]);
                focus(getInputFor(select));
                afterMutations(done);
            });

            afterEach(function () {
                tServer.restore();
                thServer.restore();
            });

            it('types: t, h', function () {
                var results;

                fakeTypingOut('t');
                fakeTypingOut('h');

                thServer.respond();
                results = getResultsAsArray(select);
                expect(results.length).to.equal(1);
                expect(results).to.include.members(['Three']);

                tServer.respond();
                results = getResultsAsArray(select);
                expect(results.length).to.equal(1);
                expect(results).to.include.members(['Three']);
            });

            it('should maintain aria-busy as true until final response returns', function () {
                fakeTypingOut('t');
                fakeTypingOut('h');
                expect(input.getAttribute('aria-busy')).to.equal('true');
                tServer.respond();
                expect(input.getAttribute('aria-busy')).to.equal('true');
                thServer.respond();
                expect(input.getAttribute('aria-busy')).to.equal('false');
            });

            it('types: t, backspace', function () {
                fakeTypingOut('t');
                pressKey(keyCode.ESCAPE);

                tServer.respond();
                expect(getResultsAsArray(select).length).to.equal(0);
            });

            it('types: t, h, backspace', function () {
                var results;

                fakeTypingOut('t');
                fakeTypingOut('h');
                fakeBackspace();

                thServer.respond();
                results = getResultsAsArray(select);
                expect(results.length).to.equal(1);
                expect(results).to.include.members(['Three']);

                tServer.respond();
                results = getResultsAsArray(select);
                expect(results.length).to.equal(1);
                expect(results).to.include.members(['Three']);
            });

            it('types: t, h, backspace, h', function () {
                fakeTypingOut('t');
                fakeTypingOut('h');
                fakeBackspace();
                fakeTypingOut('h');

                thServer.respond();
                expect(getResultsAsArray(select).length).to.equal(1);

                tServer.respond();
                expect(getResultsAsArray(select).length).to.equal(1);
            });

            it('types: t, h, backspace, escape', function () {
                fakeTypingOut('t');
                fakeTypingOut('h');
                fakeBackspace();
                fakeClear();

                clock.tick();

                thServer.respond();
                expect(dropdown.getAttribute('aria-hidden')).to.equal('true');

                tServer.respond();
                expect(dropdown.getAttribute('aria-hidden')).to.equal('true');
            });
        });
    });

    describe('with sync default values', function () {
        var response = [{
            label: 'Aquarium',
            value: 'Aquarium'
        }, {
            label: 'Aquatic',
            value: 'Aquatic'
        }, {
            label: 'Aqua',
            value: 'Aqua'
        }];

        beforeEach(function () {
            setUpSingleSelectAsync(response, '<aui-option>Apple</aui-option><aui-option>Banana</aui-option>');
        });

        describe('types: a', function () {
            beforeEach(function () {
                focus(input);
                pressKey(keyCode.DOWN);
                fakeTypingOut('a');
            });

            it('should provide a message for assistive div when async response returns with more suggestions', function () {
                expect(select.querySelector('.aui-select-status').innerHTML).to.not.be.defined;
                server.respond();
                expect(select.querySelector('.aui-select-status').innerHTML).to.equal(I18n.getText('aui.select.new.suggestions'));
            });
        });

        describe('types: b', function () {
            beforeEach(function () {
                fakeTypingOut('b');
            });

            it('should not provide a message for assistive div when async response returns with no suggestions', function () {
                expect(select.querySelector('.aui-select-status').innerHTML).to.not.be.defined;
                server.respond();
                expect(select.querySelector('.aui-select-status').innerHTML).to.not.be.defined;
            });
        });

        describe('types: a, q, backspace, backspace before server returns', function () {
            beforeEach(function () {
                focus(input);
                fakeTypingOut('a');
                // Backspace and wait for event to register.
                fakeBackspace();
                clock.tick(1);
            });

            it('should only append one set of "Apple" and "Banana"', function () {
                server.respond();
                // expand dropdown
                pressKey(keyCode.DOWN);
                expect(getResultsAsArray(select).length).to.equal(2);
            });
        });
    });

    describe('with images', function () {
        var imagesResponse = [{
            label: 'Blue',
            value: 'blue',
            'img-src': 'blue.png'
        }, {
            label: 'Red',
            value: 'red',
            'img-src': 'red.jpg'
        }, {
            label: 'Green',
            value: 'green',
            'img-src': 'green.png'
        }];

        beforeEach(function () {
            setUpSingleSelectAsync(imagesResponse, '<aui-option img-src="yellow.gif">Yellow</aui-option>');
        });

        describe('types: y', function () {
            beforeEach(function () {
                fakeTypingOut('y');
            });

            it('an image is present in the dropdown list', function () {
                var image = $(dropdown).find('img')[0];
                expect(image).to.be.an.instanceof(HTMLElement);
                expect(image.getAttribute('src')).to.equal('yellow.gif');
            });
        });

        describe('types: r', function () {
            function getBackgroundImageSource(input) {
                var backgroundImage = $(input).css('background-image');
                var urlMatch = backgroundImage.match(/url\(('|")?([^'"\)]+)('|")?\)/);
                if (urlMatch) {
                    var url = urlMatch[2];
                    return url.substring(url.lastIndexOf('/') + 1);
                } else if (backgroundImage === 'none') {
                    return 'none';
                }
            }

            function hasInlineImageClass(input) {
                return $(input).hasClass('aui-select-has-inline-image');
            }

            beforeEach(function () {
                fakeTypingOut('r');
                server.respond();
            });

            it('an image is present in the dropdown list', function () {
                var image = $(dropdown).find('img')[0];
                expect(image).to.be.an.instanceof(HTMLElement);
                expect(image.getAttribute('src')).to.equal('red.jpg');
            });

            describe('and the first result is clicked', function () {
                beforeEach(function () {
                    mousedown(dropdown.querySelector('li'));
                });

                it('then the input is preceded by an image', function () {
                    expect(getBackgroundImageSource(input)).to.equal('red.jpg');
                    expect(hasInlineImageClass(input)).to.be.true;
                });

                it('then we backspace and blur clears the image', function () {
                    fakeBackspace();
                    blur(input);
                    expect(getBackgroundImageSource(input)).to.equal('none');
                    expect(hasInlineImageClass(input)).to.be.false;
                });

                it('then we backspace and blur leaves the text behind', function () {
                    fakeBackspace();
                    blur(input);
                    expect(input.value).to.equal('Re');
                });
            });
        });
    });
});

describe('aui/select - focus and press down', function () {
    var select;

    beforeEach(function () {
        const constructed = fixtures({
            select: '<aui-select><aui-option>1</aui-option><aui-option>2</aui-option><aui-option>3</aui-option></aui-select>'
        });
        select = constructed.select;

        skate.init(select);
        focus(select);
    });

    it('should be open', function () {
        expect(select._dropdown.getAttribute('aria-hidden')).to.equal('false');
    });

    it('should select the first item when opened with the keyboard', function () {
        expect(select._dropdown.querySelector('.aui-select-active').textContent).to.equal('1');
    });

    it('should reset the selected item when closed', function () {
        pressKey(keyCode.ESCAPE);
        pressKey(keyCode.DOWN);
        expect(select._dropdown.querySelector('.aui-select-active').textContent).to.equal('1');
    });
});

describe('aui/select - click trigger', function () {
    var select;

    beforeEach(function () {
        const constructed = fixtures({
            select: '<aui-select><aui-option>1</aui-option><aui-option>2</aui-option><aui-option>3</aui-option></aui-select>'
        });
        select = constructed.select;

        skate.init(select);
        select._button.click();
    });

    it('dropdown should be open', function () {
        expect(select._dropdown.getAttribute('aria-hidden')).to.equal('false');
    });

    it('should highlight the first item', function () {
        expect(select._dropdown.querySelector('.aui-select-active').textContent).to.equal('1');
    });
});

describe('aui/select - placeholder', function () {
    it('should project the placeholder attribute onto the input element', function (done) {
        var ss = new SingleSelect();
        ss.setAttribute('placeholder', 'my placeholder');
        afterMutations(() => {
            expect(ss.querySelector('input').getAttribute('placeholder')).to.equal('my placeholder');
            done();
        });
    });
});

describe('aui/select', function () {
    describe('value', function () {
        var select;

        beforeEach(function (done) {
            select = fixtures({
                select: '<aui-select><aui-option>yes</aui-option></aui-select>'
            }).select;
            afterMutations(done);
        });

        it('setting should not work if value is not an option', function (done) {
            select.value = 'no';
            afterMutations(function () {
                expect(select._input.value).to.equal('');
                done();
            });
        });

        it('setting should work if value is an option', function (done) {
            select.value = 'yes';
            afterMutations(function () {
                expect(select._input.value).to.equal('yes');
                done();
            });
        });
    });

    describe('with many options', function () {
        let singleSelect;
        const defaultVersion = '2.2.2';
        let versions;

        beforeEach(function (done) {

            // generate 130 options
            versions = [];
            /* eslint max-depth: [2, 5] */
            for (let major = 0; major < 5; major++) {
                for (let minor = 0; minor < 5; minor++) {
                    for (let patch = 0; patch < 5; patch++) {
                        let version = `${major}.${minor}.${patch}`;
                        versions.push(version);
                        if (version === defaultVersion) {
                            for (let beta = 0; beta < 5; beta++) {
                                versions.push(`${major}.${minor}.${patch}-beta${beta}`);
                            }
                        }
                    }
                }
            }

            let options = versions.map(version => {
                let selected = version === defaultVersion ? ' selected' : '';
                return `<aui-option${selected}>${version}</aui-option>`;
            });

            singleSelect = createAndSkate(`<aui-select>${options}</aui-select>`);
            focus(singleSelect);
            afterMutations(done);
            clock = sinon.useFakeTimers();
        });

        afterEach(() => {
            clock.restore();
        });

        describe('should show the correct suggestions', function() {

            it('initially show all because we have a selection', function() {
                clock.tick(200);
                expect(getResultsAsArray(singleSelect)).to.eql(versions.filter(version => version !== defaultVersion));
            });

            it('after backspace', function() {
                const input = getInputFor(singleSelect);
                focus(input);
                fakeBackspace(input);

                expect(getResultsAsArray(singleSelect)).to.eql([
                    '2.2.0',
                    '2.2.1',
                    '2.2.2',
                    '2.2.2-beta0',
                    '2.2.2-beta1',
                    '2.2.2-beta2',
                    '2.2.2-beta3',
                    '2.2.2-beta4',
                    '2.2.3',
                    '2.2.4'
                ]);
            });

            it('after typing', function() {
                expect(getResultsForTyping(singleSelect, defaultVersion)).to.eql([
                    '2.2.2',
                    '2.2.2-beta0',
                    '2.2.2-beta1',
                    '2.2.2-beta2',
                    '2.2.2-beta3',
                    '2.2.2-beta4'
                ]);
            });
        });
    });

    describe('blur', function() {

        let singleSelect;
        let trigger;
        let input2;

        beforeEach(function () {
            let constructed = fixtures({
                select: '<aui-select><aui-option>Option 1</aui-option><aui-option>Option 2</aui-option><aui-option>Option 3</aui-option></aui-select>',
                input2: '<input type="text" value="focus me to trigger aui-select blur" />'
            });
            skate.init(constructed.select);
            singleSelect = constructed.select;
            trigger = getTriggerFor(singleSelect);
            input2 = constructed.input2;
        });

        it('should retain the chosen value in the select element when blurred', function (done) {
            click(trigger);
            pressKey(keyCode.DOWN);
            pressKey(keyCode.ENTER);
            focus(input2);
            afterMutations(function () {
                const selectElement = singleSelect.querySelector('select');
                expect(singleSelect.value).to.equal('Option 2');
                expect(selectElement.options.length).to.equal(1);
                expect(selectElement.selectedIndex).to.equal(0);
                expect(selectElement.options[selectElement.selectedIndex].textContent).to.equal('Option 2');
                done();
            });
        });

    });

    describe('bindSelectMousedown', function () {
        it('should prevent closing all layers on select close if nested in layered element', function () {
            const containerLayer = $('<div class="aui-layer"></div>')[0];
            const el = $('<div>el</div>')[0];
            const dropdown = $('<div></div>')[0];
            const li = $('<li></li>')[0];
            el._dropdown = dropdown;
            el._suggestionModel = {
                getNumberOfResults: () => 1,
                highlight: () => {},
                highlighted: () => {}
            };
            el._suggestionsView = {
                hide: () => {}
            };
            el._input = $('<div></div>')[0];

            containerLayer.appendChild(el);
            dropdown.appendChild(li);
            bindSelectMousedown(el);

            $(li).trigger($.Event('mousedown'));
            const e = $.Event('aui-close-layers-on-outer-click');
            $(document).trigger(e);
            expectEvent.isPrevented(e);
        });
    });
});
