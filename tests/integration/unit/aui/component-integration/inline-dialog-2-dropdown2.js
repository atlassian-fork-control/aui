import skate from '@atlassian/aui/src/js/aui/internal/skate';
import $ from '@atlassian/aui/src/js/aui/jquery';
import {click, hover} from '../../../helpers/all';
import '@atlassian/aui/src/js/aui/inline-dialog2';
import {expectDialogOpenAfterTimeout, generateInlineDialog2, sendMouseEnterMessage} from './integration-helpers';

describe('inline dialog 2 with dropdown2 integration', function () {
    let inlineDialog;
    beforeEach(function () {
        const dropdownMarkup = ` <button id="dropdown2-trigger" class="aui-button aui-dropdown2-trigger" aria-controls="example-dropdown">
                                        Dropdown menu
                                    </button>

                                    <aui-dropdown-menu id="example-dropdown">
                                        <aui-section label="Links">
                                            <aui-item-link href="http://atlassian.com">Atlassian</aui-item-link>
                                            <aui-item-link href="http://news.ycombinator.com">Hacker news</aui-item-link>
                                            <aui-item-link href="http://github.com">Github</aui-item-link>
                                        </aui-section>
                                        <aui-section label="Browsers">
                                            <aui-item-radio interactive checked>Chrome</aui-item-radio>
                                            <aui-item-radio interactive>Firefox</aui-item-radio>
                                            <aui-item-radio interactive disabled>Safari</aui-item-radio>
                                        </aui-section>
                                        <aui-section label="Languages">
                                            <aui-item-checkbox interactive checked>Javascript</aui-item-checkbox>
                                            <aui-item-checkbox interactive>Fortran</aui-item-checkbox>
                                            <aui-item-checkbox interactive>Rust</aui-item-checkbox>
                                        </aui-section>
                                    </aui-dropdown-menu>`;

        const inlineDialogId = 'inline-dialog2-visible';
        const fixtures = generateInlineDialog2(inlineDialogId, dropdownMarkup);
        skate.init(fixtures.trigger);
        skate.init(fixtures.inlineDialog);
        inlineDialog = fixtures.inlineDialog;
    });

    it('stays open when hovering on options', function (done) {
        sendMouseEnterMessage(inlineDialog);

        $('#dropdown2-trigger').click();
        hover($('aui-item-link').get(2));

        expectDialogOpenAfterTimeout(inlineDialog, done);
    });

    it('stays open after interacting with dropdown', function (done) {
        sendMouseEnterMessage(inlineDialog);

        $('#dropdown2-trigger').click();
        click($('aui-item-radio').get(1));

        expectDialogOpenAfterTimeout(inlineDialog, done);
    });
});
