import '@atlassian/aui/src/js/aui/dropdown2';
import '@atlassian/aui/src/js/aui/button';
import $ from '@atlassian/aui/src/js/aui/jquery';
import { click, fixtures, focus, pressKey } from '../../helpers/all';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import keyCode from '@atlassian/aui/src/js/aui/key-code';

describe('aui/button', function () {
    let button;

    describe('- button group', function () {
        describe('with a dropdown2 trigger adds and removes the aui-dropdown2-in-buttons class', function () {
            let trigger;
            let dropdown;

            function expectButtonsClass(dropdown, isExpected, summary) {
                expect(dropdown.classList.contains('aui-dropdown2-in-buttons')).to.equal(isExpected, summary);
            }

            beforeEach(function () {
                const constructed = fixtures({
                    buttons: `<div class="aui-buttons">
                            <button id="trigger-button" class="aui-button aui-dropdown2-trigger" aria-haspopup="true" aria-owns="test-dropdown">Dropdown trigger</button>
                        </div>`,
                    dropdown: `<div id="test-dropdown" class="aui-style-default aui-dropdown2">
                                <ul><li><a href="#">Test item</a></li></ul>
                            </div>`
                });
                trigger = constructed.buttons.querySelector('#trigger-button');
                dropdown = constructed.dropdown;

                skate.init(trigger);
                skate.init(dropdown);
            });

            it('to dropdown triggered via click', function() {
                expectButtonsClass(dropdown, false, 'Dropdown should not have class before triggered');
                click(trigger);
                expectButtonsClass(dropdown, true, 'Dropdown should have class when triggered by click');
                click(trigger);
                expectButtonsClass(dropdown, false, 'Dropdown should not have class when closed by click');
            });

            it('to dropdown triggered via keyboard', function() {
                expectButtonsClass(dropdown, false, 'Dropdown should not have class before triggered');
                focus(trigger);
                pressKey(keyCode.SPACE);
                expectButtonsClass(dropdown, true, 'Dropdown should have class when triggered by keyboard');
                pressKey(keyCode.ESCAPE);
                expectButtonsClass(dropdown, false, 'Dropdown should not have class when closed by keyboard');
            });
        });
    });

    describe('- button', function () {
        beforeEach(function () {
            button = fixtures({
                button: '<button class="aui-button"></button>'
            }).button;
            skate.init(button);
        });

        afterEach(function () {
            $(button).remove();
        });

        it('global', function () {
            expect(AJS.button).to.equal(undefined);
        });

        it('AMD module', function (done) {
            amdRequire(['aui/button'], function (amdModule) {
                expect(amdModule).to.equal(AJS.button);
                done();
            });
        });

        it('Button element has prototype', function () {
            expect(button.busy).to.be.a('function');
            expect(button.idle).to.be.a('function');
        });

        it('Calling busy should set aria-busy', function () {
            button.busy();

            expect(button.getAttribute('aria-busy')).to.equal('true');
        });

        it('Calling busy should add spin container inside button', function () {
            button.busy();
            expect(button.querySelectorAll('aui-spinner').length).to.equal(1);
        });

        it('Calling busy twice should only add one spin container inside button', function () {
            button.busy();
            button.busy();
            expect(button.querySelectorAll('aui-spinner').length).to.equal(1);
        });

        it('Calling idle should unset aria-busy', function () {
            button.busy();
            button.idle();

            expect(button.hasAttribute('aria-busy')).to.be.false;
        });

        it('Calling idle should remove spin container from inside button', function () {
            button.busy();
            button.idle();
            expect(button.querySelectorAll('.aui-button-spinner').length).to.equal(0);
        });

        it('Calling isBusy returns false initially', function () {
            expect(button.isBusy()).to.be.false;
        });

        it('Calling isBusy returns true when busy', function () {
            button.busy();
            expect(button.isBusy()).to.be.true;
        });

        it('Calling isBusy returns false when idled', function () {
            button.busy();
            button.idle();
            expect(button.isBusy()).to.be.false;
        });
    });
});
