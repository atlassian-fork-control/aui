import $ from '@atlassian/aui/src/js/aui/jquery';
import toInit from '@atlassian/aui/src/js/aui/to-init';

describe('aui/to-init', function () {
    let realDocumentIsReady;
    let fakeDocumentIsReady;
    let clock;
    let logError;

    function triggerDocumentReady() {
        fakeDocumentIsReady.resolve();
        // jquery 3 resolves deferred objects asynchronously.
        clock.tick(1);
    }

    beforeEach(function() {
        clock = sinon.useFakeTimers();
        logError = sinon.stub(console, 'error');
        fakeDocumentIsReady = new $.Deferred();
        realDocumentIsReady = $.ready;
        $.ready = fakeDocumentIsReady.promise();
    });

    afterEach(function () {
        $.ready = realDocumentIsReady;
        clock.restore();
        logError.restore();
    });

    it('globals', function () {
        expect(AJS.toInit.toString()).to.equal(toInit.toString());
    });

    it('on multiple functions', function () {
        var func1 = sinon.spy();
        var func2 = sinon.spy();

        toInit(func1);
        toInit(func2);

        expect(func1.callCount).to.equal(0);
        expect(func2.callCount).to.equal(0);

        triggerDocumentReady();

        // It still won't have been called, because we run *after* document ready
        expect(func1.callCount).to.equal(0);
        expect(func2.callCount).to.equal(0);

        clock.tick(1);

        // Now they should have run.
        expect(func1.callCount).to.equal(1);
        expect(func2.callCount).to.equal(1);
        expect(logError.callCount).to.equal(0);
    });

    it('should properly throw errors from passed in functions', function () {
        var error = new Error('test');
        var func1 = sinon.stub().throws(error);
        var func2 = sinon.spy();

        toInit(func1);
        toInit(func2);

        expect(func1.callCount).to.equal(0);
        expect(func2.callCount).to.equal(0);

        triggerDocumentReady();
        clock.tick(1);

        expect(func1.callCount).to.equal(1);
        expect(func2.callCount).to.equal(1);
        expect(logError.callCount).to.equal(1);
    });

    it('should provide jQuery as its first argument', function () {
        var func1 = sinon.spy();
        toInit(func1);

        triggerDocumentReady();
        clock.tick(1);

        expect(func1.getCall(0).args[0]).to.equal($);
    });
});
