import $ from '@atlassian/aui/src/js/aui/jquery';
import CustomEvent from '@atlassian/aui/src/js/aui/polyfills/custom-event';
import Alignment, {GPU_ACCELERATION_FLAG} from '@atlassian/aui/src/js/aui/internal/alignment';
import {
    fixtures,
    mockPopper,
    accountForKarmaIframe
} from '../../../helpers/all';

describe('aui/internal/alignment', function () {
    let $viewport;
    let $canvas;
    let $anchor;
    let popper;
    let alignment;

    function resize() {
        window.dispatchEvent(new CustomEvent('resize'));
    }

    function scrollTo(pixels) {
        $viewport.get(0).scrollTop = pixels;
        $viewport.get(0).dispatchEvent(new CustomEvent('scroll'));
    }

    function createLayer(alignment) {
        return $('<div id="layer-1" class="box"></div>')
            .attr('data-aui-alignment', alignment)
            .attr('aria-hidden', false)
            .appendTo($canvas);
    }

    function comparePosition ($layer, expected) {
        expected = accountForKarmaIframe(expected);
        const position = $layer.offset();
        expect((position.top === expected.y) && (position.left === expected.x),
            `${expected.position}: expected "${expected.x}, ${expected.y}" but got "${position.left}, ${position.top}"`
        ).to.equal(true);
    }

    beforeEach(function () {
        const constructed = fixtures({
            viewport: $('<div id="viewport">')
                .css({
                    overflow: 'scroll',
                    height: 200,
                    width: 200,
                }),
            canvas: $('<div id="canvas"></div>')
                .css({
                    height: 400,
                    width: 400,
                    top: 0,
                    left: 0,
                }),
            styles: `<style>
                .dot {
                    position: relative;
                    overflow: hidden;
                    width: 10px;
                    height: 10px;
                    background: rebeccapurple;
                    display: block;
                }
                .box {
                    position: absolute;
                    display: block;
                    background: orange;
                    /* layer needs some height so that layer :visible selector works properly */
                    width: 16px;
                    height: 16px;
                }
            </style>`,
        });
        popper = mockPopper();

        $viewport = $(constructed.viewport);
        $canvas = $(constructed.canvas).appendTo($viewport);
        $anchor = $('<div class="dot">Im the target</div>')
            // throw it in the middle of the canvas
            .css({
                top: 40,
                left: 40,
            })
            .appendTo($canvas);
    });

    afterEach(function () {
        popper.restore();
        if (alignment) {
            alignment.destroy();
        }
    });

    const expectedOffsets = [
        {position: '', y: 37, x: 50},
        {position: 'bad position', y: 37, x: 50},

        {position: 'bottom right', y: 50, x: 34},
        {position: 'bottom center', y: 50, x: 37},
        {position: 'bottom left', y: 50, x: 40},

        {position: 'top left', y: 24, x: 40},
        {position: 'top center', y: 24, x: 37},
        {position: 'top right', y: 24, x: 34},

        {position: 'right top', y: 40, x: 50},
        {position: 'right middle', y: 37, x: 50},
        {position: 'right bottom', y: 34, x: 50},

        {position: 'left top', y: 40, x: 24},
        {position: 'left middle', y: 37, x: 24},
        {position: 'left bottom', y: 34, x: 24}
    ];

    expectedOffsets.forEach(function (expected) {
        it(`sets proper transform for ${expected.position}`, function () {
            const $layer = createLayer(expected.position);
            alignment = new Alignment($layer.get(0), $anchor.get(0));
            popper.tick();

            comparePosition($layer, expected);
        });
    });

    it('does not automatically follow resize and scroll events', function () {
        const offset = expectedOffsets[2];
        const $layer = createLayer(offset.position);
        alignment = new Alignment($layer.get(0), $anchor.get(0));
        popper.tick();

        $anchor.css({top: 100, left: 100});
        resize();
        popper.tick();

        comparePosition($layer, offset);

        scrollTo(10);
        popper.tick();

        comparePosition($layer, offset);
    });

    describe('css positioning', function () {
        it('is using top/left to position layer', function () {
            const offset = expectedOffsets[2];
            const $layer = createLayer(offset.position);
            alignment = new Alignment($layer.get(0), $anchor.get(0));
            popper.tick();

            expect($layer.get(0).style.transform).to.equal('');
            expect($layer.get(0).style.top).to.not.equal('');
            expect($layer.get(0).style.top).to.not.equal('0px');
            expect($layer.get(0).style.left).to.not.equal('');
            expect($layer.get(0).style.left).to.not.equal('0px');
        });

        it('is using transform to position layer when CSS class exists on body', function () {
            const offset = expectedOffsets[2];
            const $layer = createLayer(offset.position);
            document.body.classList.add(GPU_ACCELERATION_FLAG);
            alignment = new Alignment($layer.get(0), $anchor.get(0));
            popper.tick();

            expect($layer.get(0).style.transform).to.not.equal('');
            expect($layer.get(0).style.top).to.equal('0px');
            expect($layer.get(0).style.left).to.equal('0px');
        });
    });

    describe('', function() {
        const oldPosition = { top: 40, left: 40 };
        const newPosition = { top: 100, left: 100 };
        const expectedOffset = { position: 'right top', x: 110, y: 100 };
        let $layer;

        beforeEach(function () {
            $anchor.css(oldPosition);
            $layer = createLayer(expectedOffset.position);
            alignment = new Alignment($layer.get(0), $anchor.get(0));
            popper.tick();
        });

        describe('#enable', function () {
            it('causes the element to re-position when the window is resized', function () {
                alignment.enable();
                $anchor.css(newPosition);
                resize();
                popper.tick();

                comparePosition($layer, expectedOffset);
            });

            it('causes the element to re-position when its scroll parent is scrolled', function () {
                const offsetY = 27;
                alignment.enable();
                $anchor.css(newPosition);
                scrollTo(offsetY);
                popper.tick();

                comparePosition($layer, { x: expectedOffset.x, y: expectedOffset.y - offsetY });
            });
        });

        describe('#scheduleUpdate', function () {
            it('re-positions the element next to its target', function () {
                $anchor.css(newPosition);
                alignment.scheduleUpdate();
                popper.tick();

                comparePosition($layer, expectedOffset);
            });
        });
    });

    describe('', function () {
        let $layer;
        let $anchor1;
        let $anchor2;

        beforeEach(function() {
            $anchor1 = $anchor
                .css({
                    top: 20,
                    left: 40
                });
            $anchor2 = $('<div class="dot">Second anchor</div>')
                .css({
                    top: 80,
                    left: 80
                })
                .appendTo($canvas);
            $layer = createLayer('right top');
        });

        it('has callbacks', function () {
            const created = sinon.spy();
            const updated = sinon.spy();

            alignment = new Alignment($layer.get(0), $anchor.get(0), {
                onCreate: created,
                onUpdate: updated,
            });

            // callbacks don't happen until at least one render occurs.
            expect(created.callCount, 'should not be called until a render occurs').to.equal(0);
            expect(updated.callCount, 'should not be called until a render occurs').to.equal(0);

            // trigger popper to render
            popper.tick();
            expect(created.callCount, 'should be called on first render').to.equal(1);
            expect(updated.callCount, 'should not be called on first render').to.equal(0);

            // schedule and trigger another render
            alignment.scheduleUpdate();
            popper.tick();

            expect(created.callCount, 'should not be called on second render').to.equal(1);
            expect(updated.callCount, 'should be called on second render').to.equal(1);

            // schedule and trigger another render
            alignment.scheduleUpdate();
            popper.tick();

            expect(created.callCount, 'should not be called on third render').to.equal(1);
            expect(updated.callCount, 'should be called on third render').to.equal(2);
        });

        describe('#changeTarget', function () {
            beforeEach(function() {
                alignment = new Alignment($layer.get(0), $anchor1.get(0));
                popper.tick();
            });

            it('updates the alignment', function () {
                alignment.enable();
                alignment.changeTarget($anchor2.get(0));
                popper.tick();

                comparePosition($layer, { x: 90, y: 90 });
            });

            it('updates the alignment even when resize and scroll events are disabled', function () {
                alignment.disable();
                alignment.changeTarget($anchor2.get(0));
                popper.tick();

                comparePosition($layer, { x: 90, y: 90 });
            });
        });
    });

});
