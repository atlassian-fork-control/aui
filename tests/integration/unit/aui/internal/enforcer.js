import enforce from '@atlassian/aui/src/js/aui/internal/enforcer';
import * as logger from '@atlassian/aui/src/js/aui/internal/log';

function createDivWithAttribute(attribute, value) {
    var newElement = document.createElement('div');
    newElement.setAttribute(attribute, value);
    return newElement;
}

describe('aui/internal/enforcer', function () {
    var testFixture;

    beforeEach(function () {
        testFixture = document.getElementById('test-fixture');
        testFixture.innerHTML = '';
        logger.error = sinon.spy();
    });

    afterEach(function () {
        logger.error = undefined;
    });

    it('ariaControls returns false if there is no element with id set to "test-id"', function () {
        var element = createDivWithAttribute('aria-controls', 'test-id');
        testFixture.appendChild(element);
        expect(enforce(element).ariaControls()).to.be.equal(false);
    });

    it('ariaControls logs an error if there is no element with id set to "test-id"', function () {
        var element = createDivWithAttribute('aria-controls', 'test-id');
        testFixture.appendChild(element);
        enforce(element).ariaControls()
        expect(logger.error.calledOnce).to.be.equal(true);
    });

    it('ariaControls returns false if there is no aria-controls attribute set on the element', function () {
        var element = createDivWithAttribute('id', 'test-id')
        testFixture.appendChild(element);
        expect(enforce(element).ariaControls()).to.be.equal(false);
    });

    it('ariaControls logs an error if there is no aria-controls attribute set on the element', function () {
        var element = createDivWithAttribute('id', 'test-id')
        testFixture.appendChild(element);
        enforce(element).ariaControls();
        expect(logger.error.calledOnce).to.be.equal(true);
    });

    it('ariaOwns returns false if there is no element with id set to "test-id"', function () {
        var element = createDivWithAttribute('aria-owns', 'test-id');
        testFixture.appendChild(element);
        expect(enforce(element).ariaOwns()).to.be.equal(false);
    });

    it('ariaOwns logs an error if there is no element with id set to test-id', function () {
        var element = createDivWithAttribute('aria-owns', 'test-id');
        testFixture.appendChild(element);
        enforce(element).ariaOwns();
        expect(logger.error.calledOnce).to.be.equal(true);
    });

    it('ariaOwns returns false if there is no aria-owns attribute set on the element', function () {
        var element = createDivWithAttribute('id', 'test-id')
        testFixture.appendChild(element);
        expect(enforce(element).ariaOwns()).to.be.equal(false);
    });

    it('ariaOwns logs an error if there is no "aria-owns" attribute set on the element', function () {
        var element = createDivWithAttribute('id', 'test-id')
        testFixture.appendChild(element);
        enforce(element).ariaOwns();
        expect(logger.error.calledOnce).to.be.equal(true);
    });

    it('refersTo returns false if there is no element with id set to "test-id"', function () {
        var element = createDivWithAttribute('aria-controls', 'test-id');
        testFixture.appendChild(element);
        expect(enforce(element).refersTo('aria-controls')).to.be.equal(false);
    });

    it('refersTo logs an error if there is no element with id set to "test-id"', function () {
        var element = createDivWithAttribute('aria-controls', 'test-id');
        testFixture.appendChild(element);
        enforce(element).refersTo('aria-controls');
        expect(logger.error.calledOnce).to.be.equal(true);
    });

    it('refersTo returns false if there is no "aria-controls" attribute set on the element', function () {
        var element = createDivWithAttribute('id', 'test-id')
        testFixture.appendChild(element);
        expect(enforce(element).refersTo('aria-controls')).to.be.equal(false);
    });

    it('refersTo logs an error if there is no "aria-controls" attribute set on the element', function () {
        var element = createDivWithAttribute('id', 'test-id')
        testFixture.appendChild(element);
        enforce(element).refersTo('aria-controls');
        expect(logger.error.calledOnce).to.be.equal(true);
    });

    it('refersTo returns true if there is an associated element', function () {
        testFixture.appendChild(createDivWithAttribute('id', 'test-id'));
        var element = createDivWithAttribute('aria-controls', 'test-id');
        testFixture.appendChild(element);
        expect(enforce(element).refersTo('aria-controls')).to.be.equal(true);
    });

    it('attributeExists returns true if "id" attribute is on the element', function () {
        var element = createDivWithAttribute('id', 'test-id')
        testFixture.appendChild(element);
        expect(enforce(element).attributeExists('id')).to.be.equal(true);
    });

    it('attributeExists does not log an error if "id" attribute is on the element', function () {
        var element = createDivWithAttribute('id', 'test-id')
        testFixture.appendChild(element);
        enforce(element).attributeExists('id');
        expect(logger.error.calledOnce).to.be.equal(false);
    });

    it('attributeExists returns false if there the "id" attribute is not on the element', function () {
        var element = document.createElement('div');
        testFixture.appendChild(element);
        expect(enforce(element).attributeExists('id')).to.be.equal(false);
    });

    it('attributeExists logs an error if there the "id" attribute is not on the element', function () {
        var element = document.createElement('div');
        testFixture.appendChild(element);
        enforce(element).attributeExists('id');
        expect(logger.error.calledOnce).to.be.equal(true);
    });

    it('satisifesRules returns false if the rules function returns false', function () {
        expect(enforce(null).satisfiesRules(() => false, 'this should break')).to.be.equal(false);
    });


});
