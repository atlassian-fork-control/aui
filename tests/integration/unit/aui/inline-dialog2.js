import $ from '@atlassian/aui/src/js/aui/jquery';
import '@atlassian/aui/src/js/aui/trigger';
import {expectBooleanAttribute} from '../../helpers/assertions';
import aui from '@atlassian/aui-soy/entry/aui-soy';
import InlineDialog from '@atlassian/aui/src/js/aui/inline-dialog2';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import CustomEvent from '@atlassian/aui/src/js/aui/polyfills/custom-event';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import {
    afterMutations,
    afterRender,
    blur,
    click,
    fixtures,
    focus,
    hover,
    mockPopper,
    pressKey,
    withKarmaIframeTopOffset
} from '../../helpers/all';

describe('aui/inline-dialog2', function () {
    let trigger;
    let inlineDialog;
    let spy;
    let clock;

    beforeEach(function () {
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    function sendMessage (type) {
        inlineDialog.message($.Event(type));
    }

    function onPrefixedEvent(prefix, inlineDialog, event, fn) {
        inlineDialog.addEventListener(prefix + event, fn);
    }

    function offPrefixedEvent(prefix, inlineDialog, event, fn) {
        inlineDialog.removeEventListener(prefix + event, fn);
    }

    function generateInlineDialog2(content, attrs) {
        const params = $.extend({}, {
            id: 'inline-dialog2-1',
            alignment: 'bottom center',
            content
        }, attrs);
        return $(aui.inlineDialog2.inlineDialog2(params));
    }

    describe('api', function () {
        it('global', function () {
            expect(AJS.InlineDialog2.toString()).to.equal(InlineDialog.toString());
        });

        it('AMD module', function (done) {
            amdRequire(['aui/inline-dialog2'], function (amdModule) {
                expect(amdModule).to.equal(InlineDialog);
                done();
            });
        });
    });

    describe('Basic Functionality -', function () {
        beforeEach(function () {
            const constructed = fixtures({
                trigger: '<button data-aui-trigger="" aria-controls="inline-dialog2-1"></button>',
                inlineDialog: generateInlineDialog2('<h3>InlineDialog content</h3>'),
            });
            trigger = constructed.trigger;
            inlineDialog = constructed.inlineDialog;
            skate.init([trigger, inlineDialog]);
        });

        it('Should default to hidden', function () {
            expect(inlineDialog.open).to.equal(false);
        });

        it('Should open (and remain open post document observer) on first click', function (done) {
            // TODO Remove this test when https://github.com/skatejs/skatejs/issues/261 is fixed.
            click(trigger);

            // Since layer reparents the inline dialog to the body, we first get the native
            // detached and attached handlers, followed by the document observer's detached
            // and attached handlers, hence we check that it is visible after both run.
            expect(inlineDialog.open).to.equal(true, 'sync');
            afterMutations(function () {
                expect(inlineDialog.open).to.equal(true, 'async');
                done();
            });
        });

        it('Correct element should be focused if visible and persistent', function (done) {
            var $inlineDialog = $(inlineDialog);
            var $myInput = $('<input type="text" id="my-input">');
            var dialogFocusSpy = sinon.spy();
            var inputFocusSpy = sinon.spy();

            inlineDialog.persistent = true;
            $inlineDialog.attr('aria-controls', 'my-input');
            $inlineDialog.find('.aui-inline-dialog-contents').append($myInput);
            $inlineDialog.focus(dialogFocusSpy);
            $myInput.focus(inputFocusSpy);

            click(trigger);

            afterMutations(function () {
                expect(dialogFocusSpy.callCount).to.equal(0, 'dialog focus handler');
                expect(inputFocusSpy.callCount).to.equal(1, 'input focus handler');
                expect($myInput.is(document.activeElement)).to.equal(true, 'input focused');
                done();
            }, 50);
        });

        it('Should close when escape pressed if open', function () {
            click(trigger);
            pressKey(keyCode.ESCAPE);
            expect(inlineDialog.open).to.equal(false);
        });

        it('Should not close when clicked if open', function () {
            click(trigger);
            click(inlineDialog);
            expect(inlineDialog.open).to.equal(true);
        });
    });

    describe('Proper templating -', function () {
        it('should preserve events after templating', function () {
            var $innerButton;
            var clickEventSpy;
            var $inlineDialog = $(aui.inlineDialog2.inlineDialog2({
                id: 'inline-dialog2-1',
                alignment: 'bottom center',
                respondsTo: 'toggle',
                content: '<div><button>test button</button></div>'
            }));

            $innerButton = $inlineDialog.find('button');
            clickEventSpy = sinon.spy();
            $innerButton.bind('click', clickEventSpy);

            skate.init($inlineDialog);

            $innerButton.trigger('click');
            clickEventSpy.should.have.been.calledOnce;
        });
    });

    describe('open attribute/property -', function () {
        beforeEach(function () {
            var inlineDialogId = 'inline-dialog2-visible';
            var fixture = fixtures({
                trigger: '<a data-aui-trigger aria-controls="' + inlineDialogId + '" href="#' + inlineDialogId + '">Show</a>',
                inlineDialog: generateInlineDialog2('<h3>I was generated!</h3>', {
                    id: inlineDialogId,
                    open: true
                })
            });
            skate.init(fixture.trigger);
            skate.init(fixture.inlineDialog);

            trigger = fixture.trigger;
            inlineDialog = fixture.inlineDialog;
        });

        it('soy template emits attr', function () {
            expectBooleanAttribute.isPresentAndEmpty(inlineDialog, 'open');
        });

        it('should initialize aria-hidden attribute', function () {
            expect(inlineDialog.hasAttribute('aria-hidden')).to.equal(true);
        });

        it('initially open', function () {
            expect(inlineDialog.open).to.equal(true);
        });

        it('open property reflects actual visibility', function (done) {
            expect($(inlineDialog).is(':visible')).to.equal(true, ':visible when open');

            inlineDialog.open = false;

            afterMutations(function () {
                let visibility = $(inlineDialog).is(':visible');
                expect(visibility).to.equal(false, 'not :visible when closed');
                done();
            }, 1000);
        });

        it('open attribute and property are correct if aui-hide event is prevented permanently', function (done) {
            inlineDialog.addEventListener('aui-hide', function (event) {
                event.preventDefault();
            });

            inlineDialog.open = false;

            afterMutations(function () {
                expect(inlineDialog.open).to.equal(true, 'open property is not true after closed but aui-hide prevented');
                expect(inlineDialog.hasAttribute('open')).to.equal(true, 'open attribute is not present after closed and aui-hide prevented');
                done();
            });
        });

        it('open attribute and property are correct if aui-hide event is prevented only once', function (done) {
            var alreadyPrevented = false;

            inlineDialog.addEventListener('aui-hide', function (event) {
                if (!alreadyPrevented) {
                    event.preventDefault();
                    alreadyPrevented = true;
                }
            });

            inlineDialog.open = false;

            afterMutations(function () {
                expect(inlineDialog.open).to.equal(true, 'open property is not true after closed but aui-hide prevented');
                expect(inlineDialog.hasAttribute('open')).to.equal(true, 'open attribute is not present after closed and aui-hide prevented');

                inlineDialog.open = false;
                afterMutations(function () {
                    expect(inlineDialog.open).to.equal(false, 'open property is not false after closed');
                    expect(inlineDialog.hasAttribute('open')).to.equal(false, 'open attribute is present after closed');

                    inlineDialog.open = false;
                    done();
                });
            });
        });

        it('open attribute and property are correct if aui-show event is prevented permanently', function (done) {
            inlineDialog.open = false;

            inlineDialog.addEventListener('aui-show', function (event) {
                event.preventDefault();
            });

            inlineDialog.open = true;

            afterMutations(function () {
                expect(inlineDialog.open).to.equal(false, 'open property is not false after closed but aui-show prevented');
                expect(inlineDialog.hasAttribute('open')).to.equal(false, 'open attribute is present after opened and aui-show prevented');
                done();
            });
        });

        it('aui-show AND aui-hide events can both have prevented listeners without causing an inifinite event loop', function (done) {
            let eventHandler = function (event) {
                event.preventDefault();
            };

            const handlerSpy = sinon.spy(eventHandler);
            inlineDialog.addEventListener('aui-show', handlerSpy);
            inlineDialog.addEventListener('aui-hide', handlerSpy);

            inlineDialog.open = false;

            afterMutations(function () {
                expect(handlerSpy.calledOnce).to.equal(true, 'aui-show/aui-hide events were not triggered exactly once');
                done();
            }, 200);
        });

    });

    describe('persistent attribute/property -', function () {
        beforeEach(function () {
            var inlineDialogId = 'inline-dialog2-visible';
            var fixture = fixtures({
                trigger: '<a data-aui-trigger aria-controls="' + inlineDialogId + '" href="#' + inlineDialogId + '">Show</a>',
                inlineDialog: aui.inlineDialog2.inlineDialog2({
                    id: inlineDialogId,
                    content: '<h3>I was generated by soy :)</h3>',
                    persistent: true
                })
            });
            skate.init(fixture.trigger);
            skate.init(fixture.inlineDialog);

            trigger = fixture.trigger;
            inlineDialog = fixture.inlineDialog;
        });

        it('soy template emits attr', function () {
            expectBooleanAttribute.isPresentAndEmpty(inlineDialog, 'persistent');
        });

        it('stays open on outside click', function () {
            click(trigger);
            expect(inlineDialog.open).to.equal(true, 'opens like normal');

            click(document);
            expect(inlineDialog.open).to.equal(true, 'persistent outside click');

            inlineDialog.persistent = false;
            click(document);
            expect(inlineDialog.open).to.equal(false, 'outside click');
        });
    });

    describe('responds-to attribute/property -', function () {
        beforeEach(function () {
            var inlineDialogId = 'inline-dialog2-visible';
            var fixture = fixtures({
                trigger: '<a data-aui-trigger aria-controls="' + inlineDialogId + '" href="#' + inlineDialogId + '">Show</a>',
                inlineDialog: aui.inlineDialog2.inlineDialog2({
                    id: inlineDialogId,
                    content: '<h3>I was generated by soy :)</h3>',
                    respondsTo: 'hover'
                })
            });
            skate.init(fixture.trigger);
            skate.init(fixture.inlineDialog);

            trigger = fixture.trigger;
            inlineDialog = fixture.inlineDialog;
        });

        it('soy template emits attr', function () {
            expect(inlineDialog.getAttribute('responds-to')).to.equal('hover');
        });
    });

    describe('Template - ', function () {
        var dialog;

        beforeEach(function () {
            dialog = fixtures({dialog: new InlineDialog()}).dialog;
            skate.init(dialog);
        });

        it('should add the aui-layer class', function () {
            expect(dialog.className).to.contain('aui-layer');
        });

        it('the custom element should be the same as the soy generated template', function () {
            const constructed = fixtures({
                soy: aui.inlineDialog2.inlineDialog2({})
            });

            skate.init(constructed.soy);
            expect(document.getElementById('test-fixture').innerHTML).to.equal(constructed.soy.outerHTML);
        });
    });

    describe('Triggers -', function () {
        it('Should add the aria-haspopup attribute to its trigger', function () {
            var fixture = fixtures({
                trigger: '<button aria-controls="my-inline-dialog"></button>',
                inlineDialog: '<aui-inline-dialog id="my-inline-dialog"></aui-inline-dialog>'
            });

            skate.init(fixture.inlineDialog);
            expect(fixture.trigger.getAttribute('aria-haspopup')).to.equal('true');
        });

        it('Should set the aria-expanded attribute when showing and hiding', function () {
            const { trigger, inlineDialog } = fixtures({
                trigger: '<button aria-controls="my-inline-dialog"></button>',
                inlineDialog: '<aui-inline-dialog id="my-inline-dialog"></aui-inline-dialog>'
            });

            skate.init(inlineDialog);
            expect(trigger.getAttribute('aria-expanded')).to.equal('false');
            inlineDialog.open = true;
            expect(trigger.getAttribute('aria-expanded')).to.equal('true');
            inlineDialog.open = false;
            expect(trigger.getAttribute('aria-expanded')).to.equal('false');
        });

        it('Should set the aria-expanded attribute to false when hiding by clicking outside', function (done) {
            const { trigger, inlineDialog } = fixtures({
                trigger: '<button class="aui-button" data-aui-trigger aria-controls="my-inline-dialog">Show</button>',
                inlineDialog: '<aui-inline-dialog id="my-inline-dialog"></aui-inline-dialog>'
            });

            skate.init(trigger);
            skate.init(inlineDialog);
            expect(inlineDialog.open).to.equal(false);
            click(trigger);
            expect(inlineDialog.open).to.equal(true);
            click(document);
            expect(inlineDialog.open).to.equal(false);
            // Ideally don't need this additional call but without it this test in FF will fail because the attribute does
            // not get updated until after the test. Can't figure out exactly why this is the case so this is the workaround for now.
            inlineDialog.open = false;
            afterMutations(function () {
                expect(trigger.getAttribute('aria-expanded')).to.equal('false');
                done();
            }, 500);
        });

        it('Should respond-to toggle by default (when responds-to is omitted)', function () {
            const { trigger, inlineDialog } = fixtures({
                trigger: '<button class="aui-button" data-aui-trigger aria-controls="my-inline-dialog">Show</button>',
                inlineDialog: '<aui-inline-dialog id="my-inline-dialog"></aui-inline-dialog>'
            });
            skate.init(trigger);
            skate.init(inlineDialog);

            expect(inlineDialog.open).to.equal(false, 'initial state');
            hover(trigger);
            expect(inlineDialog.open).to.equal(false, 'trigger hover');
            click(trigger);
            expect(inlineDialog.open).to.equal(true, 'trigger click');
        });

        describe('when inline dialog responds to toggle', function () {
            let inlineDialog;
            let trigger1;
            let trigger2;
            let trigger3;

            beforeEach(function() {
                const id = 'toggle-able-inline-dialog';
                const constructed = fixtures({
                    inlineDialog: generateInlineDialog2('Toggle-able', { id, respondsTo: 'toggle', alignment: 'left top' }),
                    trigger1: `<button data-aui-trigger aria-controls="${id}" class="test-trigger">First</button>`,
                    trigger2: `<span data-aui-trigger aria-controls="${id}" class="test-trigger">Second</span>`,
                    trigger3: `<a data-aui-trigger aria-controls="${id}" class="test-trigger" href="#">Third</a>`,
                    styles: '<style>.test-trigger { width: 100px; height: 100px; display: block; }</style>',
                });
                inlineDialog = constructed.inlineDialog;
                trigger1 = constructed.trigger1;
                trigger2 = constructed.trigger2;
                trigger3 = constructed.trigger3;
                skate.init([inlineDialog, trigger1, trigger2, trigger3]);
                expect(inlineDialog.open).to.equal(false);
            });

            it('Should support multiple triggers', function () {
                click(trigger2);
                expect(inlineDialog.open).to.equal(true);

                click(trigger3);
                expect(inlineDialog.open).to.equal(false);

                click(trigger1);
                expect(inlineDialog.open).to.equal(true);
            });
        });

        describe('when inline dialog responds to hover', function () {
            let inlineDialog;
            let trigger1;
            let trigger2;
            let trigger3;
            let triggerOther;
            let popper;

            beforeEach(function() {
                const id = 'hoverable-inline-dialog';
                const constructed = fixtures({
                    inlineDialog: generateInlineDialog2('Hover-able', { id, respondsTo: 'hover', alignment: 'left top' }),
                    trigger1: `<button data-aui-trigger aria-controls="${id}" class="test-trigger">First</button>`,
                    trigger2: `<span data-aui-trigger aria-controls="${id}" class="test-trigger">Second</span>`,
                    trigger3: `<a data-aui-trigger aria-controls="${id}" class="test-trigger" href="#">Third</a>`,
                    triggerOther: '<button data-aui-trigger aria-controls="nothing" class="test-trigger">Other</button>',
                    styles: '<style>.test-trigger { width: 100px; height: 100px; display: block; }</style>',
                });
                inlineDialog = constructed.inlineDialog;
                trigger1 = constructed.trigger1;
                trigger2 = constructed.trigger2;
                trigger3 = constructed.trigger3;
                triggerOther = constructed.triggerOther;
                skate.init([inlineDialog, trigger1, trigger2, trigger3, triggerOther]);
                popper = mockPopper();
                expect(inlineDialog.open).to.equal(false);
            });

            afterEach(function() {
                popper.restore();
            });

            it('Should close when any valid trigger tells it to', function () {
                focus(trigger3);
                expect(inlineDialog.open).to.equal(true);

                blur(triggerOther);
                expect(inlineDialog.open).to.equal(true, 'should stay open when unrelated trigger fires');

                blur(trigger3);
                expect(inlineDialog.open).to.equal(false, 'should close when its last trigger tells it to');
            });

            it.skip('Should position itself next to the trigger that opened it', function () {
                hover(trigger1);
                popper.tick();
                expect(inlineDialog.open).to.equal(true);
                expect(inlineDialog.getBoundingClientRect().top).to.equal(withKarmaIframeTopOffset(0));

                pressKey(keyCode.ESCAPE);
                expect(inlineDialog.open).to.equal(false);

                hover(trigger3);
                popper.tick();
                expect(inlineDialog.open).to.equal(true);
                expect(inlineDialog.getBoundingClientRect().top).to.equal(withKarmaIframeTopOffset(200), 'should move to the trigger that opened it');
            });

            it.skip('Should move positions while open and another trigger opens the dialog', function () {
                hover(trigger2);
                popper.tick();
                expect(inlineDialog.open).to.equal(true);
                expect(inlineDialog.getBoundingClientRect().top).to.equal(withKarmaIframeTopOffset(100));

                hover(trigger3);
                popper.tick();
                expect(inlineDialog.open).to.equal(true, 'should stay open');
                expect(inlineDialog.getBoundingClientRect().top).to.equal(withKarmaIframeTopOffset(200), 'should move to the last trigger to open it');
            });
        });
    });

    describe('Messages -', function () {
        var sandbox;

        beforeEach(function () {
            sandbox = sinon.sandbox.create();
            inlineDialog = $(aui.inlineDialog2.inlineDialog2({
                id: 'inline-dialog2-1',
                alignment: '',
                respondsTo: 'hover',
                content: '<h3>I was generated by soy :)</h3>'
            })).appendTo('#test-fixture').get(0);
            skate.init(inlineDialog);
        });

        afterEach(function () {
            sandbox.restore();
        });

        it('Hover inline dialog should not respond to click messages', function () {
            sendMessage('click');
            expect(inlineDialog.open).to.equal(false);
        });

        it('Hover inline dialog should not respond to invalid messages', function () {
            sendMessage('blah');
            expect(inlineDialog.open).to.equal(false);
        });

        it('Hover inline dialog should open on mouseenter message', function () {
            sendMessage('mouseenter');
            expect(inlineDialog.open).to.equal(true);
        });

        it('Hover inline dialog should close on mouseleave message', function () {
            var spy = sandbox.spy();
            var time = sandbox.useFakeTimers();

            inlineDialog.addEventListener('aui-layer-hide', spy);
            inlineDialog.open = true;
            sendMessage('mouseleave');

            time.tick(1000);
            expect(spy.callCount).to.equal(1);
        });

        it('Hover persistent inline dialog should not close on mouseleave message', function () {
            var time = sandbox.useFakeTimers();

            inlineDialog.persistent = true;

            sendMessage('mouseenter');
            expect(inlineDialog.open).to.equal(true);

            sendMessage('mouseleave');
            time.tick(1000);
            expect(inlineDialog.open).to.equal(true);
        });

        it('Hover inline dialog should not close if hovered over regardless of event ordering', function () {
            var time = sandbox.useFakeTimers();

            sendMessage('mouseenter');
            time.tick(1000);
            expect(inlineDialog.open).to.equal(true);

            inlineDialog.dispatchEvent(new CustomEvent('mouseenter'));
            sendMessage('mouseleave');
            time.tick(1000);
            expect(inlineDialog.open).to.equal(true);
        });

        it('Hover inline dialog should close if no longer hovered', function () {
            var time = sandbox.useFakeTimers();

            sendMessage('mouseenter');
            time.tick(1000);
            expect(inlineDialog.open).to.equal(true);

            sendMessage('mouseleave');
            inlineDialog.dispatchEvent(new CustomEvent('mouseenter'));
            time.tick(1000);
            expect(inlineDialog.open).to.equal(true);

            inlineDialog.dispatchEvent(new CustomEvent('mouseleave'));
            time.tick(1000);
            expect(inlineDialog.open).to.equal(false);
        });

        it('Hover inline dialog should not close if hovered over before the delay is executed', function () {
            var layerHideSpy = sandbox.spy();
            var time = sandbox.useFakeTimers();

            inlineDialog.addEventListener('aui-layer-hide', layerHideSpy);
            inlineDialog.open = true;
            sendMessage('mouseleave');

            time.tick(999);
            inlineDialog.dispatchEvent(new CustomEvent('mouseenter'));
            time.tick(1);

            expect(layerHideSpy.callCount).to.equal(0);
        });

        it('Hovering over the trigger after hovering out of it should not allow the inline dialog to close', function () {
            var layerHideSpy = sandbox.spy();
            var time = sandbox.useFakeTimers();

            inlineDialog.addEventListener('aui-layer-hide', layerHideSpy);
            inlineDialog.open = true;

            trigger.dispatchEvent(new CustomEvent('mouseleave'));
            time.tick(500);

            trigger.dispatchEvent(new CustomEvent('mouseenter'));
            time.tick(500);

            expect(layerHideSpy.callCount).to.equal(0);
        });

        it('Hover inline dialog should open on focus message', function () {
            sendMessage('focus');
            expect(inlineDialog.open).to.equal(true);
        });

        it('Hover inline dialog should close on blur message', function () {
            inlineDialog.open = true;
            sendMessage('blur');
            expect(inlineDialog.open).to.equal(false);
        });
    });

    // TODO: Remove 'aui-layer-' prefixed event tests once it is no longer used by inline dialog.
    ['aui-layer-', 'aui-'].forEach(function (prefix) {
        describe(`Events with ${prefix} prefix-`, function () {
            var onEvent = onPrefixedEvent.bind(this, prefix);
            var offEvent = offPrefixedEvent.bind(this, prefix);

            beforeEach(function () {
                var inlineDialogId = 'inline-dialog2-1';
                var fixture = fixtures({
                    trigger: '<a data-aui-trigger aria-controls="' + inlineDialogId + '" href="#' + inlineDialogId + '">Show</a>',
                    inlineDialog: aui.inlineDialog2.inlineDialog2({
                        id: inlineDialogId,
                        content: '<h3>I was generated by soy :)</h3>'
                    })
                });
                skate.init(fixture.trigger);
                skate.init(fixture.inlineDialog);

                trigger = fixture.trigger;
                inlineDialog = fixture.inlineDialog;
                spy = sinon.spy();
            });

            it('Local show event', function () {
                onEvent(inlineDialog, 'show', spy);
                inlineDialog.open = true;
                offEvent(inlineDialog, 'show', spy);

                expect(spy.callCount).to.equal(1);
                expect(inlineDialog.open).to.equal(true);
            });

            it('Local cancellable show event', function () {
                // Set up spy with predetermined behaviour.
                var prevent = function (e) {
                    e.preventDefault();
                };
                var preventDefault = {prevent: function () {}};
                var cancelShowEventSpy = sinon.stub(preventDefault, 'prevent', prevent);

                onEvent(inlineDialog, 'show', cancelShowEventSpy);
                inlineDialog.open = true;
                offEvent(inlineDialog, 'show', cancelShowEventSpy);

                expect(cancelShowEventSpy.called).to.equal(true, 'spy (AUI-3726)');
                expect(inlineDialog.open).to.equal(false, 'inline dialog');
                expect(trigger.getAttribute('aria-expanded')).to.equal('false', 'trigger');
            });

            it('Local hide event', function () {
                onEvent(inlineDialog, 'hide', spy);
                inlineDialog.open = true;
                inlineDialog.open = false;
                offEvent(inlineDialog, 'hide', spy);

                expect(spy.callCount).to.equal(1, 'spy');
                expect(inlineDialog.open).to.equal(false, 'open');
            });

            it('Local cancellable hide event', function () {
                // Set up spy with predetermined behaviour.
                var prevent = function (e) {
                    e.preventDefault();
                };
                var preventDefault = {prevent: function () {}};
                var cancelHideEventSpy = sinon.stub(preventDefault, 'prevent', prevent);

                onEvent(inlineDialog, 'hide', cancelHideEventSpy);
                inlineDialog.open = true;
                inlineDialog.open = false;
                offEvent(inlineDialog, 'hide', cancelHideEventSpy);

                expect(cancelHideEventSpy.called).to.equal(true, 'spy (AUI-3726)');
                expect(inlineDialog.open).to.equal(true, 'inline dialog');
                expect(trigger.getAttribute('aria-expanded')).to.equal('true', 'trigger');
            });

            it('Global show event', function () {
                onEvent(document, 'show', spy);
                inlineDialog.open = true;
                offEvent(document, 'show', spy);

                expect(spy.callCount).to.equal(1);
                expect(inlineDialog.open).to.equal(true);
            });

            it('Global multiple show event', function () {
                onEvent(document, 'show', spy);
                inlineDialog.open = true;
                inlineDialog.open = false;
                inlineDialog.open = true;
                inlineDialog.open = false;
                offEvent(document, 'show', spy);

                expect(spy.callCount).to.equal(2);
                expect(inlineDialog.open).to.equal(false);
            });

            it('Global cancellable show event', function () {
                // Set up spy with predetermined behaviour.
                var prevent = function (e) {
                    e.preventDefault();
                };
                var preventDefault = {prevent: function () {}};
                var cancelShowEventSpy = sinon.stub(preventDefault, 'prevent', prevent);

                onEvent(document, 'show', cancelShowEventSpy);
                inlineDialog.open = true;
                offEvent(document, 'show', cancelShowEventSpy);

                expect(cancelShowEventSpy.called).to.equal(true, 'spy (AUI-3726)');
                expect(inlineDialog.open).to.equal(false, 'inline dialog');
                expect(trigger.getAttribute('aria-expanded')).to.equal('false', 'trigger');
            });

            it('Global hide event', function () {
                onEvent(document, 'hide', spy);
                inlineDialog.open = true;
                inlineDialog.open = false;
                offEvent(document, 'hide', spy);

                expect(spy.callCount).to.equal(1);
                expect(inlineDialog.open).to.equal(false);
            });

            it('Global multiple hide event', function () {
                onEvent(document, 'hide', spy);
                inlineDialog.open = true;
                inlineDialog.open = false;
                inlineDialog.open = true;
                inlineDialog.open = false;
                offEvent(document, 'hide', spy);

                expect(spy.callCount).to.equal(2);
                expect(inlineDialog.open).to.equal(false);
            });

            it('Global cancellable hide event', function () {
                // Set up spy with predetermined behaviour.
                var prevent = function (e) {
                    e.preventDefault();
                };
                var preventDefault = {prevent: function () {}};
                var cancelHideEventSpy = sinon.stub(preventDefault, 'prevent', prevent);

                onEvent(document, 'hide', cancelHideEventSpy);
                inlineDialog.open = true;
                inlineDialog.open = false;
                offEvent(document, 'hide', cancelHideEventSpy);

                expect(cancelHideEventSpy.called).to.equal(true, 'spy (AUI-3726)');
                expect(inlineDialog.open).to.equal(true, 'inline dialog');
                expect(trigger.getAttribute('aria-expanded')).to.equal('true', 'trigger');
            });
        });
    });

    describe('scrolling', function () {
        let popper;
        beforeEach(function () {
            popper = mockPopper();
        });

        afterEach(function () {
            popper.restore();
        });

        it('AUI-4308 follows trigger when parent element is scrolling', function (done) {
            const getYPosition = dialog => dialog.getBoundingClientRect().top;
            const markup = $(`<div id="scroll-container" style="height: 500px; width: 500px; overflow-y: scroll">
                            <div style="height: 200px">
                            </div>
                            <button id="scroll-trigger" data-aui-trigger aria-controls="scrolling-dialog">
                                Scrolling trigger
                            </button>
                            <aui-inline-dialog id="scrolling-dialog" alignment="bottom center">
                                <p>they see me scrolling...</p>
                            </aui-inline-dialog>
                            <div style="height: 500px">
                            </div>
                        </div>`);
            $('#test-fixture').append(markup);

            const dialog = $('#scrolling-dialog')[0];
            skate.init(dialog);
            const scrollTrigger = $('#scroll-trigger')[0];
            skate.init(scrollTrigger);

            click(scrollTrigger);
            popper.tick();
            afterRender(() => {
                const scrollAmount = 100;
                const scrollContainer = $('#scroll-container');
                const dialogYPosition = getYPosition(dialog);
                scrollContainer[0].scrollTop = scrollAmount;

                popper.tick();
                afterRender(() => {
                    popper.tick();
                    const yPositionAfterScroll = getYPosition(dialog);
                    expect(yPositionAfterScroll).to.equal(dialogYPosition - scrollAmount,
                        'y position of dialog should change to account for container scroll');
                    expect(dialog.open).to.equal(true, 'dialog should be open');
                    done();
                });
            })
        });
    });
});
