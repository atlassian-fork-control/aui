import $ from '@atlassian/aui/src/js/aui/jquery';
import FocusManager from '@atlassian/aui/src/js/aui/focus-manager';
import { afterMutations } from '../../helpers/all';

describe('aui/focus-manager', function () {
    var $el;
    var spy;
    var $container;
    var $el1;
    var $el2;
    var spy1;
    var spy2;

    function createSingleInput () {
        $el = $('<input type="text" />').appendTo('#test-fixture');
        spy = sinon.spy();
    }

    function createTwoInputs () {
        $container = $('<div></div>').appendTo('#test-fixture');
        $el1 = $('<input type="text" />').appendTo($container);
        $el2 = $('<input type="text" />').appendTo($container);
        spy1 = sinon.spy();
        spy2 = sinon.spy();
    }

    function createSingleInputAndFocus (done) {
        createSingleInput();
        afterMutations(function () {
            $el.on('focus', spy);
            done();
        }, 50);
    }

    function createTwoInputsAndFocus (done) {
        createTwoInputs();

        afterMutations(function () {
            $el1.on('focus', spy1);
            $el2.on('focus', spy2);
            done();
        }, 50);
    }

    function createSingleInputAndBlur () {
        createSingleInput();
        $el.on('blur', spy);
    }

    function createTwoInputsAndBlur () {
        createTwoInputs();
        $el1.on('blur', spy1);
        $el2.on('blur', spy2);
    }

    it('globals', function () {
        expect(AJS.FocusManager.toString()).to.equal(FocusManager.toString());
    });

    it('enter() focuses on the first element only using the default selector', function (done) {
        createTwoInputsAndFocus(function () {
            new FocusManager().enter($container);

            afterMutations(function () {
                spy1.should.have.been.calledOnce;
                spy2.should.have.not.been.called;
                done();
            }, 50);
        });

    });

    it('enter() does not focus if data-aui-focus="false" is provided', function (done) {
        createTwoInputsAndFocus(function () {
            $container.attr('data-aui-focus', 'false');
            new FocusManager().enter($container);

            afterMutations(function () {
                spy1.should.have.not.been.called;
                spy2.should.have.not.been.called;
                done();
            }, 50);
        });
    });

    it('enter() does not focus if data-aui-focus="false" is provided and data-aui-focus-selector is present', function (done) {
        createTwoInputsAndFocus(function () {
            $el2.attr('id', 'sideshow-bob');
            $container.attr('data-aui-focus', 'false');
            $container.attr('data-aui-focus-selector', '#sideshow-bob');

            new FocusManager().enter($container);

            afterMutations(function () {
                spy1.should.have.not.been.called;
                spy2.should.have.not.been.called;
                done();
            }, 50);
        });

    });

    it('enter() focuses on the specified element using a custom selector', function (done) {
        createTwoInputsAndFocus(function () {
            $el2.attr('id', 'sideshow-bob');
            $container.attr('data-aui-focus-selector', '#sideshow-bob');

            new FocusManager().enter($container);

            afterMutations(function () {
                spy1.should.have.not.been.called;
                spy2.should.have.been.calledOnce;
                done();
            }, 50);
        });

    });

    it('enter() focuses on the first element only using a custom selector', function (done) {
        createTwoInputsAndFocus(function () {
            $el2.attr('id', 'sideshow-bob');
            $container.attr('data-aui-focus-selector', ':input');

            new FocusManager().enter($container);

            afterMutations(function () {
                spy1.should.have.been.calledOnce;
                spy2.should.have.not.been.called;
                done();
            }, 50);
        });

    });

    it('enter() selects passed element if it matches the focus selector', function (done) {
        createSingleInputAndFocus(function () {

            new FocusManager().enter($el);

            afterMutations(function () {
                spy.should.have.been.calledOnce;
                done();
            }, 50);
        });
    });

    it('exit() blurs the active element', function (done) {
        createTwoInputsAndBlur();
        $el1.focus();

        new FocusManager().exit($container);

        afterMutations(function () {
            spy1.should.have.been.calledOnce;
            done();
        }, 50);
    });

    it('exit() blurs the active element if the passed element is focussed', function (done) {
        createSingleInputAndBlur();
        $el.focus();

        new FocusManager().exit($el);
        afterMutations(function () {
            spy.should.have.been.calledOnce;
            done();
        }, 50);
    });

    it('exit() does not trigger blur on an element that is not underneath it', function () {
        createTwoInputsAndBlur();
        var $el = $('<input type="text" />').appendTo('#test-fixture');
        $el.focus();

        new FocusManager().exit($container);

        spy1.should.have.not.been.called;
        spy2.should.have.not.been.called;
    });

    it('preserves focus after enter() then exit()', function () {
        createTwoInputsAndBlur();
        var $focusButton = $('<button id="focusButton">Focus button</button>');
        $('#test-fixture').append($focusButton);
        $focusButton.focus();

        new FocusManager().enter($container);
        expect($focusButton.is(document.activeElement)).to.be.false;
        new FocusManager().exit($container);

        expect($focusButton.is(document.activeElement)).to.be.true;
    });

    describe(':aui-focusable', function () {
        it('matches an <input>', function () {
            var html = '<input>';
            var $el = $(html);
            $('#test-fixture').append($el);
            expect($el.is(':aui-focusable')).to.be.true;
        });

        it('does not match an invisible <input>', function () {
            var html = '<input style="visibility: hidden">';
            var $el = $(html);
            $('#test-fixture').append($el);
            expect($el.is(':aui-focusable')).to.be.false;
        });

        it('matches a visible <input> nested inside an invisible <div>', function () {
            var html = '<div style="visibility: hidden"><input id="nested-visible-input" style="visibility: visible"></div>';
            $('#test-fixture').html(html);
            var $el = $('#nested-visible-input');
            expect($el.is(':aui-focusable')).to.be.true;
        });
    });
});
