import $ from '@atlassian/aui/src/js/aui/jquery';
import DatePicker, { CalendarWidget } from '@atlassian/aui/src/js/aui/date-picker';
import {
    afterMutations,
    click,
    fixtures,
    focus,
} from '../../helpers/all';

describe('aui/date-picker', function () {
    let $input;
    let datePicker;

    function selectDate(date) {
        const $results = $('.ui-datepicker-calendar td').filter(function() {
            return String(this.textContent || '').trim() === String(date);
        });
        click($results.first());
    }

    beforeEach(function () {
        const field = fixtures({
            field: '<input class="aui-date-picker" id="test-input" type="date">'
        }).field;
        $input = $(field);
    });

    afterEach(function () {
        if (datePicker && datePicker.destroyPolyfill) {
            datePicker.destroyPolyfill();
        }
        $('.aui-inline-dialog').remove();
    });

    it('globals', function () {
        expect(AJS.DatePicker.toString()).to.equal(DatePicker.toString());
    });

    it('API', function () {
        expect(DatePicker).to.be.a('function');
        expect(DatePicker.prototype.browserSupportsDateField).to.be.a('boolean');
        expect(DatePicker.prototype.defaultOptions).to.be.an('object');
        expect(DatePicker.prototype.defaultOptions.overrideBrowserDefault).to.be.a('boolean');
        expect(DatePicker.prototype.defaultOptions.firstDay).to.be.a('number');
        expect(DatePicker.prototype.defaultOptions.languageCode).to.be.a('string');
        expect(DatePicker.prototype.localisations).to.be.an('object');
    });

    it('instance API (without polyfill)', function () {
        DatePicker.prototype.browserSupportsDateField = true;
        datePicker = $input.datePicker();

        expect(datePicker).to.be.an('object');
        expect(datePicker.getField).to.be.a('function');
        expect(datePicker.getOptions).to.be.a('function');
        expect(datePicker.reset).to.be.a('function');
        expect(datePicker.hide).to.not.be.defined;
        expect(datePicker.show).to.not.be.defined;
        expect(datePicker.getDate).to.not.be.defined;
        expect(datePicker.setDate).to.not.be.defined;
        expect(datePicker.destroyPolyfill).to.not.be.defined;
    });

    it('instance API (with polyfill)', function () {
        DatePicker.prototype.browserSupportsDateField = false;
        datePicker = $input.datePicker();

        expect(datePicker).to.be.an('object');
        expect(datePicker.getField).to.be.a('function');
        expect(datePicker.getDate).to.be.a('function');
        expect(datePicker.setDate).to.be.a('function');
        expect(datePicker.getOptions).to.be.a('function');
        expect(datePicker.reset).to.be.a('function');
        expect(datePicker.hide).to.be.a('function');
        expect(datePicker.show).to.be.a('function');
        expect(datePicker.destroyPolyfill).to.be.a('function');
    });

    it('remembers being constructed (via jQuery)', function () {
        datePicker = $input.datePicker({ overrideBrowserDefault: true });
        const datePicker2 = $input.datePicker();

        expect(datePicker).to.equal(datePicker2);
    });

    it('updates constructed datepicker options (via jQuery)', function () {
        datePicker = $input.datePicker({ overrideBrowserDefault: true, firstDay: 1 });
        $input.datePicker({ overrideBrowserDefault: true, firstDay: 5 });

        expect(datePicker.getOptions().firstDay).to.equal(5);
    });

    describe('CelandarWidget', function () {
        let $container;
        let $calendarWidget;
        beforeEach(function () {
            const container = fixtures({
                container: '<div id="test-container" type="date"></div>'
            }).container;
            $container = $(container);
        });

        afterEach(function() {
            if ($calendarWidget && $calendarWidget.destroy) {
                $calendarWidget.destroy();
            }
        });

        it('globals', function () {
            expect(AJS.CalendarWidget.toString()).to.equal(CalendarWidget.toString());
        });

        it('instance API', function () {
            $calendarWidget = $container.calendarWidget();
            expect($calendarWidget).to.be.an('object');
            expect($calendarWidget.reconfigure).to.be.a('function');
            expect($calendarWidget.destroy).to.be.a('function');
        });

        it('remembers being constructed (via jQuery)', function () {
            $calendarWidget = $container.calendarWidget();
            const $calendarWidget2 = $container.calendarWidget();

            expect($calendarWidget).to.equal($calendarWidget2);
        });

        it('updates constructed calendarWidget options (via jQuery)', function () {
            $calendarWidget = $container.calendarWidget({ firstDay: 1 });
            $container.datePicker({ firstDay: 5 });

            expect(datePicker.getOptions().firstDay).to.equal(5);
        });
    });

    describe('Behaviour', function () {

        it('change event fires (with polyfill)', function () {
            const inputEventSpy = sinon.spy();

            //We need to wrap the spy because sinon uses 'this' to access and record properties
            //when called directly as an eventHandler the value of 'this' is the element and
            //in chrome there are some properties on HTMLInputElement that throw exceptions
            //when accessed.
            function runSpy(){
                inputEventSpy();
            }

            $input.on('change', runSpy);
            datePicker = $input.datePicker({ overrideBrowserDefault: true });
            datePicker.show();
            selectDate('16');

            inputEventSpy.should.have.been.calledOnce;
        });

        it('focus stays on input field after opening dialog', function (done) {
            $input.datePicker({ overrideBrowserDefault: true });

            focus($input);

            const dialog = $('aui-inline-dialog');

            afterMutations(function () {
                expect(dialog.get(0).open, 'dialog to be open').to.equal(true);
                expect(document.activeElement, 'input to be element in focus').to.equal($input.get(0));
                done();
            });
        });

        describe('field attributes', function () {
            describe('min', function () {
                beforeEach(function () {
                    $input.val('2000-01-01');
                });

                it('is used on construction', function (done) {
                    $input.attr('min', '2999-01-20');
                    datePicker = $input.datePicker();
                    datePicker.show();
                    afterMutations(function () {
                        selectDate(1);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2999);
                        expect(date.getDate()).to.equal(20);
                        done();
                    });
                });

                it('can be changed after construction', function (done) {
                    datePicker = $input.datePicker();
                    datePicker.show();
                    $input.attr('min', '2999-01-20');
                    afterMutations(() => {
                        selectDate(1);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2999);
                        expect(date.getDate()).to.equal(20);
                        done();
                    });
                });

                it('can be removed', function (done) {
                    $input.attr('min', '2999-01-20');
                    datePicker = $input.datePicker();
                    datePicker.show();
                    $input.removeAttr('min');
                    afterMutations(() => {
                        selectDate(1);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2999);
                        expect(date.getDate()).to.equal(1);
                        done();
                    });
                });
            });

            describe('max', function () {
                beforeEach(function () {
                    $input.val('2999-01-16');
                });

                it('is used on construction', function (done) {
                    $input.attr('max', '2000-01-07');
                    datePicker = $input.datePicker();
                    datePicker.show();
                    afterMutations(function () {
                        selectDate(31);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2000);
                        expect(date.getDate()).to.equal(7);
                        done();
                    });
                });

                it('can be changed after construction', function (done) {
                    datePicker = $input.datePicker();
                    datePicker.show();
                    $input.attr('max', '2000-01-07');
                    afterMutations(() => {
                        selectDate(31);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2000);
                        expect(date.getDate()).to.equal(7);
                        done();
                    });
                });

                it('can be removed', function (done) {
                    $input.attr('max', '2000-01-07');
                    datePicker = $input.datePicker();
                    datePicker.show();
                    $input.removeAttr('max');
                    afterMutations(() => {
                        selectDate(31);
                        const date = datePicker.getDate();
                        expect(date.getFullYear()).to.equal(2000);
                        expect(date.getDate()).to.equal(31);
                        done();
                    });
                });
            });
        });
    });
});
