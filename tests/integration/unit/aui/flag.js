import $ from '@atlassian/aui/src/js/aui/jquery';
import flag from '@atlassian/aui/src/js/aui/flag';

describe('aui/flag', function () {
    const TEST_DEFAULTS = {
        type: 'info',
        title: 'Title',
        body: 'This message should float over the screen'
    };

    beforeEach(function () {
        $('#test-fixture').append('<div id="aui-flag-container"></div>');
    });

    afterEach(function () {
        $('#aui-flag-container').remove();
    });

    function setupFlag ({ type, title, body } = TEST_DEFAULTS) {
        return flag({ type, title, body });
    }

    const getFlagInDom = () => $('.aui-flag');
    const closeFlagInDom = () => $('.aui-flag .icon-close').click();

    const getTitleText = (flagElement) => $(flagElement).find('p.title').text();
    const getBodyText = (flagElement) => $(flagElement).text();

    const isVisible = (element) => $(element).attr('aria-hidden') !== 'true';

    const hasCloseIcon = (element) => !!$(element).find('.icon-close').length;



    it('global', function () {
        expect(AJS.flag.toString()).to.equal(flag.toString());
    });

    it('AMD module', function (done) {
        amdRequire(['aui/flag'], function (amdModule) {
            expect(amdModule).to.equal(AJS.flag);
            done();
        });
    });

    describe('Construction', function () {
        describe('via the JS API', function () {
            it('should return the raw DOM element', function () {
                expect(flag({})).to.be.an.instanceof(Element);
            });
        });
    });

    it('Floating message is present on the screen', function () {
        setupFlag();
        expect(getFlagInDom().length).to.equal(1);
        expect(isVisible(getFlagInDom())).to.equal(true);
    });

    it('Floating messages HTML contain the title and contents somewhere', function () {
        const flagProperties = {
            title: 'look at me!',
            body: 'I have a body!!!1'
        };
        const flag = setupFlag(flagProperties);
        const flagHtml = $(flag).html();
        expect(flagHtml).to.include(flagProperties.title);
        expect(flagHtml).to.include(flagProperties.body);
    });

    it('Messages appear with mostly default options', function () {
        setupFlag({body: 'This is a message with nearly all options default'});
        expect(getFlagInDom().length).to.equal(1);
    });

    it('Closing message triggers a close event on the message', function () {
        const flagElement = flag({body: 'Close me'});
        const flagSpy = sinon.spy();
        $(flagElement).on('aui-flag-close', flagSpy);

        closeFlagInDom();
        expect(flagSpy.callCount).to.equal(1);
    });

    it('Flags can be closed via the API method', function () {
        const flag = setupFlag();
        expect(flag.getAttribute('aria-hidden')).to.equal('false');
        flag.close();
        expect(flag.getAttribute('aria-hidden')).to.equal('true');
    });

    it('Flags can be closed via the DOM', function () {
        setupFlag();
        expect(getFlagInDom().attr('aria-hidden')).to.equal('false');
        closeFlagInDom();
        expect(getFlagInDom().attr('aria-hidden')).to.equal('true');
    });

    describe('When a flag is shown', function () {
        var clock;
        var flagElement;

        beforeEach(function () {
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            clock.restore();
        });

        describe('with the title option', function () {
            it('set to null, it has no title', function () {
                flagElement = flag({title: null});
                expect(getTitleText(flagElement)).to.equal('');
            });

            it('set to false, it has no title', function () {
                flagElement = flag({title: false});
                expect(getTitleText(flagElement)).to.equal('');
            });

            it('unset, it has no title', function () {
                flagElement = flag({});
                expect(getTitleText(flagElement)).to.equal('');
            });

            it('is HTML-safe', function () {
                flagElement = flag({title: '<b class="oh-whoops">groovy!</b>'});
                expect($(flagElement).find('.oh-whoops').length).to.equal(0);
            });
        });

        describe('with the body option', function () {
            it('set to null, it has no body', function () {
                flagElement = flag({body: null});
                expect(getBodyText(flagElement)).to.equal('');
            });

            it('set to false, it has no body', function () {
                flagElement = flag({body: false});
                expect(getBodyText(flagElement)).to.equal('');
            });

            it('unset, it has no body', function () {
                flagElement = flag({});
                expect(getBodyText(flagElement)).to.equal('');
            });

            it('outputs HTML', function () {
                flagElement = flag({body: '<b class="oh-yay">groovy!</b>'});
                expect($(flagElement).find('.oh-yay').length).to.equal(1);
            });

            it('preserves whitespace in body content', function () {
                flagElement = flag({body: '<p> one flew over \n the <b> cuckoo\'s nest</b> </p>'});
                expect(getBodyText(flagElement)).to.equal(' one flew over \n the  cuckoo\'s nest ');
            });
        });

        describe('with the close option', function () {
            describe('set to "manual"', function () {
                beforeEach(function () {
                    flagElement = flag({close: 'manual'});
                });

                it('the flag should not close after five seconds', function () {
                    clock.tick(10000);
                    expect(isVisible(flagElement)).to.equal(true);
                });

                it('the flag should have a close icon', function () {
                    expect(hasCloseIcon(flagElement)).to.equal(true);
                });
            });

            describe('set to "auto"', function () {
                beforeEach(function () {
                    flagElement = flag({close: 'auto'});
                });

                it('the flag should close after five seconds', function () {
                    clock.tick(10000);
                    expect(isVisible(flagElement)).to.equal(false);
                });

                it('the flag should have a close icon', function () {
                    expect(hasCloseIcon(flagElement)).to.equal(true);
                });
            });

            describe('set to "never"', function () {
                beforeEach(function () {
                    flagElement = flag({close: 'never'});
                });

                it('the flag should not close after five seconds', function () {
                    clock.tick(10000);
                    expect(isVisible(flagElement)).to.equal(true);
                });

                it('the flag should not have a close icon', function () {
                    expect(hasCloseIcon(flagElement)).to.equal(false);
                });
            });

            describe('left as default', function () {
                beforeEach(function () {
                    flagElement = flag({});
                });

                it('the flag should not close after five seconds', function () {
                    clock.tick(10000);
                    expect(isVisible(flagElement)).to.equal(true);
                });

                it('the flag should have a close icon', function () {
                    expect(hasCloseIcon(flagElement)).to.equal(true);
                });
            });
        });
    });

    describe('security', function() {
        let alerter;
        beforeEach(function () {
            alerter = sinon.stub(window, 'alert');
        });
        afterEach(function () {
            alerter.restore();
        });

        it('does not execute scripts', function() {
            flag({body: 'this script should not execute <script>alert("whoops")</script>'});
            expect(alerter.callCount).to.equal(0);
        });
    });
});
