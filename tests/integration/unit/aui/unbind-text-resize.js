import unbindTextResize from '@atlassian/aui/src/js/aui/unbind-text-resize';

describe('aui/unbind-text-resize', function () {
    it('globals', function () {
        expect(AJS.unbindTextResize.toString()).to.equal(unbindTextResize.toString());
    });
});
