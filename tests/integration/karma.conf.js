const webpackParts = require('@atlassian/aui-webpack-config/webpack.parts');
const merge = require('webpack-merge');

module.exports = function (config) {
    const {
        BITBUCKET_BUILD_NUMBER,
        BITBUCKET_REPO_SLUG,
        BROWSERSTACK_USER,
        BROWSERSTACK_USERNAME,
        BROWSERSTACK_ACCESS_KEY,
        BROWSERSTACK_ACCESSKEY,
        BROWSERSTACK_KEY,
        CI_BUILD_NUMBER,
    } = process.env;

    const webpackConfig = merge([
        webpackParts.setAuiVersion(),
        webpackParts.loadSoy(),
        webpackParts.resolveSoyDeps(),
        webpackParts.inlineCss(),
        webpackParts.transpileJs({
            exclude: /(tether|soy-template-plugin-js|node_modules)/
        }),
        {
            mode: 'development',
            devtool: false,
            externals: {
                'jquery': 'jQuery'
            },
            module: {
                rules: [
                    {
                        test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2)$/,
                        use: 'ignore-loader'
                    }
                ]
            }
        }
    ]);

    const testPattern = './entry/all-tests.js';
    const bootstrapPattern = './*.js';

    config.set({
        browserStack: {
            username: BROWSERSTACK_USERNAME || BROWSERSTACK_USER,
            accessKey: BROWSERSTACK_ACCESS_KEY || BROWSERSTACK_ACCESSKEY || BROWSERSTACK_KEY,
            project: BITBUCKET_REPO_SLUG || 'aui',
            build: BITBUCKET_BUILD_NUMBER || CI_BUILD_NUMBER || Date.now(),
        },
        hostname: config.watch ? '0.0.0.0' : 'localhost',
        autoWatch: config.watch,
        singleRun: !config.watch,
        frameworks: ['mocha', 'sinon-chai'],
        browsers: ['Chrome_1024x768','Firefox_1024x768'],
        browserDisconnectTolerance: 3,
        browserDisconnectTimeout: 10000,
        client: {
            mocha: {
                // bumped up from 2s so that IE will complete more consistently. we hope.
                // yes, this does indicate that there are performance issues in IE.
                timeout: 10000,
                slow: 1,
            }
        },
        customLaunchers: {
            Chrome_1024x768: {
                base: 'Chrome',
                flags: ['--window-size=1024,768']
            },
            Firefox_1024x768: {
                base: 'Firefox',
                flags: ['-foreground', '-width', '1024', '-height', '768']
            },
            IE11_1024x768: {
                base: 'BrowserStack',
                os: 'Windows',
                os_version: '10',
                browser: 'IE',
                browser_version: '11.0',
                resolution: '1024x768',
                name: 'Unit tests on IE',
            },
            Chrome_OSX: {
                base: 'BrowserStack',
                os: 'OS X',
                os_version: 'El Capitan',
                browser: 'Chrome',
                browser_version: null, // latest
                resolution: '1024x768',
                name: 'Unit tests on Chrome OSX',
            },
            Safari_OSX: {
                base: 'BrowserStack',
                os: 'OS X',
                os_version: 'Sierra',
                browser: 'Safari',
                browser_version: '10.1',
                resolution: '1024x768',
                name: 'Unit tests on Safari OSX',
            },
        },
        reporters: ['progress','junit', 'BrowserStack'],
        coverageReporter: {
            dir: 'reports/istanbul',
            type: 'html'
        },
        junitReporter: {
            outputDir: 'test-reports',
            suite: 'AUI'
        },
        preprocessors: {
            [testPattern]: 'webpack',
            [bootstrapPattern]: 'webpack',
        },
        webpack: webpackConfig,
        files: [
            require.resolve('jquery'),
            require.resolve('jquery-migrate'),

            {
                pattern: testPattern,
                watched: false,
            }
        ]
    });
};
